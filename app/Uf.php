<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uf extends Model
{
    //
    protected $fillable = ['nome'];

    //Criei o Mutator abaixo que transforma o Dado nome em Maiúsculas antes de entrar no Banco de Dados
    public function setNomeAttribute($value)
    {
        $this->attributes['nome'] = strtoupper($value);
    }

    public function fornecedores(){
        return $this->hasMany(Fornecedor::class);
    }

}
