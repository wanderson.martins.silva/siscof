<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoPessoa extends Model
{
    //
    protected $fillable = [
        'nome_tipo_pessoa'
    ];

    //Criei o Mutator abaixo que transforma o Dado nome_tipo_pessoa em Minúsculas antes de entrar no Banco de Dados
    public function setNomeTipoPessoaAttribute($value){
        $this->attributes['nome_tipo_pessoa'] = strtolower($value);
    }       
    
    public function fornecedores(){
        return $this->hasMany(Fornecedor::class);
    }
}
