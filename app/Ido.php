<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ido extends Model
{
    //
    protected $fillable = [
        'data',
        'usuario',
        'ano_ido',
        'scm_ido',
        'ano_scm',
        'id_modalidade_ido',
        'numero_ido',
        'ano_pregao_ido',
        'processo_ido',
        'objeto',
        'demanda',
    ];

    public function modalidade(){
        return $this->belongsTo(Modalidade::class, 'id_modalidade_ido', 'id');
    }
}
