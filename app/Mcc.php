<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mcc extends Model
{
    protected $fillable = [
        'ficha',
        'tipo_credito',
        'uo',
        'ua',
        'funcao',
        'subfuncao',
        'programa',
        'subprograma',
        'projativ',
        'natureza_despesa',
        'fonte',
        'fonte_detalhe',
        'vl_orcado',
        'vl_acrescimo',
        'vl_cancelado',
        'vl_provido_base',
        'vl_provido_adicional',
        'vl_reservado_minuta',
        'vl_solicitado_base',
        'vl_solicitado_adicional',
        'vl_aprovado_base',
        'vl_aprovado_adicional',
        'classificacao',
    ];
}
