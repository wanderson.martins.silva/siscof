<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrigemDestino extends Model
{
    //
    protected $fillable = ['bm', 'nome_usuario', 'id_setor'];
}
