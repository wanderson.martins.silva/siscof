<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NaturezaDespesas extends Model
{
    //
    protected $fillable = ['codigo_natureza', 'nome_natureza', 'escrituracao', 'ementa'];
}
