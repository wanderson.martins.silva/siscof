<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoLogradouro extends Model
{
    //
    public $fillable = [
        'nome_tipo_logradouro'
    ];

    public function logradouros(){
        return $this->hasMany(Logradouro::class, 'tipo_logradouro_id', 'id');        
    }
    

}
