<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecursoSicom extends Model
{
    //
    protected $fillable = [
        'codigo_recurso',
        'nome_recurso'
    ];
}
