<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ordenador extends Model
{
    //
    protected $table = 'ordenadores';
    
    protected $fillable = [
        'setor_id',
        'nome_ordenador',
        'matricula_ordenador',        
        'observacao_ordenador'
    ];

    public function setor(){
        return $this->belongsTo(Setor::class);
    }

}
