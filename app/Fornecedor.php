<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends Model
{
    //
    use SoftDeletes;

    protected $table = 'fornecedores';
    protected $fillable = [
        'nome',
        'cod_pessoa_externa',
        'cnpj',
        'tel_fixo',
        'tel_cel',
        'rua',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado_id',
        'cep',
        'pais', 
        'contato',
        'email',
        'site',
        'observacoes'             
    ];

    public function uf(){
        return $this->belongsTo(Uf::class, 'estado_id', 'id');
    }

    public function tipoPessoa(){
        return $this->belongsTo(TipoPessoa::class, 'tipo_pessoa_id', 'id');
    }
  
    public function espelhamentos(){
        return $this->hasMany(Espelhamento::class);
    }

}
