<?php

namespace App\Http\Middleware;

use Closure;
use App\LogAcesso;
use Illuminate\Support\Facades\Auth;

class LogAcessoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        // dd($request);
        $ip = $request->server->get('REMOTE_ADDR');
        $rota = $request->getRequestUri();

        
        if(isset(Auth::user()->name)){
            LogAcesso::create(['log' => "O Usuário " . Auth::user()->name ." com o IP $ip acessou a Rota $rota"]);
        }

        // return response('Chegamos no Middleware e finalizamos no próprio Middleware!');
        return $next($request);
    }
}
