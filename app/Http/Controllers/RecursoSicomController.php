<?php

namespace App\Http\Controllers;

use App\RecursoSicom;
use Illuminate\Http\Request;

class RecursoSicomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
        $recursos = RecursoSicom::all();
        return view('app.recurso_sicom.create', ['recursos' => $recursos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        RecursoSicom::create($request->all());
        return redirect()->route('recurso_sicom.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecursoSicom  $recursoSicom
     * @return \Illuminate\Http\Response
     */
    public function show(RecursoSicom $recursoSicom)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecursoSicom  $recursoSicom
     * @return \Illuminate\Http\Response
     */
    public function edit(RecursoSicom $recursoSicom)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecursoSicom  $recursoSicom
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecursoSicom $recursoSicom)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecursoSicom  $recursoSicom
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecursoSicom $recursoSicom)
    {
        //
    }
}
