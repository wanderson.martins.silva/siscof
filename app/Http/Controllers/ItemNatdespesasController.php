<?php

namespace App\Http\Controllers;

use App\ItemNatdespesas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemNatdespesasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $itens_natureza_despesas = DB::select("select n.id, n.codigo_natureza, n.nome_natureza, i.codigo_item, i.nome_item
            from natureza_despesas n
            inner join item_natdespesas i
            on i.codigo_natureza_despesa = n.codigo_natureza
            order by i.codigo_item asc");
        return view('app.item_natdespesa.index', ['itens_natureza_despesas' => $itens_natureza_despesas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //                
        // dd($request->_method);
        $codigo_natureza = $request->_method;

        $natureza_despesa = DB::select('select n.codigo_natureza, n.nome_natureza
            from natureza_despesas n
            where id = ?', [$codigo_natureza]
        );

        $naturezas_despesas = DB::select('select n.codigo_natureza, n.nome_natureza, i.codigo_item, i.nome_item
            from natureza_despesas n
            inner join item_natdespesas i
            on i.codigo_natureza_despesa = n.codigo_natureza
            where n.id = ?
            order by i.codigo_item asc', [$codigo_natureza]);

        // dd($natureza_despesa);
        return view('app.item_natdespesa.create', ['natureza_despesa' => $natureza_despesa, 'naturezas_despesas' => $naturezas_despesas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $saveRecord = [
            'codigo_item' => $request->codigo_item,
            'nome_item' => $request->nome_item,
            'codigo_natureza_despesa' => $request->codigo_natureza_despesa,            
            "created_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
            "updated_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
        ];

        DB::table('item_natdespesas')->insert($saveRecord);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemNatdespesas  $itemNatdespesas
     * @return \Illuminate\Http\Response
     */
    public function show(ItemNatdespesas $itemNatdespesas)
    {
        //
        // $response = ItemNatdespesas::all();

        // // return view('app.item_natdespesa.itens', ['response' => $response]);
        // return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemNatdespesas  $itemNatdespesas
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemNatdespesas $itemNatdespesas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemNatdespesas  $itemNatdespesas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemNatdespesas $itemNatdespesas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemNatdespesas  $itemNatdespesas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemNatdespesas $itemNatdespesas)
    {
        //
    }
    
     public function getItens(Request $request)
    {        
        $search = $request->search; 
        $path = '/app/item_despesa/getItens/';   
        $trimmed = str_replace($path, '', $request->getPathInfo());           
        
        
        $itens = ItemNatdespesas::select('codigo_item','nome_item','codigo_natureza_despesa')->where('codigo_natureza_despesa', '=', $trimmed)->orderBy('codigo_item')->get(); 

        $response = array();
        
        foreach($itens as $item){           
           $response[] = array("label"=>$item->codigo_item,"nome_item"=>$item->nome_item,"codigo_natureza_despesa"=>$item->codigo_natureza_despesa);
        }
          
        return response()->json($response); 
     } 
	 
     
}
