<?php

namespace App\Http\Controllers;

use App\Mcc;
use Illuminate\Http\Request;

class MccController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mccs = Mcc::paginate(1);
        return view('app.mcc.index', ['mccs' => $mccs]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mcc  $mcc
     * @return \Illuminate\Http\Response
     */
    public function show(Mcc $mcc)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mcc  $mcc
     * @return \Illuminate\Http\Response
     */
    public function edit(Mcc $mcc)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mcc  $mcc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mcc $mcc)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mcc  $mcc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mcc $mcc)
    {
        //
    }

    public function getMcc(Request $request)
    {
        $search = $request->search;
                
        if($search == ''){
            $mccs = Mcc::select('uo','ficha')->get();
        }else{
            $mccs = Mcc::select('uo','ficha','ua','funcao','projativ','programa','subfuncao','natureza_despesa','vl_orcado','fonte','fonte_detalhe')->where('ficha', 'like', '%' .$search . '%')->get();
        }
  
        $response = array();
        foreach($mccs as $mcc){
           $response[] = array("label"=>$mcc->ficha,"uo"=>$mcc->uo,"ua"=>$mcc->ua,"funcao"=>$mcc->funcao,"projativ"=>$mcc->projativ,"programa"=>$mcc->programa,
           "subfuncao"=>$mcc->subfuncao,"naturezadespesa"=>$mcc->natureza_despesa,"valororcado"=>$mcc->vl_orcado,"fonte"=>$mcc->fonte,"fontedetalhe"=>$mcc->fonte_detalhe);
        }
  
        return response()->json($response); 
     } 

    }

