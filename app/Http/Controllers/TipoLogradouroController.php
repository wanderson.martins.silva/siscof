<?php

namespace App\Http\Controllers;

use App\TipoLogradouro;
use Illuminate\Http\Request;

class TipoLogradouroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $tipos = TipoLogradouro::orderBy('nome_tipo_logradouro')->paginate(10);
        return view('app.tipologradouro.index', ['tipos' => $tipos, 'request' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.tipologradouro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        TipoLogradouro::create($request->all());
        return redirect()->route('tipo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoLogradouro  $tipoLogradouro
     * @return \Illuminate\Http\Response
     */
    public function show(TipoLogradouro $tipo)
    {
        //
        return view('app.tipologradouro.show', ['tipo' => $tipo]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoLogradouro  $tipoLogradouro
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoLogradouro $tipo)
    {
        //
        return view('app.tipologradouro.edit', ['tipo' => $tipo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoLogradouro  $tipoLogradouro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoLogradouro $tipo)
    {
        //
        $tipo->update($request->all());
        return redirect()->route('tipo.index', ['tipo' => $tipo->id]);   

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoLogradouro  $tipoLogradouro
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoLogradouro $tipoLogradouro)
    {
        //
    }
}
