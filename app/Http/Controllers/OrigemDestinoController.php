<?php

namespace App\Http\Controllers;

use App\OrigemDestino;
use App\Setor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrigemDestinoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $setores = Setor::all();
        $usuarios = DB::select('
            select o.bm, o.nome_usuario, s.nome_setor
            from setores s
            inner join origem_destinos o
            on o.id_setor = s.id            
        ');
        // dd($usuarios);
        
        return view('app.origem_destino.create', ['usuarios' => $usuarios, 'setores' => $setores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        OrigemDestino::create($request->all());
        return redirect()->route('origem-destino.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrigemDestino  $origemDestino
     * @return \Illuminate\Http\Response
     */
    public function show(OrigemDestino $origemDestino)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrigemDestino  $origemDestino
     * @return \Illuminate\Http\Response
     */
    public function edit(OrigemDestino $origemDestino)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrigemDestino  $origemDestino
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrigemDestino $origemDestino)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrigemDestino  $origemDestino
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrigemDestino $origemDestino)
    {
        //
    }

    public function getUsuario(Request $request){
        $path = '/app/origem-destino/getUsuario/';   
        $trimmed = str_replace($path, '', $request->getPathInfo()); 
        // dd($trimmed);
       
        $origens = OrigemDestino::select('bm','nome_usuario','id_setor')->where('bm', '=', $trimmed)->get(); 

        $response = array();
        
        foreach($origens as $origem){           
           $response[] = array("label"=>$origem->bm,"nome_usuario"=>$origem->nome_usuario,"id_setor"=>$origem->id_setor);
        }
          
        return response()->json($response); 
    }
}
