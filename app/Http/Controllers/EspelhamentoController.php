<?php

namespace App\Http\Controllers;

use App\Espelhamento;
use App\Fornecedor;
use App\Empenho;
use App\LogAcesso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class EspelhamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Ajustes
        // $empenhos = Empenho::all();
        // $espelhamento = Espelhamento::all();

        $empenhos = DB::select('
            SELECT e.empenho, e.nome_pessoa, e.ano_empenho, e.vl_empenhado, e.processo, e.ua, SUM(es.valor_despesa) AS total_despesas, e.vl_anulado
            FROM empenhos e
            LEFT JOIN espelhamentos es
            ON e.empenho::INTEGER = es.empenho::INTEGER
            GROUP BY e.empenho, e.nome_pessoa, e.ano_empenho, es.valor_empenhado, e.processo, e.vl_empenhado, e.vl_anulado, e.ua
        ');        

        // dd($empenhos[0]->empenho);

        $totais = DB::select('
        SELECT sum(es.valor_despesa) as total_despesas, e.empenho
        FROM empenhos e
        INNER JOIN espelhamentos es
        ON es.fornecedor_id = e.codigo_pessoa
        AND e.empenho::INTEGER = es.empenho::INTEGER
        GROUP BY e.codigo_pessoa, e.codigo_pessoa, e.empenho'        
        );              


    // dd($total);

        return view('app.espelhamento.index', ['empenhos' => $empenhos, 'request' => $request->all()]);      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $fornecedores = Fornecedor::all();
        return view('app.espelhamento.create', ['fornecedores' => $fornecedores]);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Espelhamento::create($request->all());
        return redirect()->route('espelhamento.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Espelhamento  $espelhamento
     * @return \Illuminate\Http\Response
     */
    public function show(Espelhamento $espelhamento)
    {
        //        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Espelhamento  $espelhamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Espelhamento $espelhamento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Espelhamento  $espelhamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Espelhamento $espelhamento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Espelhamento  $espelhamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Espelhamento $espelhamento)
    {              
                
        $espelhamento->delete();

        $lancamento_id = $espelhamento->getAttribute('id');
        $empenho = $espelhamento->getAttribute('empenho');
        $valor_despesa = $espelhamento->getAttribute('valor_despesa');        
        $valor_empenhado = substr_replace($espelhamento->getAttribute('valor_empenhado'), ",", strlen($espelhamento->getAttribute('valor_empenhado')) - 2, 0);
        // $valor_empenhado = number_format($valor_empenhado,2, ',', '.');

        //Retira o ponto monetário
        // $valor_empenhado = preg_replace("/[^0-9,]/","", $valor_empenhado);   

        //Substitui o ponto pela vírgula
        // $valor_empenhado = str_replace(",", ".", $valor_empenhado);        
        
        if(isset(Auth::user()->name)){
            LogAcesso::create(['log' => "O Usuário de Login " . Auth::user()->name ." com o Nome " . Auth::user()->login ." excluiu o Lançamento " .$lancamento_id . " do Empenho " . $empenho . " com o Valor de Despesa " . number_format($valor_despesa, 2, ',', '.') . " e Valor Empenhado " .  $valor_empenhado]);
        }
        
        return redirect()->back();
    }   
}
