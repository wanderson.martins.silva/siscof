<?php

namespace App\Http\Controllers;

use App\LancamentoIdo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LancamentoIdoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $lancamentos = DB::select('select l.id, l.ido_id, l.ficha_ido, i.ano_ido, l.resolucao, l.conta, l.portaria, i.processo_ido
            from lancamento_idos l
            inner join idos i
            on i.id = l.ido_id
        ');
        // dd($lancamentos);
        return view('app.lancamento_ido.index', ['lancamentos' => $lancamentos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // dd($request);
        
        $saveRecord = [
            'ido_id' => $request->ido_id,
            'ficha_ido' => $request->ficha_ido,
            'uo' => $request->uo,
            'ua' => $request->ua,
            'funcao' => $request->funcao,
            'subfuncao' => $request->subfuncao,
            'programa' => $request->programa,            
            'projativ' => $request->projativ, //Campo Ação que veio da Planilha Importada
            'subacao' => $request->subacao,
            'natureza_despesa' => $request->natureza_despesa,
            'item' => $request->item,
            'fonte' => $request->fonte,
            'fonte_detalhe' => $request->fonte_detalhe,
            'grupo' => $request->grupo,
            'sicom' => $request->sicom,
            'recurso' => $request->recurso,
            'resolucao' => $request->resolucao,
            'conta' => $request->conta,
            'portaria' => $request->portaria,
            'bm_origem' => $request->bm_origem,
            'bm_destino' => $request->bm_destino,
            'ord1' => $request->matricula_ordenador1,
            'ord2' => $request->matricula_ordenador2,
            'ord3' => $request->matricula_ordenador3,
            'ord4' => $request->matricula_ordenador4,

            "created_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
            "updated_at" =>  \Carbon\Carbon::now(), # Gravando a data de Alteração
        ];

        DB::table('lancamento_idos')->insert($saveRecord);
        return redirect()->route('lancamento_ido.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LancamentoIdo  $lancamentoIdo
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
                
        // dd($request()->all());
        return view('app.lancamento_ido.lancar');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LancamentoIdo  $lancamentoIdo
     * @return \Illuminate\Http\Response
     */
    public function edit(LancamentoIdo $lancamentoIdo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LancamentoIdo  $lancamentoIdo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LancamentoIdo $lancamentoIdo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LancamentoIdo  $lancamentoIdo
     * @return \Illuminate\Http\Response
     */
    public function destroy(LancamentoIdo $lancamentoIdo)
    {
        //
    }
}
