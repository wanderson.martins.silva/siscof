<?php

namespace App\Http\Controllers;

use App\Logradouro;
use App\TipoLogradouro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogradouroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //        
        //$tipos = TipoLogradouro::all();
        
        $logradouros = Logradouro::orderBy('id')->paginate(10);
        //return view('app.logradouro.index', ['logradouros' => $logradouros, 'tipos' => $tipos]);
        return view('app.logradouro.index', ['logradouros' => $logradouros, 'request' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tipos = TipoLogradouro::all();
        
        return view('app.logradouro.create', ['tipos' => $tipos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());
        //Ajustes realizados para aceitar a gravação de Setores

        Logradouro::create($request->all());
        return redirect()->route('logradouro.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Logradouro  $logradouro
     * @return \Illuminate\Http\Response
     */
    public function show(Logradouro $logradouro)
    {        
        return view('app.logradouro.show', ['logradouro' => $logradouro]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Logradouro  $logradouro
     * @return \Illuminate\Http\Response
     */
    public function edit(Logradouro $logradouro)
    {
        //
        return view('app.logradouro.edit', ['logradouro' => $logradouro]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Logradouro  $logradouro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Logradouro $logradouro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Logradouro  $logradouro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Logradouro $logradouro)
    {
        //
    }
}
