<?php

namespace App\Http\Controllers;

use App\TipoPessoa;
use Illuminate\Http\Request;

class TipoPessoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $pessoas = TipoPessoa::orderBy('id')->paginate(10);
        return view('app.pessoa.index', ['pessoas' => $pessoas, 'request' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.pessoa.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        TipoPessoa::create($request->all());
        
        return redirect()->route('pessoa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TipoPessoa  $tipoPessoa
     * @return \Illuminate\Http\Response
     */
    public function show(TipoPessoa $pessoa)
    {
        //
        return view('app.pessoa.show', ['pessoa' => $pessoa]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TipoPessoa  $tipoPessoa
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoPessoa $pessoa)
    {
        //
        return view('app.pessoa.edit', ['pessoa' => $pessoa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TipoPessoa  $tipoPessoa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoPessoa $pessoa)
    {
        //
        $pessoa->update($request->all());
        return redirect()->route('pessoa.index', ['pessoa' => $pessoa->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TipoPessoa  $tipoPessoa
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoPessoa $pessoa)
    {
        //
    }
}
