<?php

namespace App\Http\Controllers;

use App\Empenho;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmpenhoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empenhos = Empenho::paginate(1);
        return view('app.empenho.index', ['empenhos' => $empenhos]);               
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Empenho  $empenho
     * @return \Illuminate\Http\Response
     */
    public function show(Empenho $empenho)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Empenho  $empenho
     * @return \Illuminate\Http\Response
     */
    public function edit(Empenho $empenho)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Empenho  $empenho
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empenho $empenho)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Empenho  $empenho
     * @return \Illuminate\Http\Response
     */
    public function destroy(Empenho $empenho)
    {
        //
    }

}
