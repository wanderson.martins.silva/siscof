<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empenho;
use App\Espelhamento;
use App\Fornecedor;
use Illuminate\Support\Facades\DB;

class PreparaEspelhamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        try {

            //Query realizada para Pegar o Nome do Fornecedor dono do Espelhamento
            $espelhamentos = DB::select('select distinct(f.cod_pessoa_externa), f.nome, es.*, e.ua
            from fornecedores f
            inner join espelhamentos es
            on f.cod_pessoa_externa::BIGINT = es.fornecedor_id::BIGINT
            inner join empenhos e
            on e.empenho::INTEGER = es.empenho::INTEGER');
          
          } catch (\Exception $e) {          
            
            return $e->getPrevious()->getMessage();            
            // dd($e);              
          }       
        
        return view('app.prepara_espelhamento.index', ['espelhamentos' => $espelhamentos, 'request' => $request->all()]);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);        
        
        //Retira a Máscara monetária do Input valor_empenhado
        $valor_empenhado = preg_replace("/[^0-9]/","", $request->valor_empenhado);
                
        //Pega o valor_despesa que vem do preenchimento do Input
        $valor_despesa = $request->valor_despesa;

        ////Retira a Máscara monetária do Input valor_despesa mantendo a vírgula
        $valor_despesa = preg_replace("/[^0-9,]/","", $request->valor_despesa); 
        
        //Substitui a vírgula por ponto (Pois no banco de dados as casas decimais são representadas por .)
        $valor_despesa = str_replace(",", ".", $valor_despesa);
        
        //Verificando os dados da Requisição e Gravando os dados no Banco
        foreach($request ->nota_fiscal as $key=>$insert){
            $saveRecord = [
                'processo_licitatorio'      => $request->processo_licitatorio[$key],
                'valor_empenhado'           => $valor_empenhado[$key],           
                'fornecedor_id'             => $request->fornecedor_id[$key],
                'empenho'                   => $request->empenho[$key],        
                'ficha'                     => $request->ficha[$key],
                'ano_exercisio'             => $request->ano_exercisio[$key],  
                'numero_espelhamento'       => $request->numero_espelhamento[$key],
                'numero_credor'             => $request->numero_credor[$key],
                'nota_fiscal'               => $request->nota_fiscal[$key],  
                'processo_pagamento'        => $request->processo_pagamento[$key],                 
                'valor_despesa'             => $valor_despesa[$key],                
                'observacao_espelhamento'   => $request->observacao_espelhamento[$key],
                "created_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
                "updated_at" =>  \Carbon\Carbon::now(), # Gravando a Data de Atualização
                
            ];
            DB::table('espelhamentos')->insert($saveRecord);
        }
        
        return back();        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($empenho, Request $request)
    {              
      
        
        //Configurando a Data de Lançamento do Espelhamento                
        $data = date("d/m/Y");   
     
        //Buscando somente os Dados necessários da Tabela Empenhos para o Espelhamento
        $empenhos = Empenho::all('empenho', 'ua', 'nome_pessoa', 'processo', 'vl_empenhado', 'codigo_pessoa', 'ficha', 'vl_anulado', 'sub_acao', 'instrumento_juridico', 'ano_empenho')->where('empenho', $empenho);              
       
        //Realizando a consulta que vai preencher os dados na Grid dos Lançamentos realizados
        $espelhamento = DB::select('SELECT id, nota_fiscal, valor_despesa, created_at, updated_at, processo_pagamento, empenho 
        FROM espelhamentos WHERE empenho = ?', [$empenho]);
        
        $totais = DB::select('
            select es.valor_empenhado, sum(es.valor_despesa) as total_despesas
            from espelhamentos es
            where es.empenho = ? 
            group by es.valor_empenhado
            ', [$empenho]
        );       

        //Query realizada para aplicar nos Cálculos do Saldo Atual = vl_empenhado - despesas - vl_anulado
        $despesa = DB::select('SELECT sum(es.valor_despesa) as despesas
            FROM ESPELHAMENTOS es
            WHERE es.empenho = ?', [$empenho]);        
                        
        return view('app.prepara_espelhamento.show', ['empenhos' => $empenhos, 'data' => $data, 'espelhamento' => $espelhamento, 'totais' => $totais, 'despesa' => $despesa]);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(Request $request)
    {                   
        
        $r = $request->query();

        foreach($r as $value => $key)
        {
            $value;
        }

        $espelhamentos = DB::select('SELECT id, nota_fiscal, valor_despesa, created_at, processo_pagamento, empenho, observacao_espelhamento 
        FROM espelhamentos WHERE id = ?', [$key]);
        
        // $valor_despesa_editado = preg_replace("/[^0-9]/","", $espelhamentos[0]->valor_despesa);
        $valor_despesa_editado = $espelhamentos[0]->valor_despesa;

        $valor_despesa_editado = str_replace(".", ",", $valor_despesa_editado);
        //dd($valor_despesa_editado); 
        return view('app.prepara_espelhamento.edit', ['espelhamentos' => $espelhamentos, 'valor_despesa_editado' => $valor_despesa_editado, 'espelhamento_id' => $key]);  
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Espelhamento $espelhamento)
    {        
               
        $data = date('Y-m-d H:i:s', strtotime($request->updated_at));
        //$data = preg_replace("/[^0-9]/","", $data);
        
        $implode = $data;

        $id = $request->get('espelhamento_id'); 

        //Pega o Valor da Despesa
        $valor_despesa = $request->valor_despesa[0];

        //Retira o ponto monetário
        $valor_despesa = preg_replace("/[^0-9,]/","", $valor_despesa);

        //Substitui o ponto pela vírgula
        $valor_despesa = str_replace(",", ".", $valor_despesa);
        
        foreach($request ->nota_fiscal as $key=>$insert){
            $saveRecord = [                               
                'processo_pagamento'        => $request->processo_pagamento[$key],                
                'nota_fiscal'               => $request->nota_fiscal[$key],                
                'valor_despesa'             => $valor_despesa,                
                'observacao_espelhamento'   => $request->observacao_espelhamento[$key],
                'updated_at'                => $data,               
                
            ];
            DB::table('espelhamentos')
            ->where('id', $id)
            ->update($saveRecord);
        }

        
        return redirect()->route('prepara_espelhamento.show', $request->get('empenho'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
