<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\MccImport;

class ImportMccController extends Controller
{
    //
    public function import(Request $request) 
    {
            
        $regras = [
            'file' => 'required',            
        ];

        $feedback = [
            'required' => "O Nome da PLANILHA deve ser PREENCHIDO primeiro para realização da IMPORTAÇÃO",            
        ];

        $request->validate($regras, $feedback);

        //Deleta no Banco de dados todos os dados gravados da Planilha anterior
        

        try {
            
            //Prepara a importação da Nova Planilha para o Banco de Dados
            $file = $request->file('file')->store('import');
            
            (new MccImport)->import($file);
            
            return back(); 

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
             $file = $e->failures();
             
             foreach ($failures as $failure) {
                 $failure->row(); // row that went wrong
                 $failure->attribute(); // either heading key (if using heading row concern) or column index
                 $failure->errors(); // Actual error messages from Laravel validator
                 $failure->values(); // The values of the row that has failed.
             }
        }

        if($file){
            $msg = 'Importação realizada';
        }        
       
        return redirect('mcc.index', ['msg' => $msg]);

    }
}
