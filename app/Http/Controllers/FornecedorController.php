<?php

namespace App\Http\Controllers;

use App\Fornecedor;
use App\TipoPessoa;
use App\Uf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $fornecedores = Fornecedor::all();
        return view('app.fornecedor.index', ['fornecedores' => $fornecedores, 'request' => $request->all()]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $estados = Uf::all();
        $tipos = TipoPessoa::all();
        return view('app.fornecedor.create', ['estados' => $estados, 'tipos' => $tipos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $regras = [
            'nome' => 'required|min:3|max:30',
            'cod_pessoa_externa' => 'required',
            'tipo_pessoa_id' => 'required',
            'estado_id' => 'required'

        ];

        $feedback = [
            'required' => 'O Campo :attribute deve ser preenchido',
            'nome.min' => 'O Campo :attribute deve ter no mínimo 3 Caracteres',            
        ];

        $request->validate($regras, $feedback);

        $saveRecord = [
            'nome' => $request->nome,
            'tipo_pessoa_id' => $request->tipo_pessoa_id,
            'cod_pessoa_externa' => $request->cod_pessoa_externa,
            'estado_id' => $request->estado_id,
            'cnpj' => $request->cnpj,
            'tel_fixo' => $request->tel_fixo,
            'tel_cel' => $request->tel_cel,
            'rua' => $request->rua,
            'numero' => $request->numero,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cidade' => $request->cidade,
            'pais' => $request->pais,
            'cep' => $request->cep,
            'contato' => $request->contato,
            'email' => $request->email,
            'site' => $request->site,
            'observacoes' => $request->observacoes,
            "created_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
        ];

        DB::table('fornecedores')->insert($saveRecord);
        
        return redirect()->route('fornecedor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Fornecedor $fornecedor)
    {
        //
        //$uf = $fornecedor->uf()->first();
        //dd($uf);
        return view('app.fornecedor.show', ['fornecedor' => $fornecedor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Fornecedor $fornecedor)
    {
        //
        $estados = Uf::all();
        $tipos = TipoPessoa::all();
        return view('app.fornecedor.edit', ['fornecedor' => $fornecedor, 'estados' => $estados, 'tipos' => $tipos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fornecedor $fornecedor)
    {
        //
        $fornecedor->update($request->all());
        return redirect()->route('fornecedor.index', ['fornecedor' => $fornecedor->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function delete($id)
    {
        // dd($id);
        $fornecedor = Fornecedor::find($id);
        $fornecedor->delete();
        return response()->json(['success' => 'Fornecedor foi Excluído!']);
    }

    public function destroy($id)
    {
        
        $fornecedor = Fornecedor::find($id);
        $fornecedor->delete();
        $fornecedores = Fornecedor::all();
        // return view('app.fornecedor.index', ['fornecedores' => $fornecedores])->with('success','Fornecedor foi Excluído!');	
        return redirect()->back();	
    }    
}
