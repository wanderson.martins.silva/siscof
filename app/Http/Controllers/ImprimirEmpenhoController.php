<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ImprimirEmpenhoController extends Controller
{
    //
    
    public function imprimir_protocolo(Request $request)
    {
        
        //dd($request->all());
        //
        //return "Teste"; //
        //echo $request;

        foreach ($request->all() as $espelhamento => $key){
            $espelhamento;
        }

        // dd($espelhamento);

        $espelhamentos = DB::select('SELECT empenho, valor_despesa, ano_exercisio, created_at
        FROM espelhamentos WHERE empenho = ?', [$espelhamento]);

        // dd($espelhamentos);

        $funcionarios = [
            'Márcia Aparecida Abouid - BM - 38.127-x',
            'Amanda Caroline Amaral Campos - PRPS022309',
            'Guéria Silva de Paulo - PR108650',
            'Juliana Carvalho de Oliveira - PR111396',
            'Maria do Carmo Assunção – PR001763',
            'Simoní de Souza - PRPS017787'
        ];

        // dd($funcionarios);

        $totais = DB::select("
            select es.valor_empenhado, sum(es.valor_despesa) as total_despesas
            from espelhamentos es
            where es.empenho = ? 
            group by es.valor_empenhado
            ", [$espelhamento]
        );           

        return view('app.espelhamento.imprime_protocolo', ['espelhamentos' => $espelhamentos, 'totais' => $totais, 'funcionarios' => $funcionarios]);        
        
    }

    public function lancamento_unico(Request $request) {
        
        // dd($request->all());
        
        $lancamento = $request->get('lancamento');
        $empenho = $request->get('empenho');

        // dd($lancamento);

        $lancamentos = DB::select('SELECT empenho, valor_despesa, ano_exercisio, created_at
        FROM espelhamentos WHERE empenho = ? AND id= ?', [$empenho, $lancamento]);

        $funcionarios = [
            'Márcia Aparecida Abouid - BM - 38.127-x',
            'Amanda Caroline Amaral Campos - PRPS022309',
            'Guéria Silva de Paulo - PR108650',
            'Juliana Carvalho de Oliveira - PR111396',
            'Maria do Carmo Assunção – PR001763',
            'Simoní de Souza - PRPS017787'
        ];

        $funcionario = Auth::user()->login . " - " . Auth::user()->name;
        
        
        // dd($lancamentos);
        
        return view('app.espelhamento.imprime_lancamento', ['lancamentos' => $lancamentos, 'funcionarios' => $funcionarios, 'funcionario' => $funcionario]);

    }

    
}
