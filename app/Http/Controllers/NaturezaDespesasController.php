<?php

namespace App\Http\Controllers;

use App\NaturezaDespesas;
use Illuminate\Http\Request;

class NaturezaDespesasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $naturezas = NaturezaDespesas::all();
        return view('app.natureza_despesa.index', ['naturezas' => $naturezas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.natureza_despesa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        NaturezaDespesas::create($request->all());
        return redirect()->route('natureza_despesa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NaturezaDespesas  $naturezaDespesas
     * @return \Illuminate\Http\Response
     */
    public function show(NaturezaDespesas $naturezaDespesas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NaturezaDespesas  $naturezaDespesas
     * @return \Illuminate\Http\Response
     */
    public function edit(NaturezaDespesas $naturezaDespesas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NaturezaDespesas  $naturezaDespesas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NaturezaDespesas $naturezaDespesas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NaturezaDespesas  $naturezaDespesas
     * @return \Illuminate\Http\Response
     */
    public function destroy(NaturezaDespesas $naturezaDespesas)
    {
        //
    }

}
