<?php

namespace App\Http\Controllers;

use App\Setor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SetorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $setores = Setor::paginate(10);
        return view('app.setor.index', ['setores' => $setores]);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.setor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $regras = [
            'responsavel' => 'required|min:3|max:50',            
            'nome_setor' => 'required|min:3|max:130',            
        ];

        $feedback = [
            'required' => 'O Campo :attribute deve ser preenchido',
            'responsavel.min' => 'O Campo :attribute deve ter no mínimo 3 Caracteres', 
            'responsavel.max' => 'O Campo :attribute deve ter no máximo 50 Caracteres', 
             'nome_setor.min' => 'O Campo :attribute deve ter no mínimo 3 Caracteres', 
             'nome_setor.max' => 'O Campo :attribute deve ter no máximo 130 Caracteres',
            
        ];

        $request->validate($regras, $feedback); 
        // dd($request);
        Setor::create($request->all());
        return redirect()->route('setor.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Setor $setor)
    {
        //
        return view('app.setor.show', ['setor' => $setor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Setor $setor)
    {
        //
        return view('app.setor.edit', ['setor' => $setor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setor $setor)
    {
        //
        $setor->update($request->all());
        return redirect()->route('setor.index', ['setor' => $setor->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
