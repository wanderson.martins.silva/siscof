<?php

namespace App\Http\Controllers;

use App\Uf;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //$ufs = Uf::orderBy('nome', 'asc')->paginate(10);
        $ufs = Uf::all();
        return view('app.uf.index', ['ufs' => $ufs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('app.uf.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {                
        //Criei o Mutator setNomeAttribute($value) na Model Uf que transforma os dados em
        // Maiúsculos antes de entrar no Banco de dados.       
        Uf::create($request->all());
        return redirect()->route('uf.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Uf $uf)
    {
        //        
        return view('app.uf.show', ['uf' => $uf]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Uf $uf)
    {
        //
        return view('app.uf.edit', ['uf' => $uf]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Uf $uf)
    {
        //
        $uf->update($request->all());
        return redirect()->route('uf.index', ['uf' => $uf->id]);                 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Uf $uf)
    {
        //
        $uf->delete();
        return redirect()->route('uf.index');
    }
}
