<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ordenador;
use App\Setor;
use App\Uf;

class OrdenadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $ordenadores = Ordenador::paginate(10);
        $setores = Setor::all();
        return view('app.ordenador.index', ['ordenadores' => $ordenadores, 'request' => $request->all(), 'setores' => $setores]);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $setores = Setor::all();
        return view('app.ordenador.create', ['setores' => $setores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Ajustes realizados para aceitar a gravação de Setores

        //dd($request);
        
        $regras = [
            'nome_ordenador' => 'required|min:3|max:50',
            'matricula_ordenador' => 'required|min:1',
            'setor_id' => 'required|min:1|max:30',            
        ];

        $feedback = [
            'required' => 'O Campo :attribute deve ser preenchido',
             'nome_ordenador.min' => 'O Campo :attribute deve ter no mínimo 3 Caracteres',
             'matricula_ordenador.min' => 'O Campo :attribute deve ter no mínimo 1 Caracteres',             
        ];

        $request->validate($regras, $feedback);  
              
        Ordenador::create($request->all());        
        
        return redirect()->route('ordenador.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Ordenador $ordenador)
    {
        //
        return view('app.ordenador.show', ['ordenador' => $ordenador]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ordenador $ordenador)
    {
        //
        $estados = Uf::all();
        $setores = Setor::all();
        return view('app.ordenador.edit', ['estados' => $estados, 'ordenador' => $ordenador, 'setores' => $setores]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ordenador $ordenador)
    {
        //
        $ordenador->update($request->all());
        return redirect()->route('ordenador.index', ['ordenador' => $ordenador->id]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
