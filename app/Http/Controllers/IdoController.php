<?php

namespace App\Http\Controllers;

use App\Ido;
use App\RecursoSicom;
use App\Modalidade;
use App\OrigemDestino;
use App\Ordenador;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IdoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usuario = Auth::user()->name;

        $idos = Ido::where('usuario', '=', $usuario)->paginate(10);
        return view('app.ido.index', ['idos' => $idos]);   
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $modalidades = Modalidade::all();

        if(isset(Auth::user()->name)){
            $usuario = Auth::user()->name;
        }

        return view('app.ido.create', ['usuario' => $usuario, 'modalidades' => $modalidades]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        // dd($request->all());
        $regras = [
            'data' => 'required',            
        ];

        $feedback = [
            'required' => 'O Campo :attribute deve ser preenchido',            
        ];

        $request->validate($regras, $feedback);

        $saveRecord = [
            'data' => $request->data,
            'usuario' => $request->usuario,
            'ano_ido' => $request->ano_ido,
            'scm_ido' => $request->scm_ido,
            'ano_scm' => $request->ano_scm,
            'id_modalidade_ido' => $request->id_modalidade_ido,
            'numero_ido' => $request->numero_ido,
            'ano_pregao_ido' => $request->ano_pregao_ido,
            'processo_ido' => $request->processo_ido,
            'objeto' => $request->objeto,
            'demanda' => $request->demanda,
            "created_at" =>  \Carbon\Carbon::now(), # Gravando a data de Criação
            "updated_at" =>  \Carbon\Carbon::now(), # Gravando a data de Alteração
        ];

        DB::table('idos')->insert($saveRecord);
        
        return redirect()->route('ido.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ido  $ido
     * @return \Illuminate\Http\Response
     */
    public function show(Ido $ido)
    {
        
        $ordenadores = DB::select('select o.nome_ordenador, o.matricula_ordenador, s.nome_setor, s.sigla_setor
            from ordenadores o
            inner join setores s
            on s.id = o.setor_id');

        $recursos = RecursoSicom::all();        
       
        // dd($idos);

        $origem = DB::select('
            select o.bm, o.nome_usuario, s.nome_setor, s.sigla_setor
            from setores s
            inner join origem_destinos o
            on o.id_setor = s.id            
        ');
        $usuario = Auth::user()->name;
        return view('app.ido.show', ['usuario' => $usuario, 'ido' => $ido, 'recursos' => $recursos, 'origem' => $origem, 'ordenadores' => $ordenadores]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ido  $ido
     * @return \Illuminate\Http\Response
     */
    public function edit(Ido $ido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ido  $ido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ido $ido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ido  $ido
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ido $ido)
    {
        //
    }

    public function teste()
    {
        //
        return "Função Teste";
    }

}
