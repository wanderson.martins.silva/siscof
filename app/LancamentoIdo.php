<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LancamentoIdo extends Model
{
    //
    protected $fillable = [
        'ido_id',
        'ano_ido',
        'ficha_ido',
        'scm_ido',
        'ano_scm',
        'id_modalidade_ido',
        'numero_ido',
        'ano_pregao_ido',
        'processo_ido',
        'uo_ido',
        'ua_ido',
        'funcao_ido',
        'subfuncao_ido',
        'programa_ido',
        'projativ_ido',
        'subacao_ido',
        'natureza_despesa_ido',
        'item_ido',
        'fonte_ido',
        'fonte_detalhe_ido',
        'grupo_ido',
        'sicom_ido',
        'recurso_ido',
        'bm_origem',
        'bm_destino',
        'setor_origem',
        'setor_destinatario',       

    ];
}
