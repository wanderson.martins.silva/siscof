<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Espelhamento extends Model
{
    //
    public $fillable = [
        'fornecedor_id',
        'numero_credor',
        'processo_licitatorio',
        'processo_pagamento',
        'nota_fiscal',
        'valor_despesa',
        'ano_exercisio',
        'empenho',
        'ficha',
        'valor_empenhado',
        'numero_espelhamento',     
        'observacao_espelhamento',      
    ];


    public function fornecedor(){
        return $this->belongsTo(Fornecedor::class, 'cod_pessoa_externa', 'id');
    }

}
