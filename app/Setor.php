<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    //
    public $table = 'setores';
    
    public $fillable = [
        'nome_setor',
        'sigla_setor',
        'responsavel',
        'telefone_setor',
        'email_setor',        
        'observacao_setor',
    ];
}
