<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logradouro extends Model
{
    //
    protected $fillable = [
        'tipo_logradouro_id',
        'nome_logradouro',
        'numero_logradouro',
        'complemento_logradouro',
        'bairro_logradouro',
        'cidade_logradouro',
        'cep_logradouro',
        'observacao_logradouro'
    ];

    public function tipo(){
        return $this->belongsTo(TipoLogradouro::class, 'tipo_logradouro_id', 'id');        
    }

}
