<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contrato extends Model
{
    //
    protected $fillable = [
        'for_id',
        'numero_contrato',
        'inst_juri',
        'numero_processo',
        'pregao',
        'ano_pregao',
        'objeto_pregao',
        'objeto_contrato',
        'observacao'
    ];
}
