<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidade extends Model
{
    //
    protected $fillable = ['nome_modalidade'];

    public function idos(){
        return $this->hasMany(Ido::class);
    }

}
