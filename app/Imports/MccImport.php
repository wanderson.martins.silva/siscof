<?php

namespace App\Imports;

use App\Mcc;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class MccImport implements ToModel, WithHeadingRow, WithBatchInserts, WithChunkReading
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    use Importable, SkipsErrors;

    public function model(array $row)
    {
        return new Mcc([
            //
            'ficha'                         => $row['ficha'],            
            'tipo_credito'                  => $row['tipo_credito'],            
            'uo'                            => $row['uo'],            
            'ua'                            => $row['ua'],            
            'funcao'                        => $row['funcao'],            
            'subfuncao'                     => $row['subfuncao'],            
            'programa'                      => $row['programa'],            
            'subprograma'                   => $row['subprograma'],            
            'projativ'                      => $row['projativ'],            
            'natureza_despesa'              => $row['natureza_despesa'],            
            'fonte'                         => $row['fonte'],            
            'fonte_detalhe'                 => $row['fonte_detalhe'],            
            'vl_orcado'                     => $row['vl_orcado'],            
            'vl_acrescimo'                  => $row['vl_acrescimo'],            
            'vl_cancelado'                  => $row['vl_cancelado'],            
            'vl_provido_base'               => $row['vl_provido_base'],            
            'vl_provido_adicional'          => $row['vl_provido_adicional'],            
            'vl_reservado_minuta'           => $row['vl_reservado_minuta'],            
            'vl_solicitado_base'            => $row['vl_solicitado_base'],            
            'vl_solicitado_adicional'       => $row['vl_solicitado_adicional'],            
            'vl_aprovado_base'              => $row['vl_aprovado_base'],            
            'vl_aprovado_adicional'         => $row['vl_aprovado_adicional'],            
            'classificacao'                 => $row['classificacao'],                                   
        ]);
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    } 
}