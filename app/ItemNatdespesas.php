<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemNatdespesas extends Model
{
    //
    protected $fillable = ['codigo_item', 'nome_item', 'id_natureza_despesa'];
}
