@extends('site.layouts.basico')

    @section('titulo', 'Setor')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Setores - Editar</h1>                       
    </div>    

    <div class="conteudo-pagina">
        
        {{ $msg ?? '' }}
        <div class="container texto-container cor-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('setor.update', ['setor' => $setor->id]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >            
            <input type="hidden" name="id" value="{{ $setor->id ?? '' }}">
            @csrf
            @method('PUT')

                <div class="col-md-6" style="">       
                    <label for="inputNome" class="form-label">Nome</label>         
                    <input type="text" name="nome" value="{{ $setor->nome_setor ?? old('nome') }}" id="autoSizingInput" class="form-control">
                    {{ $errors->has('nome') ? $errors->first('nome') : '' }}
                </div>    

                <div class="col-md-2">
                    <label for="inputSigla" class="form-label">Sigla</label> 
                    <input type="text" name="sigla" value="{{ $setor->sigla_setor ?? old('sigla') }}" class="form-control">
                    {{ $errors->has('sigla') ? $errors->first('sigla') : '' }}
                </div>

                <div class="col-md-4">  
                    <label for="inputResponsavel" class="form-label">Responsável</label>   
                    <input type="text" name="responsavel" value="{{ $setor->responsavel ?? old('responsavel') }}" class="form-control">
                </div>  

                <div class="col-md-2">
                    <label for="inputTelefone" class="form-label">Telefone</label>   
                    <input type="tel" name="telefone" value="{{ $setor->telefone_setor ?? old('telefone') }}" class="form-control">
                </div>  

                <div class="col-md-4">
                    <label for="inputEmail" class="form-label">Email</label>   
                    <input type="text" name="email" value="{{ $setor->email_setor ?? old('email') }}" class="form-control">
                </div>

                <div class="col-md-12">
                    <label for="inputObservacao" class="form-label">Observação</label>   
                    <textarea name="observacao_setor" class="ajuste form-control">{{ $setor->observacao_setor ?? old('observacao_setor') }}</textarea>                    
                </div>   

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-warning" style="color:#fff;">ALTERAR</button>
                </div>                    
                      
                      
            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection