@extends('site.layouts.basico')

    @section('titulo', 'Setores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar Setor</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('setor.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                <tr>
                    <td class="texto-caixa-alta">ID:</td>
                    <td>{{ $setor->id }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Nome:</td>
                    <td>{{ $setor->nome_setor }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">Sigla:</td>
                    <td>{{ $setor->sigla_setor }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">Responsável:</td>
                    <td>{{ $setor->responsavel }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Telefone:</td>
                    <td>{{ $setor->telefone_setor }}</td>
                </tr>                                
                <tr>
                    <td class="texto-caixa-alta">Email:</td>
                    <td>{{ $setor->email_setor }}</td>
                </tr>                    
                
                <tr>                
                    <td class="texto-caixa-alta">Observação:</td>                    
                    <td style="padding-top: 50px; text-align:left">{{ $setor->observacao_setor }}</td>
                </tr>                    

            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection