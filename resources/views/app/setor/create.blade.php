@extends('site.layouts.basico')

    @section('titulo', 'Setores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Setores - Adicionar</h1> 
        <p class="texto-preto alinhar-titulo">Setores serão criados de acordo com o http://siomexterno.pbh.gov.br/siomexterno/</p>                              
    </div>
    
    <div class="conteudo-pagina-2">
        
        <div class="container texto-container alinhar-container">
        
        <form method="post" action="{{ route('setor.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-6" style="">       
                    <label for="inputNome" class="form-label">NOME</label>                             
                    <input type="text" name="nome_setor" value="{{ old('nome_setor') }}" class="form-control">                    
                    {{ $errors->has('nome_setor') ? $errors->first('nome_setor') : '' }}
                </div>    

                <div class="col-md-2">
                    <label for="inputSigla" class="form-label">SIGLA SETOR</label> 
                    <input type="text" name="sigla_setor" value="{{ old('sigla_setor') }}" class="form-control">
                    {{ $errors->has('sigla_setor') ? $errors->first('sigla_setor') : '' }}
                </div>

                <div class="col-md-4">  
                    <label for="inputResponsavel" class="form-label">RESPONSÁVEL</label>   
                    <input type="text" name="responsavel" value="{{ old('responsavel') }}" class="form-control">
                    {{ $errors->has('responsavel') ? $errors->first('responsavel') : '' }}
                </div>  

                <div class="col-md-2">
                    <label for="inputTelefone" class="form-label">TELEFONE SETOR</label>   
                    <input type="text" name="telefone_setor" value="{{ old('telefone_setor') }}" class="form-control">
                    {{ $errors->has('telefone_setor') ? $errors->first('telefone_setor') : '' }}
                </div>
                
                <div class="col-md-5">
                    <label for="inputEmail" class="form-label">EMAIL SETOR</label>   
                    <input type="text" name="email_setor" value="{{ old('email_setor') }}" class="form-control">
                    {{ $errors->has('email_setor') ? $errors->first('email_setor') : '' }}
                </div>     

                <div class="row">
                    <label for="inputObservacao" class="form-label">OBSERVACAO SETOR</label>                       
                    <textarea type="text" name="observacao_setor" class="form-control">{{ old('observacao_setor') }}</textarea>
                    {{ $errors->has('observacao_setor') ? $errors->first('observacao_setor') : '' }}
                </div>                 

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection