@extends('site.layouts.basico')

    @section('titulo', 'Setores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Setores /Listar</h1>                     
    </div>    

    <div class="conteudo-pagina">

        <div class="container-fluid" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>                                                        
                            <th class="texto-caixa-alta">Sigla</th>                            
                            <th class="texto-caixa-alta">Nome</th>
                            <th class="texto-caixa-alta">Responsável</th>
                            <th class="texto-caixa-alta">Telefone</th>
                            <th class="texto-caixa-alta">Últ. Alteração</th>                            
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody> 
                            @foreach ($setores as $setor )
                            <tr>
                                <td>{{ $setor->sigla_setor }}</td>
                                <td>{{ $setor->nome_setor }}</td>
                                <td>{{ $setor->responsavel }}</td>
                                <td>{{ $setor->telefone_setor }}</td>
                                <td>{{ $setor->updated_at->format('d/m/Y H:i:s') }}</td>

                                <td>
                                    <a href="{{ route('setor.show', ['setor' => $setor->id]) }}"class="btn btn-sm btn-success" style="color: #fff !important; width:120px;">
                                        <i class="far fa-eye" style="color:#000;"></i> Visualizar
                                    </a>
                                </td>

                                <td>
                                    <a href="" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-trash-alt" style="color:#000;"></i> Excluir
                                    </a>
                                </td>

                                <td>                                    
                                    <a href="{{ route('setor.edit', ['setor' => $setor->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-edit" style="color:#000;"></i> Editar
                                    </a>
                                </td>
                            </tr>    
                            @endforeach                                                                                               
 
                    </tbody>
                </table>    
      
                <br>
                O Total de Setores cadastrados são <strong>{{ $setores->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection