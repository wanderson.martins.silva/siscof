@extends('site.layouts.basico')

    @section('titulo', 'Contratos')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar Contratos</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('contrato.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                <tr>
                    <td class="texto-caixa-alta">FORNECEDOR:</td>
                    <td>{{ $contrato->for_id }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">NUMERO CONTRATO:</td>
                    <td>{{ $contrato->numero_contrato }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">INTRUMENTO JURÍDICO:</td>
                    <td>{{ $contrato->inst_juri }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">NÚMERO PROCESSO:</td>
                    <td>{{ $contrato->numero_processo }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">ANO PREGÃO:</td>
                    <td>{{ $contrato->ano_pregao }}</td>
                </tr>    
                <tr>
                    <td class="texto-caixa-alta">OBJETO PREGÃO:</td>
                    <td>{{ $contrato->objeto_pregao }}</td>
                </tr>                                
                <tr>
                    <td class="texto-caixa-alta">OBJETO CONTRATO</td>
                    <td>{{ $contrato->objeto_contrato }}</td>
                </tr>                    
                
                <tr>                
                    <td class="texto-caixa-alta">OBSERVAÇÃO:</td>                    
                    <td style="padding-top: 50px; text-align:left">{{ $contrato->observacao }}</td>
                </tr>                    

            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection