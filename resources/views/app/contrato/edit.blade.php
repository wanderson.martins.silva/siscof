@extends('site.layouts.basico')

    @section('titulo', 'Contrato')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Contratos - Editar</h1>                       
    </div>    

    <div class="conteudo-pagina">
        
        {{ $msg ?? '' }}
        <div class="container texto-container cor-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('contrato.update', ['contrato' => $contrato->numero_contrato]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >            
            <input type="hidden" name="id" value="{{ $contrato->id ?? '' }}">
            
            @csrf
            @method('PUT')

                <div class="col-md-4" style="">       
                    <label for="inputFornecedor" class="form-label">FORNECEDOR</label>         
                    <input type="text" name="for_id" value="{{ $contrato->for_id ?? old('for_id') }}" id="autoSizingInput" class="form-control">
                    {{ $errors->has('for_id') ? $errors->first('for_id') : '' }}
                </div>    

                <div class="col-auto">
                    <label for="inputNumero" class="form-label">NUMERO CONTRATO</label> 
                    <input type="text" name="numero_contrato" value="{{ $contrato->numero_contrato ?? old('numero_contrato') }}" class="form-control">
                    {{ $errors->has('numero_contrato') ? $errors->first('numero_contrato') : '' }}
                </div>

                <div class="col-auto">  
                    <label for="inputIntrumento" class="form-label">INTRUMENTO JURÍDICO</label>   
                    <input type="text" name="inst_juri" value="{{ $contrato->inst_juri ?? old('inst_juri') }}" class="form-control">
                </div>  

                <div class="col-md-12">
                    <label for="inputNumeroProcesso" class="form-label">NÚMERO PROCESSO</label>   
                    <input type="text" name="numero_processo" value="{{ $contrato->numero_processo ?? old('numero_processo') }}" class="form-control">
                </div> 
                
                <div class="col-md-12">
                    <label for="inputPregao" class="form-label">PREGÃO</label>   
                    <input type="text" name="pregao" value="{{ $contrato->pregao ?? old('pregao') }}" class="form-control">
                </div>

                <div class="col-md-12">
                    <label for="inputAno" class="form-label">ANO PREGÃO</label>   
                    <input type="text" name="ano_pregao" value="{{ $contrato->ano_pregao ?? old('ano_pregao') }}" class="form-control">
                </div>  

                <div class="col-md-3">
                    <label for="inputObjeto" class="form-label">OBJETO PREGÃO</label>                       
                    <input type="text" name="objeto_pregao" value="{{ old('objeto_pregao') }}" class="form-control">                   
                </div> 

                <div class="col-md-12">
                    <label for="inputObservacao" class="form-label">Observação</label>   
                    <textarea name="observacao" class="ajuste form-control">{{ $contrato->observacao ?? old('observacao') }}</textarea>                    
                </div>   

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-warning" style="color:#fff;">ALTERAR</button>
                </div>                    
                      
                      
            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection