@extends('site.layouts.basico')

    @section('titulo', 'Contratos')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Contratos /Listar</h1>  
    </div>    

    <div class="conteudo-pagina-2"> 

        <div class="container-fluid" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>                                                        
                            <th class="texto-caixa-alta">Fornecedor</th>                            
                            <th class="texto-caixa-alta">Número Contrato</th>
                            <th class="texto-caixa-alta">Instrumento Jurídico</th>
                            <th class="texto-caixa-alta">Número Processo</th>
                            <th class="texto-caixa-alta">Últ. Alteração</th>                            
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                            @foreach ($contratos as $contrato )
                            <tr>
                                <td>{{ $contrato->for_id }}</td>                                
                                <td>{{ $contrato->numero_contrato }}</td>
                                <td>{{ $contrato->inst_juri }}</td>
                                <td>{{ $contrato->numero_processo }}</td>
                                <td>{{ $contrato->updated_at->format('d/m/Y H:i:s') }}</td>

                                <td>
                                    <a href="{{ route('contrato.show', ['contrato' => $contrato->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:120px !important">
                                        <i class="far fa-eye" style="color:#000;"></i> Visualizar
                                    </a>
                                </td>

                                <td>
                                    <a href="" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-trash-alt" style="color:#000"></i> Excluir
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('contrato.edit', ['contrato' => $contrato->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-edit" style="color:#000;"></i> Editar
                                    </a>
                                </td>
                            </tr>    
                            @endforeach                                                                                               
 
                    </tbody>
                </table>    
      
                <br>
                O Total de Contratos cadastrados são <strong>{{ $contratos->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection