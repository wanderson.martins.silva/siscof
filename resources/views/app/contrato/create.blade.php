@extends('site.layouts.basico')

    @section('titulo', 'Contratos')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Contratos - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina-2">
        <div class = 'menu' style="background-color:#f2f3de;">
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('contrato.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-5" style="">       
                    <label for="inputFornecedor" class="form-label">FORNECEDOR</label>                             
                    <input type="text" name="for_id" value="{{ old('for_id') }}" class="form-control">                    
                    {{ $errors->has('for_id') ? $errors->first('for_id') : '' }}
                </div>    

                <div class="col-md-3">
                    <label for="inputNumero" class="form-label">NUMERO CONTRATO</label> 
                    <input type="text" name="numero_contrato" value="{{ old('numero_contrato') }}" class="form-control">
                    {{ $errors->has('numero_contrato') ? $errors->first('numero_contrato') : '' }}
                </div>

                <div class="col-md-3">  
                    <label for="inputIntrumento" class="form-label">INTRUMENTO JURÍDICO</label>   
                    <input type="text" name="inst_juri" value="{{ old('inst_juri') }}" class="form-control">
                    {{ $errors->has('inst_juri') ? $errors->first('inst_juri') : '' }}
                </div>  

                <div class="col-md-3">
                    <label for="inputNumeroProcesso" class="form-label">NÚMERO PROCESSO</label>   
                    <input type="text" name="numero_processo" value="{{ old('numero_processo') }}" class="form-control">
                    {{ $errors->has('numero_processo') ? $errors->first('numero_processo') : '' }}
                </div>
                
                <div class="col-md-3">
                    <label for="inputPregao" class="form-label">PREGÃO</label>   
                    <input type="text" name="pregao" value="{{ old('pregao') }}" class="form-control">
                    {{ $errors->has('pregao') ? $errors->first('pregao') : '' }}
                </div>     

                <div class="col-md-2">
                    <label for="inputAno" class="form-label">ANO PREGÃO</label>                       
                    <input type="text" name="ano_pregao" value="{{ old('ano_pregao') }}" class="form-control">
                    {{ $errors->has('ano_pregao') ? $errors->first('ano_pregao') : '' }}
                </div> 

                <div class="col-md-3">
                    <label for="inputObjeto" class="form-label">OBJETO PREGÃO</label>                       
                    <input type="text" name="objeto_pregao" value="{{ old('objeto_pregao') }}" class="form-control">
                    {{ $errors->has('objeto_pregao') ? $errors->first('objeto_pregao') : '' }}
                </div> 

                <div class="row">
                    <label for="inputObjeto" class="form-label">OBJETO CONTRATO</label>                       
                     <textarea type="text" name="objeto_contrato" class="form-control">{{ old('objeto_contrato') }}</textarea>
                    {{ $errors->has('objeto_contrato') ? $errors->first('objeto_contrato') : '' }}
                </div>    

                <div class="row">
                    <label for="inputObservacao" class="form-label">OBSERVAÇÃO</label>                       
                    <textarea type="text" name="observacao" class="form-control">{{ old('observacao') }}</textarea>
                    {{ $errors->has('observacao') ? $errors->first('observacao') : '' }}
                </div> 

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection