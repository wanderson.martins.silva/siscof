@extends('site.layouts.basico')

    @section('titulo', 'Recurso Sicom')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">RECURSOS SICOM - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('recurso_sicom.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf

                <div class="col-md-2">
                    <label for="inputCodigoNatureza" class="form-label">CÓDIGO RECURSO</label> 
                    <input type="text" name="codigo_recurso" value="{{ old('codigo_recurso') }}" class="form-control">
                    {{ $errors->has('codigo_recurso') ? $errors->first('codigo_recurso') : '' }}
                </div>

                <div class="col-md-5">  
                    <label for="inputNome" class="form-label">NOME DO RECURSO</label>   
                    <input type="text" name="nome_recurso" value="{{ old('nome_recurso') }}" class="form-control">
                    {{ $errors->has('nome_recurso') ? $errors->first('nome_recurso') : '' }}
                </div> 

                <div class="quebralinha">
                    <br />
                </div>

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>

        </div>

        <div class="container-fluid" style="margin-top:10px;">
            <table class="table table-dark table-striped table-hover" id="dataTable">
                <thead>
                    <tr>
                        <th class="texto-caixa-alta">CÓDIGO DA RECURSO</th>
                        <th class="texto-caixa-alta">NOME DO RECURSO</th>                                                                        
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($recursos as $recurso)
                    <tr>
                        <td>{{ $recurso->codigo_recurso }}</td>
                        <td>{{ $recurso->nome_recurso }}</td>                        
                    </tr>                            
                    @endforeach
                </tbody>    
            </table>        
        </div>           

    </div>   

</body>   

@endsection