@extends('site.layouts.basico')

    @section('titulo', 'UFs')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar UFs/Estados</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('uf.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                <tr>
                    <td class="texto-caixa-alta">ID:</td>
                    <td>{{ $uf->id }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Sigla:</td>
                    <td class="texto-caixa-alta">{{ $uf->nome }}</td>
                </tr>                
                </tr>                                
            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection