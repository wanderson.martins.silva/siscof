@extends('site.layouts.basico')

    @section('titulo', 'Ufs')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Ufs (Estados) /Listar</h1>    
    </div>    

    <div class="conteudo-pagina">

        <div class="container-fluid" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>                                                        
                            <th class="texto-caixa-alta">Nome (Sigla)</th>                                                        
                            <th class="texto-caixa-alta">Dt. Cadastro</th>                                                        
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody> 
                            @foreach ($ufs as $uf )
                            <tr>
                                <td class="texto-caixa-alta">{{ $uf->nome }}</td>                                
                                <td>{{ $uf->updated_at->format('d/m/Y H:i:s') }}</td>
                                <td>
                                    <a href="{{ route('uf.show', ['uf' => $uf->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:120px !important">
                                        <i class="far fa-eye" style="color:#000;"></i> Visualizar
                                    </a>
                                </td>

                                <td>
                                    <form class="form_exclusao-2" id="form_{{ $uf->id }}" method="POST" action="{{ route('uf.destroy', ['uf' => $uf->id]) }}">
                                        @method('DELETE')
                                        @csrf

                                        <a href="#" onclick="document.getElementById('form_{{ $uf->id }}').submit()" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px;">
                                            <i class="fas fa-trash-alt" style="color:#000"></i> Excluir
                                        </a>                                        
                                    </form>    
                                </td>
                                <td>
                                    <a href="{{ route('uf.edit', ['uf' => $uf->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-edit" style="color:#000;"></i>Editar
                                    </a>
                                </td>
                            </tr>    
                            @endforeach                                                                                               
 
                    </tbody>
                </table> 
              
                
        </div>

        </div>

    </div>   

</body>   

@endsection