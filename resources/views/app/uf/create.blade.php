@extends('site.layouts.basico')

    @section('titulo', 'Ufs (Estados)')

    @section('conteudo') 
<body>
            
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">UFs / Estados - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('uf.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-5" style="">       
                    <label for="inputNome" class="form-label">SIGLA</label>                             
                    <input type="text" name="nome" value="{{ old('nome') }}" class="form-control texto-caixa-alta" id="nome">                    
                    {{ $errors->has('nome') ? $errors->first('nome') : '' }}
                </div>

                <div class="col-md-2" style="margin-top:40px;">                     
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection