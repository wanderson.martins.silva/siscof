@extends('site.layouts.basico')

    @section('titulo', 'Ordenador')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Ordenadores - Editar</h1>                       
        <div class = 'menu' style="background-color:#f2f3de;">
            <ul>
                <li><a href = "{{ route('ordenador.create') }}">Novo</a></li>
                <li><a href = "{{ route('ordenador.index') }}">Consulta</a></li>
            </ul>
        </div>        
    </div>    

    <div class="conteudo-pagina">

        {{ $msg ?? '' }}
        <div class="container texto-container cor-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('ordenador.update', ['ordenador' => $ordenador->id]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >            
            <input type="hidden" name="id" value="{{ $ordenador->id ?? '' }}">
            @csrf
            @method('PUT')

                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">Nome</label>         
                    <input type="text" name="nome_ordenador" value="{{ $ordenador->nome_ordenador ?? old('nome_ordenador') }}" id="autoSizingInput" class="form-control">
                    {{ $errors->has('nome_ordenador') ? $errors->first('nome_ordenador') : '' }}
                </div>    

                <div class="col-auto">
                    <label for="inputMatricula" class="form-label">Matrícula</label> 
                    <input type="numeric" name="matricula_ordenador" value="{{ $ordenador->matricula_ordenador ?? old('matricula_ordenador') }}" class="form-control">
                    {{ $errors->has('matricula_ordenador') ? $errors->first('matricula_ordenador') : '' }}
                </div>
 
                <div class="col-md-2" style="">       
                    <label for="inputSetor" class="form-label">SETOR</label>                             
                    
                    <select name="setor_id" class="form-select texto-caixa-alta">
                        <option selected>SELECIONE O SETOR</option>
                                                                        
                            @foreach ($setores as $setor)
                                <option value="{{ $setor->id }}" {{ ($ordenador->setor_id ?? old('setor->id')) == $setor->id ? 'selected' : ''}}>{{ $setor->sigla_setor }}</option>
                            @endforeach                        
                    </select>
                </div> 
                
                <div class="col-md-12">
                    <label for="inputObservacao" class="form-label">Observação</label>   
                    <textarea name="observacao_ordenador" class="ajuste form-control">{{ $ordenador->observacao_ordenador ?? old('observacao_ordenador') }}</textarea>
                </div>   

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-warning" style="color:#fff;">ALTERAR</button>
                </div>                    
                      
                      
            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection