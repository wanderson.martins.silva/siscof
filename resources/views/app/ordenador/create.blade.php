@extends('site.layouts.basico')

    @section('titulo', 'Ordenadores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Ordenadores - Adicionar</h1>
    </div>    
        
    <div class="conteudo-pagina-2"> 
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('ordenador.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">NOME</label>                             
                    <input type="text" name="nome_ordenador" value="{{ old('nome_ordenador') }}" class="form-control">                    
                    {{ $errors->has('nome_ordenador') ? $errors->first('nome_ordenador') : '' }}
                </div>    

                <div class="col-auto">
                    <label for="inputMatricula" class="form-label">MATRÍCULA</label> 
                    <input type="numeric" name="matricula_ordenador" value="{{ old('matricula_ordenador') }}" class="form-control">
                    {{ $errors->has('matricula_ordenador') ? $errors->first('matricula_ordenador') : '' }}
                </div>

                <div class="col-md-3" style="">       
                    <label for="inputSetor" class="form-label">SETOR</label>                             
                    
                    <select name="setor_id" class="form-select texto-caixa-alta">
                        <option selected>SELECIONE O SETOR</option>
                                                                        
                            @foreach ($setores as $setor)
                                <option value="{{ $setor->id }}" {{ ($ordenador->setor_id ?? old('setor->id')) == $setor->id ? 'selected' : ''}}>{{ $setor->sigla_setor }}</option>
                            @endforeach                        
                    </select>
                </div> 
                
                <div class="row">
                    <label for="inputObservacao" class="form-label">OBSERVAÇÃO</label>   
                    <textarea name="observacao_ordenador" class="ajuste form-control">{{ old('observacao_ordenador') }}</textarea>
                    {{ $errors->has('observacao_ordenador') ? $errors->first('observacao_ordenador') : '' }}
                </div>                 

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection