@extends('site.layouts.basico')

    @section('titulo', 'Ordenadores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar Ordenadores</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('ordenador.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>

        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                <tr>
                    <td class="texto-caixa-alta">ID:</td>
                    <td>{{ $ordenador->id }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Nome:</td>
                    <td>{{ $ordenador->nome_ordenador }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">Matrícula:</td>
                    <td>{{ $ordenador->matricula_ordenador }}</td>
                </tr>                
                <tr>
                    <td class="texto-caixa-alta">Setor:</td>
                    <td>{{ $ordenador->setor->nome_setor }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Observação:</td>
                    <td>{{ $ordenador->observacao_ordenador }}</td>
                </tr>                                
            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection