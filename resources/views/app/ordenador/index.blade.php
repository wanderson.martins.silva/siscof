@extends('site.layouts.basico')

    @section('titulo', 'Ordenador')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Ordenadores - Listar</h1>                                    
    </div>    

    <div class="conteudo-pagina-2">

        <div class="container-fluid" style="padding-top:10px;">
                 <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">Nome</th>
                            <th class="texto-caixa-alta">Matrícula</th>
                            <th class="texto-caixa-alta">Setor</th>
                            <th class="texto-caixa-alta">Observação</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                                
                        @foreach ($ordenadores as $ordenador)
                            <tr>
                                <td>{{ $ordenador->nome_ordenador }}</td>
                                <td>{{ $ordenador->matricula_ordenador }}</td>
                                <td>{{ $ordenador->setor->sigla_setor }}</td>                                
                                <td>{{ substr($ordenador->observacao_ordenador, 0, 30) }}</td>  

                                <td>
                                    <a href="{{ route('ordenador.show', ['ordenador' => $ordenador->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:120px;">
                                        <i class="far fa-eye" style="color:#000;"></i> Visualizar
                                    </a>
                                </td>

                                <td>
                                    <a href="" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-trash-alt" style="color:#000;"></i> Excluir
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('ordenador.edit', ['ordenador' => $ordenador->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-edit" style="color:#000;"></i> Editar
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>       

                {{ $ordenadores->appends($request)->links() }}                 
                <br>
                Exibindo o Total de <strong>{{ $ordenadores->total() }}</strong> Ordenador(es)
        </div>

        </div>

    </div>   

</body>   

@endsection