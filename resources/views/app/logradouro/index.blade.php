@extends('site.layouts.basico')

    @section('titulo', 'Logradouros')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Logradouros /Listar</h1>   
    </div>    

    <div class="conteudo-pagina-2">        

        <div class="container-fluid" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>                           
                            <th class="texto-caixa-alta">TIPO</th>
                            <th class="texto-caixa-alta">LOGRADOURO</th>                            
                            <th class="texto-caixa-alta">CIDADE</th>                            
                            <th class="texto-caixa-alta">ÚLT. ALTERAÇÃO</th>                            
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                                @foreach ($logradouros as $logradouro )
                                    <td style="text-transform:capitalize">{{ $logradouro->tipo->nome_tipo_logradouro }}</td>
                                    <td style="text-transform:capitalize">{{ $logradouro->nome_logradouro }}</td>
                                    <td style="text-transform:capitalize">{{ $logradouro->cidade_logradouro }}</td>
                                    <td>{{ $logradouro->updated_at->format('d/m/Y H:i:s') }}</td>
                                    
                                    <td>
                                        <a href="{{ route('logradouro.show', ['logradouro' => $logradouro->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:120px !important">
                                            <i class="far fa-eye" style="color:#000;"></i> Visualizar</a>
                                    </td>

                                    <td>
                                        <a href="" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-trash-alt" style="color:#000;"></i> Excluir</a>
                                    </td>

                                    <td>
                                        <a href="{{ route('logradouro.edit', ['logradouro' => $logradouro->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-edit" style="color:#000;"></i> Editar</a>
                                    </td>
                            </tr>    
                                @endforeach                                                                                               

                    </tbody>
                </table>    
      
                <br>
                O Total de Logradouros cadastrados são <strong>{{ $logradouros->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection