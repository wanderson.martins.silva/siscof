@extends('site.layouts.basico')

    @section('titulo', 'Logradouros')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Logradouros - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('logradouro.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-3" style="">       
                    <label for="inputNome" class="form-label">Tipo Logradouro</label>                             
                    
                    <select name="tipo_logradouro_id" class="form-select texto-caixa-alta">
                        <option selected>-- Selecione o Tipo --</option>
                                                                          
                            @foreach ($tipos as $tipo)
	                            <option value="{{ $tipo->id }}" {{ ($logradouro->tipo_logradouro_id ?? old('tipo->id')) == $tipo->id ? 'selected' : ''}}>{{ $tipo->nome_tipo_logradouro }}</option>
                            @endforeach
                        
                    </select>
                </div>

                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">Nome Logradouro</label>                             
                    <input type="text" name="nome_logradouro" value="{{ old('nome_logradouro') }}" class="form-control texto-caixa-alta" id="nome_logradouro">                    
                    {{ $errors->has('nome_logradouro') ? $errors->first('nome_logradouro') : '' }}
                </div>

                <div class="col-md-2" style="">       
                    <label for="inputNumero" class="form-label">Número</label>                             
                    <input type="text" name="numero_logradouro" value="{{ old('numero_logradouro') }}" class="form-control texto-caixa-alta" id="numero_logradouro">                    
                    {{ $errors->has('numero_logradouro') ? $errors->first('numero_logradouro') : '' }}
                </div>                

                <div class="col-md-3" style="">       
                    <label for="inputComplemento" class="form-label">Complemento Logradouro</label>                             
                    <input type="text" name="complemento_logradouro" value="{{ old('complemento_logradouro') }}" class="form-control texto-caixa-alta" id="complemento_logradouro">                    
                    {{ $errors->has('complemento_logradouro') ? $errors->first('complemento_logradouro') : '' }}
                </div>

                <div class="col-md-5" style="">       
                    <label for="inputBairro" class="form-label">Bairro Logradouro</label>                             
                    <input type="text" name="bairro_logradouro" value="{{ old('bairro_logradouro') }}" class="form-control texto-caixa-alta" id="bairro_logradouro">                    
                    {{ $errors->has('bairro_logradouro') ? $errors->first('bairro_logradouro') : '' }}
                </div>

                <div class="col-md-5" style="">       
                    <label for="inputCidade" class="form-label">Cidade Logradouro</label>                             
                    <input type="text" name="cidade_logradouro" value="{{ old('cidade_logradouro') }}" class="form-control texto-caixa-alta" id="cidade_logradouro">                    
                    {{ $errors->has('cidade_logradouro') ? $errors->first('cidade_logradouro') : '' }}
                </div>                

                <div class="col-md-2" style="">       
                    <label for="inputCep" class="form-label">Cep Logradouro</label>                             
                    <input type="text" name="cep_logradouro" value="{{ old('cep_logradouro') }}" class="form-control texto-caixa-alta" id="cep_logradouro">                    
                    {{ $errors->has('cep_logradouro') ? $errors->first('cep_logradouro') : '' }}
                </div>
                
                <div class="row">
                    <label for="inputObservacao" class="form-label">OBSERVAÇÃO</label>   
                    <textarea name="observacao_logradouro" class="ajuste form-control"></textarea>
                    {{ $errors->has('observacao_logradouro') ? $errors->first('observacao_logradouro') : '' }}
                </div>                   

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection