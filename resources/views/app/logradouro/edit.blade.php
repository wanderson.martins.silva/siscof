@extends('site.layouts.basico')

    @section('titulo', 'Logradouro')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Logradouros - Editar</h1>                       
    </div>    

    <div class="conteudo-pagina">
        
        {{ $msg ?? '' }}
        <div class="container texto-container cor-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('logradouro.update', ['logradouro' => $logradouro->id]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >            
            <input type="hidden" name="id" value="{{ $logradouro->id ?? '' }}">
            
            @csrf
            @method('PUT')

                <div class="col-md-4" style="">                           
                    <label for="inputNome" class="form-label">Nome Logradouro</label>                             
                    <input type="text" name="nome_logradouro" value="{{ $logradouro->nome_logradouro ?? old('nome_logradouro') }}" class="form-control texto-caixa-alta" id="nome_logradouro">     
                    {{ $errors->has('nome_logradouro') ? $errors->first('nome_logradouro') : '' }}
                </div>    

                <div class="col-md-2">
                    <label for="inputNumero" class="form-label">Número</label>                             
                    <input type="text" name="numero_logradouro" value="{{ $logradouro->numero_logradouro ?? old('numero_logradouro') }}" class="form-control texto-caixa-alta" id="numero_logradouro"> 
                    {{ $errors->has('numero_logradouro') ? $errors->first('numero_logradouro') : '' }}
                </div>

                <div class="col-md-2">  
                    <label for="inputComplemento" class="form-label">Complemento</label>                             
                    <input type="text" name="complemento_logradouro" value="{{ $logradouro->complemento_logradouro ?? old('complemento_logradouro') }}" class="form-control texto-caixa-alta" id="complemento_logradouro">                    
                    {{ $errors->has('complemento_logradouro') ? $errors->first('complemento_logradouro') : '' }}
                </div>  

                <div class="col-md-4">
                    <label for="inputCidade" class="form-label">Cidade Logradouro</label>                             
                    <input type="text" name="cidade_logradouro" value="{{ $logradouro->cidade_logradouro ?? old('cidade_logradouro') }}" class="form-control texto-caixa-alta" id="cidade_logradouro">                    
                    {{ $errors->has('cidade_logradouro') ? $errors->first('cidade_logradouro') : '' }}
                </div>               
                
                <div class="col-md-3">
                    <label for="inputBairro" class="form-label">Bairro Logradouro</label>                             
                    <input type="text" name="bairro_logradouro" value="{{ $logradouro->bairro_logradouro ?? old('bairro_logradouro') }}" class="form-control texto-caixa-alta" id="cep_logradouro">                    
                    {{ $errors->has('bairro_logradouro') ? $errors->first('bairro_logradouro') : '' }}               
                </div> 

                <div class="col-md-3">
                    <label for="inputCep" class="form-label">Cep Logradouro</label>                             
                    <input type="text" name="cep_logradouro" value="{{ $logradouro->cep_logradouro ?? old('cep_logradouro') }}" class="form-control texto-caixa-alta" id="cep_logradouro">                    
                    {{ $errors->has('cep_logradouro') ? $errors->first('cep_logradouro') : '' }}               
                </div> 

                <div class="col-md-12" style="margin-left:-10px;">
                    <label for="inputObservacao" class="form-label">Observação</label>   
                    <textarea name="observacao" class="ajuste form-control">{{ $logradouro->observacao_logradouro ?? old('observacao') }}</textarea>                    
                </div>   

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">ALTERAR</button>
                </div>                    
                      
                      
            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection