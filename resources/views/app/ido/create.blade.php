@extends('site.layouts.basico')

    @section('titulo', 'Objeto IDO')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header">Objeto IDO - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="POST" action="{{ route('ido.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf                

                <div class="col-md-2" style="">       
                    <label for="inputData" class="form-label">DATA</label>   
                    {{-- Pega a Data Atual e coloca como Padrão --}}
                    <input type="date" name="data" value="{{ date('Y-m-d') }}" class="form-control texto-caixa-alta" id="data">                    
                    {{ $errors->has('data') ? $errors->first('data') : '' }}
                </div>                                

                <div class="col-md-2" style="">       
                    <label for="inputUsuario" class="form-label">LANÇADO POR :</label>                             
                    <input type="text" name="usuario" value="{{ $usuario }}" class="form-control texto-caixa-alta" id="usuario" readonly>                                        
                </div>  
                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoIdo" class="form-label">ANO IDO</label>   
                    <input type="text" name="ano_ido" value="" class="form-control texto-caixa-alta"> 
                    {{ $errors->has('ano_ido') ? $errors->first('ano_ido') : '' }}
                </div> 
                                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputScm" class="form-label">SCM</label>   
                    <input type="text" name="scm_ido" value="" class="form-control texto-caixa-alta"> 
                    {{ $errors->has('scm_ido') ? $errors->first('scm_ido') : '' }}
                </div>     
                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoScm" class="form-label">ANO SCM</label>   
                    <input type="text" name="ano_scm" value="" class="form-control texto-caixa-alta">                     
                    {{ $errors->has('ano_scm') ? $errors->first('ano_scm') : '' }}
                </div> 

                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputModalidade" class="form-label">MODALIDADE</label>   
                                        
                    <select name="id_modalidade_ido" id="modalidade" class="texto-caixa-alta">                        
                        @foreach ($modalidades as $modalidade)
                        <option value="{{ $modalidade->id }}">{{ $modalidade->nome_modalidade }}</option>
                        @endforeach
                    </select>  
                </div>

                <div class="col-md-2" style="margin-top:10px;">
                    <label for="inputNumero" class="form-label">NÚMERO</label>   
                    <input type="text" name="numero_ido" value="" class="form-control texto-caixa-alta" id="numero"> 
                    {{ $errors->has('numero_ido') ? $errors->first('numero_ido') : '' }}
                </div>       
                  
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoPregao" class="form-label">ANO PREG</label>   
                    <input type="text" name="ano_pregao_ido" value="" class="form-control texto-caixa-alta" id="anopregao"> 
                    {{ $errors->has('ano_pregao_ido') ? $errors->first('ano_pregao_ido') : '' }}
                </div> 

                <div class="col-md-2" style="margin-top:10px;">
                    <label for="inputProcesso" class="form-label">PROCESSO</label>   
                    <input type="text" name="processo_ido" value="" class="form-control texto-caixa-alta" id="processo_ido"> 
                    {{ $errors->has('processo_ido') ? $errors->first('processo_ido') : '' }}
                </div> 
                
                <div class="row" style="padding-right: 18px;">
                    <label for="inputObjeto" class="form-label">OBJETO</label>   
                    <textarea name="objeto" class="ajuste form-control"></textarea>
                    {{ $errors->has('objeto') ? $errors->first('objeto') : '' }}
                </div> 

                <div class="row" style="margin-top:10px; padding-right: 18px;">
                    <label for="inputDemanda" class="form-label">DEMANDA</label>   
                    <textarea name="demanda" class="ajuste form-control"></textarea>
                    {{ $errors->has('demanda') ? $errors->first('demanda') : '' }}
                </div> 

                <div class="row">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff; width:160px; margin-top:20px; margin-left:7px;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection