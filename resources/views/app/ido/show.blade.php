@extends('site.layouts.basico')

    @section('titulo', 'Lançamentos de Objeto IDO')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Lançamentos de Objeto IDO</h1>   
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="POST" action="{{ route('lancamento_ido.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf                
                            
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputIdo" class="form-label">IDO</label>   
                    <input type="text" name="ido_id" value="{{ $ido->id }}" class="form-control texto-caixa-alta" id="ido" readonly> 
                    {{ $errors->has('ido_id') ? $errors->first('ido_id') : '' }}
                </div> 
                        
                <div class="col-md-2" style="">       
                    <label for="inputData" class="form-label">DATA LANÇAMENTO</label>   
                    {{-- Pega a Data Atual e coloca como Padrão --}}
                    <input type="date" name="" value="{{ $ido->data }}" class="form-control texto-caixa-alta" id="data" readonly>                                        
                </div>                                

                <div class="col-md-2" style="">       
                    <label for="inputUsuario" class="form-label">LANÇADO POR :</label>                             
                    <input type="text" name="" value="{{ $usuario }}" class="form-control texto-caixa-alta" id="usuario" readonly>                                        
                </div>    
                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoIdo" class="form-label">ANO IDO</label>   
                    <input type="text" name="" value="{{ $ido->ano_ido }}" class="form-control texto-caixa-alta" id="anoido" readonly>                      
                </div> 
                                                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputFicha" class="form-label">FICHA</label>   
                    <input type="text" name="ficha_ido" value="{{ old('ficha_ido') }}"  class="form-control texto-caixa-alta ui-autocomplete-input ui-autocomplete-loading campo-auto" id="mcc_search" autocomplete="off"> 
                    {{ $errors->has('ficha_ido') ? $errors->first('ficha_ido') : '' }}
                </div>       
                                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputScm" class="form-label">SCM</label>   
                    <input type="text" name="" value="{{  $ido->scm_ido }}" class="form-control texto-caixa-alta" id="scm" readonly>                     
                </div>                   
                
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoScm" class="form-label">ANO SCM</label>   
                    <input type="text" name="" value="{{  $ido->ano_scm }}" class="form-control texto-caixa-alta" id="anoscm" readonly>                     
                </div> 
                
                <div class="col-md-3" style="margin-top:10px;">
                    <label for="inputModalidade" class="form-label">MODALIDADE</label>   
                    <input type="text" name="" value="{{  $ido->modalidade->nome_modalidade }}" class="form-control" readonly style="font-size: 14px !important;">
                </div>

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputNumero" class="form-label">NÚMERO</label>   
                    <input type="text" name="" value="{{  $ido->numero_ido }}" class="form-control texto-caixa-alta" id="numero" readonly>                     
                </div>       
                  
                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputAnoPregao" class="form-label">ANO PREG</label>   
                    <input type="text" name="" value="{{  $ido->ano_pregao_ido }}" class="form-control texto-caixa-alta" id="anopregao" readonly>                     
                </div> 

                <div class="col-md-2" style="margin-top:10px;">
                    <label for="inputProcesso" class="form-label">PROCESSO</label>   
                    <input type="text" name="" value="{{  $ido->processo_ido }}" class="form-control texto-caixa-alta" id="processo_ido" readonly>                      
                </div> 

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputUo" class="form-label">UO</label>   
                    <input type="text" name="uo" value="" class="form-control texto-caixa-alta" id="mccuo" readonly> 
                    {{ $errors->has('uo') ? $errors->first('uo') : '' }}
               </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputUa" class="form-label">UA</label>   
                    <input type="text" name="ua" value="" class="form-control texto-caixa-alta" id="mccua" readonly> 
                    {{ $errors->has('ua') ? $errors->first('ua') : '' }}
               </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputFuncao" class="form-label">FUNÇÃO</label>   
                    <input type="text" name="funcao" value="" class="form-control texto-caixa-alta" id="mccfuncao" readonly> 
                    {{ $errors->has('funcao') ? $errors->first('funcao') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputFuncao" class="form-label">SUBFUNÇÃO</label>   
                    <input type="text" name="subfuncao" value="" class="form-control texto-caixa-alta" id="mccsubfuncao" readonly> 
                    {{ $errors->has('subfuncao') ? $errors->first('subfuncao') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputPrograma" class="form-label">PROGRAMA</label>   
                    <input type="text" name="programa" value="" class="form-control texto-caixa-alta" id="mccprograma" readonly> 
                    {{ $errors->has('programa') ? $errors->first('programa') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputProjativ" class="form-label">AÇÃO</label>   
                    <input type="text" name="projativ" value="" class="form-control texto-caixa-alta" id="mccprojativ" readonly> 
                    {{ $errors->has('projativ') ? $errors->first('projativ') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputSubAcao" class="form-label">SUBAÇÃO</label>   
                    <input type="text" name="subacao" value="{{ old('subacao') }}" class="form-control texto-caixa-alta" id="subacao"> 
                    {{ $errors->has('subacao') ? $errors->first('subacao') : '' }}
                </div> 

                <div class="col-md-2" style="margin-top:10px;">
                    <label for="inputNaturezaDespesa" class="form-label">NAT. DESPESA</label>   
                    <input type="text" name="natureza_despesa" value="" class="form-control texto-caixa-alta" id="mccnaturezadespesa" readonly> 
                    {{ $errors->has('natureza_despesa') ? $errors->first('natureza_despesa') : '' }}
                </div> 
                
                {{-- Input transformado em Botão. Implementado Botão que carrega os Itens da Natureza carregada pelo campo Ficha da Tabela MCCS --}}                
                <div class="col-md-2" style="margin-top:42px;">
                    <input type="button" id="carrega_itens" class="btn btn-success"value="CARREGA ITENS">                         
                </div>                                

                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputItem" class="form-label" style="">ITEM</label>                       
                    <select name="item" class="form-control select-11" id="item_search">
                    {{-- Aqui serão injetadas as options por Jquery carregado na view basico.blade.php --}}
                    </select>                    
                </div>                

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputFonte" class="form-label" style="">FONTE</label>   
                    <input type="text" name="fonte" value="" class="form-control texto-caixa-alta" id="mccfonte" readonly> 
                    {{ $errors->has('fonte') ? $errors->first('fonte') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputFonteDetalhe" class="form-label" style="">F. DETALHE</label>   
                    <input type="text" name="fonte_detalhe" value="" class="form-control texto-caixa-alta" id="mccfontedetalhe" readonly> 
                    {{ $errors->has('fonte_detalhe') ? $errors->first('fonte_detalhe') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputGrupo" class="form-label" style="">GRUPO</label>   
                    <input type="text" name="grupo" value="{{ old('grupo_ido') }}" class="form-control texto-caixa-alta" id="grupo"> 
                    {{ $errors->has('grupo') ? $errors->first('grupo') : '' }}
                </div>                   

                <div class="col-md-1" style="margin-top:10px;">
                    <label for="inputSicom" class="form-label" style="">SICOM</label>   
                    <input type="text" name="sicom" value="{{ old('sicom') }}" class="form-control texto-caixa-alta" id="sicom"> 
                    {{ $errors->has('sicom') ? $errors->first('sicom') : '' }}
                </div>                   

                <div class="col-md-6" style="margin-top:10px;">
                    <label for="inputDescItemNatureza" class="form-label" style="">DESCRIÇÃO ITEM DA NATUREZA DESPESA</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="descitemnatureza"> 
                    {{ $errors->has('descitemnatureza') ? $errors->first('descitemnatureza') : '' }}
                </div>  

                <div class="col-md-6" style="margin-top:10px;">
                    <label for="inputRecurso" class="form-label" style="">RECURSO</label>   
                    
                    <select name="recurso" id="recurso" class="texto-caixa-alta">                        
                        @foreach ($recursos as $recurso)
                        <option value="{{ $recurso->codigo_recurso }}">{{ $recurso->nome_recurso }}</option>
                        @endforeach
                    </select>
                    {{ $errors->has('recurso') ? $errors->first('recurso') : '' }}
                </div>                   

                <div class="col-md-3" style="margin-top:10px;">
                    <label for="inputResolucao" class="form-label" style="">RESOLUÇÃO</label>   
                    <input type="text" name="resolucao" value="" class="form-control texto-caixa-alta"> 
                    {{ $errors->has('') ? $errors->first('') : '' }}
                </div>

                <div class="col-md-2" style="margin-top:10px;">
                    <label for="inputConta" class="form-label" style="">CONTA</label>   
                    <input type="text" name="conta" value="" class="form-control texto-caixa-alta"> 
                    {{ $errors->has('') ? $errors->first('') : '' }}
                </div> 

                <div class="col-md-3" style="margin-top:10px;">
                    <label for="inputPortaria" class="form-label" style="">PORTARIA</label>   
                    <input type="text" name="portaria" value="" class="form-control texto-caixa-alta"> 
                    {{ $errors->has('') ? $errors->first('') : '' }}
                </div>  
                
                <div class="col-md-6" style="">
                    <label for="inputObjeto" class="form-label">OBJETO</label>   
                    <textarea name="" class="ajuste form-control" style="height:110px;" readonly>{{ $ido->objeto }}</textarea>
                    {{ $errors->has('objeto') ? $errors->first('objeto') : '' }}
                </div> 

                <div class="col-md-6" style="padding-right: 18px;">
                    <label for="inputDemanda" class="form-label">DEMANDA</label>   
                    <textarea name="" class="ajuste form-control" style="height:110px;" readonly>{{ $ido->demanda }}</textarea>
                    {{ $errors->has('demanda') ? $errors->first('demanda') : '' }}
                </div> 
                
                <div style="float: left;">
                    <hr>
                    <label style="background-color:#b07d62; float: left; padding-top:10px; padding-bottom:10px; margin-top:-8px; width:100%; border-radius: 2px 4px 2px;">DE</label>
                </div>

                {{-- Nome do Usuário e Setor de Origem para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputNome1" class="form-label" style="">NOME</label>   
                
                    <select name="bm_origem" id="nome1" value="" class="texto-caixa-alta form-control">  
                        <option value="" selected disabled hidden>Selecione um Remetente</option>                      
                        @foreach ($origem as $o)
                            <option value="{{ $o->sigla_setor . ' - ' . $o->nome_setor }}">{{ $o->bm . ' - ' . $o->nome_usuario }}</option>                            
                        @endforeach
                    </select>
                    {{ $errors->has('bm_origem') ? $errors->first('bm_origem') : '' }}
                </div>                   

                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetor1" class="form-label" style="">SETOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor1" style="font-size:13px; height:38px;">                     
                </div>                    
                
                <div class="col-md-1" style="margin-top:10px;">                     
                    <input type="hidden" name="bm_origem" value="" class="form-control texto-caixa-alta" id="bm1">                     
                </div>
                
                <div style="float: left;">
                    <hr>
                    <label style="background-color:#b07d62; float: left; padding-top:10px; padding-bottom:10px; margin-top:-8px; width:100%; border-radius: 2px 4px 2px;">PARA</label>
                </div>

                {{-- Nome do Usuário e Setor de Destino para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputNome2" class="form-label" style="">NOME</label>   
                    
                    <select name="bm_destino" value="" id="nome2" class="texto-caixa-alta form-control">   
                        <option value="" selected disabled hidden>Selecione um Destinatário</option>                     
                        @foreach ($origem as $o)                            
                            <option value="{{ $o->sigla_setor . ' - ' . $o->nome_setor }}">{{ $o->bm . ' - ' . $o->nome_usuario }}</option>                                                      
                        @endforeach
                        
                    </select>
                    {{ $errors->has('bm_destino') ? $errors->first('bm_destino') : '' }}
                </div>     
                
                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetor2" class="form-label" style="">SETOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor2" style="font-size:13px; height:38px;">                     
                </div>

                <div class="col-md-1" style="margin-top:10px;">                      
                    <input type="hidden" name="bm_destino" value="" class="form-control texto-caixa-alta" id="bm2">                     
                </div>
                
                <div style="float: left;">
                    <hr>
                    <label style="background-color:#b07d62; float: left; padding-top:10px; padding-bottom:10px; margin-top:-8px; width:100%; border-radius: 2px 4px 2px;">ORDENADOR</label>
                </div>
                              
                {{-- Nome do Ordenador1 e Setor1 para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputOrdenador1" class="form-label" style="">PRIMEIRO ORDENADOR</label>   
                    
                    <select name="" id="ordenador1" class="texto-caixa-alta form-control">  
                        <option value="" selected disabled hidden>Selecione o Primeiro Ordenador</option>                      
                        @foreach ($ordenadores as $ordenador)
                            <option value="{{ $ordenador->sigla_setor. ' - '. $ordenador->nome_setor }}">{{ $ordenador->matricula_ordenador . ' - '. $ordenador->nome_ordenador }}</option>                            
                        @endforeach
                    </select>
                    {{ $errors->has('ord1') ? $errors->first('ord1') : '' }}
                </div>    
                 
                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetorOrd1" class="form-label" style="">SETOR PRIMEIRO ORDENADOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor_ordenador1" style="font-size:13px; height:38px;">                     
                </div> 
                 
                <div class="col-md-1" style="margin-top:10px;">                      
                    <input type="hidden" name="matricula_ordenador1" value="" class="form-control texto-caixa-alta" id="matricula_ordenador1"> 
                    {{ $errors->has('matricula_ordenador1') ? $errors->first('matricula_ordenador1') : '' }}
                </div>                

                {{-- Nome do Ordenador2 e Setor2 para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputOrd2" class="form-label" style="">SEGUNDO ORDENADOR</label>   
                    
                    <select name="" id="ordenador2" class="texto-caixa-alta form-control">  
                        <option value="" selected disabled hidden>Selecione o Segundo Ordenador</option>                      
                        @foreach ($ordenadores as $ordenador)
                            <option value="{{ $ordenador->sigla_setor. ' - '. $ordenador->nome_setor }}">{{ $ordenador->matricula_ordenador . ' - '. $ordenador->nome_ordenador }}</option>                            
                        @endforeach
                    </select>
                    {{ $errors->has('ord2') ? $errors->first('ord2') : '' }}
                </div>    
                 
                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetorOrd2" class="form-label" style="">SETOR SEGUNDO ORDENADOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor_ordenador2" style="font-size:13px; height:38px;">                     
                </div> 
                 
                <div class="col-md-1" style="margin-top:10px;">                      
                    <input type="hidden" name="matricula_ordenador2" value="" class="form-control texto-caixa-alta" id="matricula_ordenador2"> 
                    {{ $errors->has('matricula_ordenador2') ? $errors->first('matricula_ordenador2') : '' }}
                </div>
                

                {{-- Nome do Ordenador3 e Setor3 para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputOrd3" class="form-label" style="">TERCEIRO ORDENADOR</label>   
                    
                    <select name="" id="ordenador3" class="texto-caixa-alta form-control">  
                        <option value="" selected disabled hidden>Selecione o Terceiro Ordenador</option>                      
                        @foreach ($ordenadores as $ordenador)
                            <option value="{{ $ordenador->sigla_setor. ' - '. $ordenador->nome_setor }}">{{ $ordenador->matricula_ordenador . ' - '. $ordenador->nome_ordenador }}</option>                            
                        @endforeach
                    </select>
                    {{ $errors->has('ord3') ? $errors->first('ord3') : '' }}
                </div>    
                 
                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetOrd3" class="form-label" style="">SETOR TERCEIRO ORDENADOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor_ordenador3" style="font-size:13px; height:38px;">                     
                </div> 
                 
                <div class="col-md-1" style="margin-top:10px;">                      
                    <input type="hidden" name="matricula_ordenador3" value="" class="form-control texto-caixa-alta" id="matricula_ordenador3">                     
                </div>
                

                {{-- Nome do Ordenador4 e Setor4 para a impressão do IDO --}}
                <div class="col-md-4" style="margin-top:10px;">
                    <label for="inputOrd4" class="form-label" style="">QUARTO ORDENADOR</label>   
                    
                    <select name="" id="ordenador4" class="texto-caixa-alta form-control">  
                        <option value="" selected disabled hidden>Selecione o Quarto Ordenador</option>                      
                        @foreach ($ordenadores as $ordenador)
                            <option value="{{ $ordenador->sigla_setor. ' - '. $ordenador->nome_setor }}">{{ $ordenador->matricula_ordenador . ' - '. $ordenador->nome_ordenador }}</option>                            
                        @endforeach
                    </select>
                    {{ $errors->has('ord4') ? $errors->first('ord4') : '' }}
                </div>    
                 
                <div class="col-md-5" style="margin-top:10px;">
                    <label for="inputSetOrd4" class="form-label" style="">SETOR QUARTO ORDENADOR</label>   
                    <input type="text" name="" value="" class="form-control texto-caixa-alta" id="setor_ordenador4" style="font-size:13px; height:38px;">                     
                </div> 
                
                <div class="col-md-1" style="margin-top:10px;">                      
                    <input type="hidden" name="matricula_ordenador4" value="" class="form-control texto-caixa-alta" id="matricula_ordenador4">                     
                </div>
                
                <div class="row">                    
                    <button type="" class="form-control btn-primary" style="color:#fff; width:260px; margin-top:20px;"><i class="fas fa-save" style="color:#000"></i><span style="padding-left:6px; color:#fff !important;">SALVAR LANÇAMENTO IDO</span></button>
                </div>                    

            </form>
            </div>

        </div>

    </div>  
   
</body>   

@endsection