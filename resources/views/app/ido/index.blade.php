@extends('site.layouts.basico')

    @section('titulo', 'Objeto IDO')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Ojeto IDO /Listar</h1>   
    </div>    

    <div class="conteudo-pagina-2">        

        <div class="container-fluid" style="margin-top:-40px;">
                 <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>                           
                            <th class="texto-caixa-alta">OBJETO IDO</th>
                            <th class="texto-caixa-alta">DATA</th>                            
                            <th class="texto-caixa-alta">OBJETO</th>                            
                            <th class="texto-caixa-alta">DEMANDA</th>                                                                                    
                            <th class="texto-caixa-alta">LANÇADO POR</th>                                                                                    
                            <th></th>                                                                                    
                            <th></th>                                                                                    
                            <th></th>    
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                                @foreach ($idos as $ido )
                                    <td style="text-transform:capitalize">{{ $ido->id }}</td>
                                    <td>{{ date('d/m/Y', strtotime($ido->data)) }}</td>
                                    <td style="text-transform:capitalize">{{ strlen($ido->objeto) < 30 ? $ido->objeto : mb_strimwidth($ido->objeto, 0, 35, "...") }}</td> 
                                    <td style="text-transform:capitalize">{{ strlen($ido->demanda) < 30 ? $ido->demanda : mb_strimwidth($ido->demanda, 0, 35, "...") }}</td>                                     
                                    <td style="text-transform:capitalize">{{ $ido->usuario }}</td>
                                    
                                    
                                    <td>
                                        <a href="{{ route('ido.show', $ido->id) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:180px !important">
                                            <i class="fa fa-file" style="color:#000;"></i> LANÇAR OBJETO IDO</a>
                                    </td>

                                    <td>
                                        <a href="#" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-trash-alt" style="color:#000;"></i> EXCLUIR</a>
                                    </td>

                                    <td>
                                        <a href="#" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-edit" style="color:#000;"></i> EDITAR</a>
                                    </td>
                            </tr>    
                                @endforeach                                                                                               

                    </tbody>
                </table>    
      
                <br>
                O Total de IDOs cadastrados para este Usuário são <strong>{{ $idos->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection