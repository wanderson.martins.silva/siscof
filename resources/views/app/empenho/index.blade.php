@extends('site.layouts.basico')

    @section('titulo', 'Empenhos')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5 alinhar_container">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Empenhos - Importação/Listagem dos Empenhos Importados</h1>                             
    </div>    

    <div class="conteudo-pagina">

         <div class="container">
             <div class="card bg-light mt-3">
 
                 <div class="card-header header-importacao">
                     Importação de Empenhos
                 </div>
 
                 <div class="card-body">
                     <form action="{{ route('importEmpenho') }}" method="POST" enctype="multipart/form-data">
                         {{ csrf_field() }}                    
                         <div class="file">
                             
                             <input type="file" name="file" class="form-control" id="file">                    
                             <button class="btn btn-success" style="width:340px; float:left;">Importar Dados</button>
                             <span style="font-size:20px; color:red;">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                         </div>
                         
                     </form>
                 </div>            
             </div>
         </div>
        <div class="container" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover empenho">
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">Ano Empenho</th>
                            <th class="texto-caixa-alta">Dt Últ. Importação</th>                            
                            <!--
                            <th></th>
                            <th></th>
                            <th></th>
                            -->
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                                @foreach ($empenhos as $empenho )
                                <td>{{ $empenho->first()->ano_empenho }}</td>
                                <td>{{ $empenho->first()->created_at->format('d/m/Y H:i:s') }}</td>                                
                                    
                                @endforeach                                
                                <!--
                                <td><a href="">Visualizar</a></td>
                                <td><a href="">Excluir</a></td>
                                <td><a href="">Editar</a></td>
                                -->
                            </tr>   
                    </tbody>
                </table>    
      
                <br>
                O Empenho Importado tem o Total de <strong>{{ $empenhos->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection