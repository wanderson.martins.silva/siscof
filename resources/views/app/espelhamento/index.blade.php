@extends('site.layouts.basico')

@section('titulo', 'Espelhamento')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')
        <div class="container-fluid py-5">
            <h1 class="texto-header fw-bold texto-preto header">Fornecedores á Espelhar</h1>            
        </div>

        <div class="conteudo-pagina">

            <div class="container-fluid">
                
                <table class="table table-dark table-striped table-hover" id="dataTable">
                    
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">EMPENHO</th>
                            <th class="texto-caixa-alta">NOME PESSOA</th>
                            <th class="texto-caixa-alta">ANO EMPENHO</th>
                            <th class="texto-caixa-alta">UA</th>
                            <th class="texto-caixa-alta">VALOR EMPENHADO</th>                            
                            <th class="texto-caixa-alta">VALOR ANULADO</th>                            
                            <th class="texto-caixa-alta">SALDO ATUAL</th>                            
                            <th class="texto-caixa-alta">PROCESSO LICIT.</th>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($empenhos as $empenho)
                            <tr>
                                <td>{{ $empenho->empenho }}</td>
                                <td>{{ $empenho->nome_pessoa }}</td>
                                <td>{{ $empenho->ano_empenho }}</td>
                                <td>{{ $empenho->ua }}</td>
                                <td>{{ number_format($empenho->vl_empenhado, 2, ',', '.') }}</td>                                
                                <td>{{ number_format($empenho->vl_anulado, 2, ',', '.') }}</td>                                
                                <td>
                                    {{ $empenho->vl_empenhado == $empenho->vl_anulado ? "0,00" : number_format(($empenho->vl_empenhado - $empenho->total_despesas) - $empenho->vl_anulado, 2, ',', '.') }}
                                </td>                                
                                {{-- <td></td> --}}
                                    
                                <td>{{ $empenho->processo }}
                                <td><a class="btn btn-outline-primary btn-sm"
                                        href="{{ route('prepara_espelhamento.show', ['prepara_espelhamento' => $empenho->empenho]) }}">Espelhar</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>

        </div>

        </div>

    </body>

@endsection
