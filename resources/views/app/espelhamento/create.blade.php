@extends('site.layouts.basico')

    @section('titulo', 'Espelhamentos')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header">Espelhamentos - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="background-color:#f2f3de;">
            <ul>
                <li><a href = "{{ route('espelhamento.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('espelhamento.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-5" style="">       
                    <label for="inputFornecedor" class="form-label">NOME DO FORNECEDOR</label>                             
                    
                    <select name="fornecedor_id" class="form-select texto-caixa-alta">
                        <option selected>-- Selecione o Fornecedor --</option>
                                                                          
                            @foreach ($fornecedores as $fornecedor)
	                            <option value="{{ $fornecedor->id }}" {{ ($espelhamento->fornecedor_id ?? old('fornecedor->id')) == $fornecedor->id ? 'selected' : ''}}>{{ $fornecedor->nome }}</option>
                            @endforeach
                        
                    </select>
                </div>                
                
                <div class="col-md-5">
                    <label for="inputProcesso" class="form-label">PROCESSO LICITATÓRIO</label>   
                    <input name="processo_licitatorio" value="{{ old('processo_licitatorio') }}" class="ajuste form-control">
                    {{ $errors->has('processo_licitatorio') ? $errors->first('processo_licitatorio') : '' }}
                </div>

                <div class="col-md-5">
                    <label for="inputEmpenho" class="form-label">EMPENHO</label>   
                    <input name="" value="{{ old('cod_empenho') }}" class="ajuste form-control">
                    {{ $errors->has('cod_empenho') ? $errors->first('cod_empenho') : '' }}
                </div> 

                <div class="col-md-1">
                    <label for="inputAnoEmpenho" class="form-label">ANO</label>   
                    <input name="" value="{{ old('ano_exercisio') }}" class="ajuste form-control">
                    {{ $errors->has('ano_exercisio') ? $errors->first('ano_exercisio') : '' }}
                </div>

                <div class="col-auto">
                    <label for="inputFicha" class="form-label">FICHA</label>   
                    <input name="" value="{{ old('ficha') }}" class="ajuste form-control">
                    {{ $errors->has('ficha') ? $errors->first('ficha') : '' }}
                </div>    

                <div class="col-auto">
                    <label for="inputProcessoPag" class="form-label">PROCESSO PAGAMENTO</label>   
                    <input name="" value="{{ old('processo_pagamento') }}" class="ajuste form-control">
                    {{ $errors->has('processo_pagamento') ? $errors->first('processo_pagamento') : '' }}
                </div>    

                <div class="col-auto">
                    <label for="inputFicha" class="form-label">VALOR DESPESA</label>   
                    <input name="" value="{{ old('ficha') }}" class="ajuste form-control">
                    {{ $errors->has('ficha') ? $errors->first('ficha') : '' }}
                </div>    

                <div class="row">
                    <label for="inputObservacao" class="form-label">OBSERVAÇÃO</label>   
                    <textarea name="observacao_espelhamento" class="ajuste form-control"></textarea>
                    {{ $errors->has('observacao_espelhamento') ? $errors->first('observacao_espelhamento') : '' }}
                </div>                   

                <div class="col-md-12">                    
                    <button type="submit" class="form-control button" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection