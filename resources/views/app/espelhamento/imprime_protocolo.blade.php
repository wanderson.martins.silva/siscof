@extends('site.layouts.basico')

@section('titulo', 'Protocolo do Empenho')

@section('conteudo')

    <body>

        {{-- @include('site.layouts._partials.topo')         --}}

        <div class="alinhar_cabecalho-2">
            <div style="border:#000 1px solid; width:500px; height:250px; position: absolute; margin-top:30px; margin-left:20px;">

                <div class="container-fluid texto-container row alinhar_container" style="color: #000; font-weight: bold;">
                    <!-- INICIO FORM CABEÇALHO DO EMPENHO -->      
                                        
                    <div style="padding-top:20px;">                        
                            EMPENHO :
                            {{ $espelhamentos[0]->empenho }}

                            <span style="padding-left:20px;">
                                ANO :
                                {{ $espelhamentos[0]->ano_exercisio }}   
                            </span>
                    </div>    

                    <div class="col-md-12" style="padding-top:10px; padding-bottom:30px;">                        
                            DESPESA :      
                            {{-- Imprime o Total de Despesas --}}
                            R$ {{ number_format($totais[0]->total_despesas, 2, ',', '.') }}                        
                    </div>

                    <div class="col-md-12">
                        <span>                             
                            <p style="border:#000 1px solid; border-width: 1px;"></p>
                        </span>                        
                    </div>
                    
                    <div style="font-size:12px; margin-top: -30px;">
                        <span>
                            <select name="funcionario" class="select-sem-estilo">
                                <option selected>SELECIONE</option>
                                                                                
                                    @foreach ($funcionarios as $f)
                                        <option value="{{ $f }}">{{ $f }}</option>
                                    @endforeach
                                
                            </select>                            
                        </span>                        
                    </div>

                    <div class="col-md-12" style="padding-top:10px; margin-bottom:20px;">
                        <span style="padding-left:80px;">
                        DATA :     
                        {{ date('d/m/Y H:i:s', strtotime($espelhamentos[0]->created_at)) }}    
                        </span>                        
                    </div>
                </div>
            
        </div>
    </body>

@endsection