@extends('site.layouts.basico')

@section('titulo', 'Itens Natureza Despesas')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')
        <div class="container-fluid py-5">
            <h1 class="texto-header fw-bold texto-preto header">Itens Natureza Despesas /Listar</h1>
        </div>

        <div class="conteudo-pagina alinhar_cabecalho">

            <div class="container-fluid">
                <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">CÓDIGO NATUREZA</th>
                            <th class="texto-caixa-alta">NOME NATUREZA</th>
                            <th class="texto-caixa-alta">CÓDIGO ITEM</th>
                            <th class="texto-caixa-alta">NOME ITEM</th>
                            <th></th>
                            <th></th>                            
                            {{-- <th></th>                             --}}
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            @foreach ($itens_natureza_despesas as $item)
                                <td>{{ $item->codigo_natureza }} </td>                            
                                <td>{{ $item->nome_natureza }} </td>                            
                                <td>{{ $item->codigo_item }} </td>                            
                                <td>{{ $item->nome_item }} </td>                                                            
                                <td>
                                    <a 
                                        href="#" class="btn btn-sm btn-info" style="color: #fff !important;"><i class="fas fa-edit" style="color:#000;"></i> EDITAR
                                    </a>
                                </td>
                                <td>
                                    <a 
                                        href="#" class="btn btn-sm btn-danger" style="color: #fff !important;"><i class="fas fa-trash-alt" style="color:#000;"></i> EXCLUIR
                                    </a>
                                </td>              
                                {{-- <td>
                                    <form class="form_editar-2" id="form_" method="GET" action="">
                                        @csrf
                                        <input name="_method" type="hidden" value="" id="nome" data-value="natureza">                                          
                                        <a 
                                            href="javascript.void(0)" class="btn btn-sm btn-success enviar" style="color: #fff !important;"><i class="fas fa-plus-circle" style="color:#000;"></i> ADICIONAR ITEM DESPESA
                                        </a>                                        
                                    </form>    
                                </td> --}}
                        </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>

        </div>

        </div>
        

    </body>

@endsection
