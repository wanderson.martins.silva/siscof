@extends('site.layouts.basico')

    @section('titulo', 'Lançamentos de Itens da Natureza Despesas')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">ITEM NATUREZA DESPESAS - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('item_despesa.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
            
                <div class="col-md-1">
                    <label for="inputCodigoNatureza" class="form-label">CÓD. NAT.</label> 
                    <input type="text" name="codigo_natureza_despesa" value="{{ $natureza_despesa[0]->codigo_natureza }}" class="form-control" readonly>                    
                </div>

                <div class="col-md-5">  
                    <label for="inputNome" class="form-label">NOME DA NATUREZA</label>   
                    <input type="text" name="nome_natureza" value="{{ $natureza_despesa[0]->nome_natureza }}" class="form-control" readonly>                    
                </div> 

                <div class="col-md-1">  
                    <label for="inputCodItem" class="form-label">CÓD. ITEM</label>   
                    <input type="text" name="codigo_item" value="" class="form-control">                    
                </div> 

                <div class="col-md-5">  
                    <label for="inputNomeItem" class="form-label">NOME DO ITEM</label>   
                    <input type="text" name="nome_item" value="" class="form-control">                    
                </div> 

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>
            <div class="container-fluid" style="margin-top:10px;">
                <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">CÓDIGO DA NATUREZA</th>
                            <th class="texto-caixa-alta">NOME DA  NATUREZA</th>
                            <th class="texto-caixa-alta">CÓDIGO DO ITEM</th>
                            <th class="texto-caixa-alta">NOME DO ITEM</th>                                                        
                            {{-- <th></th>
                            <th></th>
                            <th></th> --}}
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($naturezas_despesas as $natureza)
                        <tr>
                            <td>{{ $natureza->codigo_natureza }}</td>
                            <td>{{ $natureza->nome_natureza }}</td>
                            <td>{{ $natureza->codigo_item }}</td>
                            <td>{{ $natureza->nome_item }}</td>
                        </tr>                            
                        @endforeach
                    </tbody>    
                </table>        
            </div>            

        </div>

    </div>   

</body>   

@endsection