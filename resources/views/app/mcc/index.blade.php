@extends('site.layouts.basico')

    @section('titulo', 'MCCs')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5 alinhar_container">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">MCCs - Importação/Listagem dos MCCs Importadas</h1>                             
    </div>    

    <div class="conteudo-pagina">

         <div class="container">
             <div class="card bg-light mt-3">
 
                 <div class="card-header header-importacao">
                     Importação de MCCs
                 </div>
 
                 <div class="card-body">
                     <form action="{{ route('importMcc') }}" method="POST" enctype="multipart/form-data">
                         {{ csrf_field() }}                    
                         <div class="file">
                             
                             <input type="file" name="file" class="form-control" id="file">                    
                             <button class="btn btn-success" style="width:340px; float:left;">Importar Dados</button>
                             <span style="font-size:20px; color:red;">{{ $errors->has('file') ? $errors->first('file') : '' }}</span>
                         </div>
                         
                     </form>
                 </div>            
             </div>
         </div>
        <div class="container" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover empenho">
                    <thead>
                        <tr>
                            {{-- <th class="texto-caixa-alta">Ano Mcc</th> --}}
                            <th class="texto-caixa-alta">Data Última Importação</th>                            
                            <!--
                            <th></th>
                            <th></th>
                            <th></th>
                            -->
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                                @foreach ($mccs as $mcc )
                                {{-- <td>{{ $empenho->first()->ano_empenho }}</td> --}}
                                <td>{{ $mcc->first()->created_at->format('d/m/Y H:i:s') }}</td>                                
                                    
                                @endforeach                                
                                <!--
                                <td><a href="">Visualizar</a></td>
                                <td><a href="">Excluir</a></td>
                                <td><a href="">Editar</a></td>
                                -->
                            </tr>   
                    </tbody>
                </table>    
      
                <br>
                O MCC Importado tem o Total de <strong>{{ $mccs->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection