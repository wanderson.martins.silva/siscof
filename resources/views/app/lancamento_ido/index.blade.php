@extends('site.layouts.basico')

    @section('titulo', 'LANÇAMENTOS IDO')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">LANÇAMENTOS OBJETO IDO /Listar</h1>   
    </div>    

    <div class="conteudo-pagina-2">        

        <div class="container-fluid" style="margin-top:-30px;">
                 <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>                           
                            <th class="texto-caixa-alta">LANCAMENTO</th>
                            <th class="texto-caixa-alta">IDO</th>
                            <th class="texto-caixa-alta">ANO IDO</th>                            
                            <th class="texto-caixa-alta">FICHA</th>  
                            <th class="texto-caixa-alta">RESOLUÇÃO</th>    
                            <th class="texto-caixa-alta">CONTA</th>    
                            <th class="texto-caixa-alta">PORTARIA</th>    
                            <th class="texto-caixa-alta">PROCESSO</th>    
                            {{--                                                                                 
                            <th></th>                                                                                    
                            <th></th>                                                                                    
                            <th></th>     --}}
                        </tr>
                    </thead>
                    <tbody>                        
                        
                            <tr>
                                @foreach ($lancamentos as $lancamento )
                                    <td style="text-transform:capitalize">{{ $lancamento->id }}</td>
                                    <td>{{ $lancamento->ido_id }}</td>
                                    <td>{{ $lancamento->ano_ido }}</td>                                    
                                    <td>{{ $lancamento->ficha_ido }}</td>                                                                      
                                    <td>{{ $lancamento->resolucao }}</td>
                                    <td>{{ $lancamento->conta }}</td>
                                    <td>{{ $lancamento->portaria }}</td>
                                    <td>{{ $lancamento->processo_ido }}</td>
                                    
                                    {{-- 
                                    <td>
                                        <a href="{{ route('ido.show', $ido->id) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:160px !important">
                                            <i class="fa fa-file" style="color:#000;"></i> LANÇAR IDO e TA</a>
                                    </td>

                                    <td>
                                        <a href="#" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-trash-alt" style="color:#000;"></i> EXCLUIR</a>
                                    </td>

                                    <td>
                                        <a href="#" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px !important">
                                            <i class="fas fa-edit" style="color:#000;"></i> EDITAR</a>
                                    </td> --}}
                            </tr>    
                                @endforeach                                                                                               

                    </tbody>
                </table>    
      
                <br>
                {{-- O Total de IDOs cadastrados para este Usuário são <strong>{{ $idos->total() }}</strong> Registro(s) --}}
                
        </div>

        </div>

    </div>   

</body>   

@endsection