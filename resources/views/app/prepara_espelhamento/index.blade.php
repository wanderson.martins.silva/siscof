@extends('site.layouts.basico')

@section('titulo', 'Espelhamento')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')
        <div class="container-fluid py-5">
            <h1 class="texto-header fw-bold texto-preto header">Espelhamentos Realizados</h1>
        </div>

        <div class="conteudo-pagina">

            <div class="container-fluid texto-container" style="padding-top:10px;">                               
                <div class="container-fluid" style="padding-top:10px;">
                    <table class="table table-dark table-striped table-hover" id="dataTable">
                        <thead>
                            <tr>                                
                                <th class="texto-caixa-alta">EMPENHO</th>
                                <th class="texto-caixa-alta">FORNECEDOR</th>
                                <th class="texto-caixa-alta">ANO EXERCÍCIO</th>                                
                                {{-- <th class="texto-caixa-alta">PROCESSO PAGAMENTO</th>                                 --}}
                                <th class="texto-caixa-alta">VALOR DESPESA</th>
                                <th class="texto-caixa-alta">UA</th>                                
                                <th class="texto-caixa-alta">DATA LANÇAMENTO</th>                                
                                <th class="texto-caixa-alta">Nº LANÇAMENTO</th>                                
                                <th></th>
                                <th></th>
                                <th></th>
    
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($espelhamentos as $espelhamento)
                                <tr>                                    
                                    <td>{{ $espelhamento->empenho }}</td>
                                    <td>{{ $espelhamento->nome }}</td>
                                    <td>{{ $espelhamento->ano_exercisio }}</td>
                                    {{-- <td>{{ $espelhamento->processo_licitatorio }}</td> --}}                                  
                                    <td>{{ number_format($espelhamento->valor_despesa, 2, ',', '.') }}</td>                                    
                                    <td>{{ $espelhamento->ua }}</td>                                    
                                    <td>{{  date('d/m/Y H:i:s', strtotime($espelhamento->created_at)) }}</td>                                                                        
                                    <td>{{ $espelhamento->id }}</td>
                                    <td>
                                        <a href="{{ route('prepara_espelhamento.show', ['prepara_espelhamento' => $espelhamento->empenho]) }}" class="btn btn-sm btn-success" style="color: #fff !important; font-size: 12px !important; width:110px !important">
                                            <i class="far fa-eye" style="color:#000;"></i> VISUALIZAR
                                        </a>
                                    </td>
                                    <td>
                                        {{-- Utilizado o Width padrão de 120px para o tamanho dos botões e form de exclusao --}}
                                    <form class="form_exclusao-2" id="form_{{ $espelhamento->id }}" method="POST" action="{{ route('espelhamento.destroy', ['espelhamento' => $espelhamento->id]) }}">                                        
                                        @csrf                                       
                                        <input name="_method" type="hidden" value="DELETE" id="nome" data-value="Espelhamento">
                                        <a href="javascript.void(0)" class="btn btn-sm btn-danger show-alert-delete-box" style="float:left; margin-top:-1px; color:#fff !important; width:100px; font-size: 12px !important;"><i class="far fa-trash-alt" style="color:#000; padding-right:4px;"></i>DELETE</a>

                                    </form>    
                                    </td>

                                    <td>
                                        <form class="form_editar-2" id="form_{{ $espelhamento->id }}" method="GET" action="{{ route('prepara_espelhamento.edit', 'espelhamento', $espelhamento->id) }}">
                                            @csrf

                                            <input name="_method" type="hidden" value="{{ $espelhamento->id }}" id="nome" data-value="Espelhamento">    
                                            <a href="javascript.void(0)" class="btn btn-sm btn-primary enviar" style="color: #fff !important; font-size: 12px !important; width:100px;">
                                                <i class="fas fa-edit" style="color:#000;"></i> EDITAR
                                            </a>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
    
                        </tbody>
                    </table>  
               
            </div>            

        </div>

        </div>

    </body>

@endsection

