@extends('site.layouts.basico')

@section('titulo', 'Espelhamento')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')        

        <div class="conteudo-pagina alinhar_cabecalho-2">

            <div class="container-fluid texto-container cor-container row alinhar_container">
                <!-- INICIO FORM CABEÇALHO DO EMPENHO -->
                <form method="post" action="{{ route('prepara_espelhamento.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >
                    @csrf

                    <!-- CABEÇALHO DO EMPENHO -->
                    <div class="col-md-12" style="padding-top: 90px;">                        
                        <label class="form-label fonte-cabecalho">CABEÇALHO DO ESPELHAMENTO</label>
                    </div>
                    
                    <hr class="ajuste-hr">
                    
                    <div class="col-md-7">
                        <label for="inputNomePessoa" class="form-label">NOME FORNECEDOR</label> 
                        <input type="text" name="nome_pessoa[]" value="{{ $empenhos->first()->nome_pessoa }}" class="form-control" readonly="true">
                        {{ $errors->has('nome_pessoa') ? $errors->first('nome_pessoa') : '' }}
                    </div>
                     
                    <div class="col-md-3">
                        <label for="inputProcessoLicitatorio" class="form-label">PROCESSO LICITATÓRIO</label> 
                        <input type="text" name="processo_licitatorio[]" value="{{ $empenhos->first()->processo }}" class="form-control" readonly="true">
                        {{ $errors->has('processo_licitatorio') ? $errors->first('processo_licitatorio') : '' }}
                    </div>
                                         
                    <div class="col-md-2">
                        <label for="inputVlEmpenhado" class="form-label" id="valor_empenhado">VLR EMPENHADO</label> 
                        <input type="text" name="valor_empenhado[]" value="{{ number_format($empenhos->first()->vl_empenhado, 2, ",", ".") }}" class="form-control" readonly="true">
                        {{ $errors->has('valor_empenhado') ? $errors->first('valor_empenhado') : '' }}                        
                    </div> 

                    <div class="col-md-2">
                        <label for="inputVlAnulado" class="form-label">VALOR ANULADO</label> 
                        <input type="text" name="vl_anulado[]" value="{{  number_format($empenhos->first()->vl_anulado, 2, ",", ".") }}" class="form-control" readonly="true">
                        {{ $errors->has('vl_anulado') ? $errors->first('vl_anulado') : '' }}
                    </div>

                    <div class="col-md-3">
                        <label for="inputFornecedorId" class="form-label">CÓD. PESSOA/FORNECEDOR</label> 
                        <input type="text" name="fornecedor_id[]" value="{{ $empenhos->first()->codigo_pessoa }}" class="form-control" readonly="true">
                        {{ $errors->has('fornecedor_id') ? $errors->first('fornecedor_id') : '' }}
                    </div>
                    
                    <div class="col-md-1">
                        <label for="inputEmpenho" class="form-label">EMPENHO</label> 
                        <input type="text" name="empenho[]" value="{{ $empenhos->first()->empenho }}" class="form-control" readonly="true" id="empenho">
                        {{ $errors->has('empenho') ? $errors->first('empenho') : '' }}
                    </div>

                    <div class="col-md-1">
                        <label for="inputUa" class="form-label">UA</label> 
                        <input type="text" name="ua[]" value="{{ $empenhos->first()->ua }}" class="form-control" readonly="true" id="ua">
                        {{ $errors->has('ua') ? $errors->first('ua') : '' }}
                    </div>
                                           
                    <div class="col-md-1">
                        <label for="inputFicha" class="form-label">FICHA</label> 
                        <input type="text" name="ficha[]" value="{{ $empenhos->first()->ficha }}" class="form-control" readonly="true">
                        {{ $errors->has('ficha') ? $errors->first('ficha') : '' }}
                    </div>

                    <div class="col-md-1">
                        <label for="inputSubAcao" class="form-label">SUBAÇÃO</label> 
                        <input type="text" name="subacao[]" value="{{ $empenhos->first()->sub_acao }}" class="form-control" readonly="true">
                        {{ $errors->has('subacao') ? $errors->first('subacao') : '' }}
                    </div>
                    
                    <div class="col-md-1">
                        <label for="inputAno" class="form-label">ANO</label> 
                        <input type="text" name="ano_exercisio[]" value="{{ $empenhos->first()->ano_empenho }}" class="form-control" readonly="true">
                        {{ $errors->has('ano_exercisio') ? $errors->first('ano_exercisio') : '' }}
                    </div>      

                    <div class="col-md-2">
                        <label for="inputInstrumentoJuridico" class="form-label">INST. JURÍDICO</label> 
                        <input type="text" name="instrumento_juridico[]" value="{{ $empenhos->first()->instrumento_juridico }}" class="form-control" readonly="true">
                        {{ $errors->has('instrumento_juridico') ? $errors->first('instrumento_juridico') : '' }}
                    </div>                   
                    
                    <hr class="hr-invisivel">                    

                    <hr class="ajuste-hr">                    
                    
                    <div class="col-md-2">
                        <label for="inputDtLancamento" class="form-label">DATA LANÇAMENTO</label> 
                        <input type="text" name="data[]" value="{{ $data }}" class="form-control" id="created_at" readonly="true">
                        {{ $errors->has('created_at') ? $errors->first('created_at') : '' }}
                    </div>                       

                   <div class="col-md-3">
                        <label for="inputProcessoPagamento" class="form-label">PROCESSO PAGAMENTO</label> 
                        <input type="text" name="processo_pagamento[]" value="" class="form-control removeLater" id="processo_pagamento">
                        {{ $errors->has('processo_pagamento') ? $errors->first('processo_pagamento') : '' }}
                    </div>   
                    
                    <div class="col-md-2">
                        <label for="inputNF" class="form-label">NOTA FISCAL</label> 
                        <input type="text" name="nota_fiscal[]" value="" class="form-control removeLater" id="nota_fiscal">
                        {{ $errors->has('nota_fiscal') ? $errors->first('nota_fiscal') : '' }}
                    </div>                    

                    <div class="col-md-3">
                        <label for="inputValorDespesa" class="form-label">VALOR DESPESA</label> 
                        <input type="text" name="valor_despesa[]" value="" class="form-control removeLater" id="valor_despesa">                        
                        {{ $errors->has('valor_despesa') ? $errors->first('valor_despesa') : '' }}
                    </div>
                                        
                    <div class="row">
                        <label for="inputObservacao" class="form-label" style="margin-top:20px;">OBSERVAÇÕES</label>                         
                        <textarea name="observacao_espelhamento[]" class="ajuste form-control removeLater" id="observacao_espelhamento"></textarea>
                        {{ $errors->has('observacao_espelhamento') ? $errors->first('observacao_espelhamento') : '' }}                        
                    </div> 

                    <div>
                        <input type="hidden" name="numero_espelhamento[]" value=""> 
                    </div>                        
                    
                    <div>
                        <input type="hidden" name="numero_credor[]" value="">
                    </div>  

                    <div class="col-md-3" style="margin-top:-15px;">
                        <button type="submit" class="btn-primary form-control"><i class="fas fa-save" style="color:#000"></i> SALVAR LANÇAMENTO</button>                        
                    </div>

                    <i class="fa-solid fa-floppy-disk-circle-arrow-right"></i>
                    <div class="cold-md-12">                        
                        <label class="form-label fonte-cabecalho">LANÇAMENTOS REALIZADOS</label>  
                    </div>

                    <div class="row">
                        
                        <div class="col-md-3 card" style="background-color:#3a86ff; font-weight: bold">
                            VALOR EMPENHADO <span style="color: aliceblue">{{ number_format($empenhos->first()->vl_empenhado, 2, ',', '.') }}</span>                        
                        </div>            
                        
                        <div class="col-md-3 card" style="background-color:#f28482; font-weight: bold">   
                            <!-- Utilizei um ternário para quando houver Valor nos Totais será apresentado caso Contrário mostrará 0 -->                         
                            TOTAIS DAS DESPESAS <span style="color: aliceblue">{{ $totais == NULL ? 0 : number_format($totais[0]->total_despesas, 2, ',', '.') }}</span>
                        </div>
                        
                        <div class="col-md-3 card" style="background-color:#d4a373; font-weight: bold">
                            <!-- Utilizei um ternário para quando houver Valor nos Totais será apresentado caso Contrário mostrará 0 -->                         
                            VALOR ANULADO <span style="color: aliceblue">{{ number_format($empenhos->first()->vl_anulado, 2, ',','.') }}</span>                        
                        </div>   
                    
                        <div class="col-md-3 card" style="background-color:#2d6a4f; font-weight: bold">
                            <!-- Utilizei um ternário para quando houver Valor nos Totais será apresentado caso Contrário mostrará 0 -->                         
                            SALDO ATUAL <span style="color: aliceblue">{{ number_format($empenhos->first()->vl_empenhado - $empenhos->first()->vl_anulado - $despesa[0]->despesas, 2, ',', '.') }}                        
                        </div>        
                        
                    </div>
                        
                    <hr>
    
                </div>            
                    
                </form>   
                <!-- FIM FORM LANÇAMENTOS -->           

            {{-- Simoni e Amanda pediram para tentar integrar uma forma de imprimir a soma do que estiver selecionado, caso existam processos de pagamentos de mesmo número --}}

            {{-- <div class="col-md-3 alinhar_container" style="margin-top:40px; margin-bottom:50px;">
                <form method="POST" action="{{ route('imprimir_protocolo') }}"> 
                    @csrf
                    <input type="hidden" name="{{ $empenhos->first()->empenho }}">

                    <button type="submit" class="btn-warning form-control">
                        <i class="fas fa-stamp" style="color:#000"></i> IMPRIMIR PROTOCOLO
                    </button>
                </form>                        
            </div>    --}}

            <div class="container-fluid" style="padding-top:50px;">
            
                <table class="table table-striped" id="dataTable" style="padding-top:10px;"> 
                    <thead>
                        <tr>
                            <th>DATA LANÇAMENTO</th>
                            <th>DATA ALTERAÇÃO</th>
                            <th>PROCESSO PAGAMENTO</th>
                            <th>NOTA FISCAL</th>
                            <th>VALOR DESPESA</th>                            
                            <th>Nº LANÇAMENTO</th>                            
                                              
                            <th></th>                            
                            <th></th> 
                            <th></th>                                                        
                        </tr>    
                    </thead>
                    <tbody>
                        @for($i = 0; $i < count($espelhamento); $i++)
                                <tr>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($espelhamento[$i]->created_at)) }}</td>                                
                                    <td>{{ date('d/m/Y H:i:s', strtotime($espelhamento[$i]->updated_at)) }}</td>                                
                                    <td>{{ $espelhamento[$i]->processo_pagamento }}</td>
                                    <td>{{ $espelhamento[$i]->nota_fiscal }}</td>
                                    
                                    <!-- Os valores das Despesas foram alinhados á Direita para uma melhor visualização e foi inserido no estilo um afastamento de 50px -->
                                    <td style="text-align: right; padding-right: 50px;">{{ number_format($espelhamento[$i]->valor_despesa, 2, ',', '.') }}</td>
                                    <td style="">{{ $espelhamento[$i]->id }}</td>
                                                                
                                    <!-- Foi incluído nos Botões Editar e Excluir um width: 100px para as colunas ficarem do mesmo tamanho -->
                                    <td>
                                        <form class="form_editar" method="GET" action="{{ route('prepara_espelhamento.edit', 'espelhamento', $espelhamento[$i]->id) }}">                                            
                                            @csrf

                                            <input type="hidden" name="espelhamento" value="{{ $espelhamento[$i]->id }}">
                                            {{-- <a href="{{ route('prepara_espelhamento.edit', [$espelhamento[$i]->id]) }}" class="btn btn-sm btn-info" style="color:#fff !important; width: 100px;"><i class="fas fa-edit" style="color:#000"></i> EDITAR</a>                                         --}}
                                            {{-- <button type="submit" class="btn btn-sm btn-info" style="color:#fff !important;"><i class="fas fa-edit" style="color:#000"></i> Editar</button> --}}
                                            <a href="javascript.void(0)" class="btn btn-sm btn-info enviar" style="float:left; margin-top:10px !important; color:#fff !important;"><i class="far fa-edit" style="color:#000; padding-right:4px;"></i> EDITAR</a>
                                        </form>    
                                    </td>
                                    
                                    <td>
                                        <form class="form_exclusao" method="post" action="{{ route('espelhamento.destroy', ['espelhamento' => $espelhamento[$i]->id]) }}">
                                            @csrf

                                            <input name="_method" type="hidden" value="DELETE" id="nome" data-value="Lançamento">                                         

                                            <a href="" class="btn btn-sm btn-danger show-alert-delete-box" style="float:left; margin-top:50px !important; color:#fff !important;"><i class="far fa-trash-alt" style="color:#000; padding-right:4px;"></i> EXCLUIR</a>
                                            {{-- <a href="" class="btn btn-sm btn-danger show-alert">Teste</a> --}}
                                        </form>
                                    </td>
                                    
                                    <td>
                                        <form class="form_lancamento" method="post" action="{{ route('lancamento_unico') }}">
                                            @csrf
                                            <input type="hidden" name="lancamento" value="{{ $espelhamento[$i]->id }}">
                                            <input type="hidden" name="empenho" value="{{ $empenhos->first()->empenho }}">

                                            {{-- <button type="submit" class="btn btn-sm btn-warning form-control" style="color:#fff !important; width: 200px; margin-top:10px; margin-left:-60px !important;">                                                
                                                <i class="fas fa-stamp" style="color:#000;"></i> IMPRIMIR PROTOCOLO
                                            </button> --}}
                                            <a href="javascript.void(0)" class="btn btn-sm btn-warning enviar" style="float:left; margin-top:10px !important; color:#fff !important;"><i class="fas fa-stamp" style="color:#000; padding-right:4px;"></i> IMPRIMIR PROTOCOLO</a>
                                        </form>
                                    </td>
                                    
                                    <td>
                                        <form class="form_exclusao" method="POST" action="{{ route('prepara_espelhamento.destroy', 'espelhamento', $espelhamento[$i]->id) }}" id="">
                                            @method('DELETE')
                                            @csrf
                                            
                                            <input type="hidden" name="id" value="{{ $espelhamento[$i]->id }}">
                                            <input type="hidden" name="empenho" value="{{ $empenhos->first()->empenho }}">
                                        </form>                                                                                   
                                    </td>                                    
                                                                        
                                </tr>     
                            @endfor
                            
                    <tbody>
                </table>

            </div>
        </div>
        
        </div>
       
    </body>

@endsection