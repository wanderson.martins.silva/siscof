@extends('site.layouts.basico')

@section('titulo', 'Espelhamento')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')        

        <div class="conteudo-pagina alinhar_cabecalho">

            <div class="container-fluid texto-container cor-container row alinhar_container">
               
                <form style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" method="post" action="{{ route('prepara_espelhamento.update', [$espelhamento_id]) }}">
                    @csrf
                    @method('PUT')

                    <hr class="hr-invisivel" style="margin-top:120px;"> 
                    <!-- Início Edição LANÇAMENTOS -->           

                    <!-- Pega o valor de espelhamento_id e coloca em input hidden -->           
                    <input type="hidden" name="espelhamento_id" value="{{ $espelhamento_id }}">
                    <input type="hidden" name="empenho" value="{{ $espelhamentos[0]->empenho }}">
                    

                    <div class="col-md-2">
                        <label for="inputNumLancamento" class="form-label">NÚMERO LANÇAMENTO :</label> 
                        <input type="text" name="" value="{{ $espelhamento_id }}" class="form-control" id="created_at" readonly>                        
                        
                    </div>   
                    
                    <div class="col-md-2">
                        <label for="inputEmpenho" class="form-label">EMPENHO</label> 
                        <input type="text" name="empenho[]" value="{{ $espelhamentos[0]->empenho }}" class="form-control" id="empenho" readonly>                        
                        
                    </div>

                    <div class="col-md-4">
                        <label for="inputDtLancamento" class="form-label">DATA ALTERAÇÃO LANÇAMENTO MODIFICADA :</label> 
                        <input type="text" name="" value="{{ date('d/m/Y H:i:s') }}" class="form-control" id="created_at" readonly>
                        <input type="hidden" name="updated_at" value="{{ date('Y-m-d H:i:s') }}">
                        
                    </div>                       

                   <div class="col-md-3">
                        <label for="inputProcessoPagamento" class="form-label">PROCESSO PAGAMENTO</label> 
                        <input type="text" name="processo_pagamento[]" value="{{ $espelhamentos[0]->processo_pagamento }}" class="form-control removeLater" id="processo_pagamento">
                        
                    </div>   
                    
                    <div class="col-md-2">
                        <label for="inputNF" class="form-label">NOTA FISCAL</label> 
                        <input type="text" name="nota_fiscal[]" value="{{ $espelhamentos[0]->nota_fiscal }}" class="form-control removeLater" id="nota_fiscal">
                        
                    </div>                    

                    <div class="col-md-2">
                        <label for="inputValorDespesa" class="form-label">VALOR DESPESA</label> 
                        <input type="text" name="valor_despesa[]" value="{{ $valor_despesa_editado }}" class="form-control removeLater" id="valor_despesa">                        
                        
                    </div>
                                        
                    <div class="row">
                        <label for="inputObservacao" class="form-label" style="margin-top:20px;">OBSERVAÇÕES</label>                         
                        <textarea name="observacao_espelhamento[]" class="ajuste form-control removeLater" id="observacao_espelhamento">{{ $espelhamentos[0]->observacao_espelhamento }}</textarea>                        
                    </div> 

                    <div>
                        <input type="hidden" name="numero_espelhamento[]" value=""> 
                    </div>                        
                    
                    <div>
                        <input type="hidden" name="numero_credor[]" value="">
                    </div>  

                    <div class="col-md-3">
                        <button type="submit" class="btn-primary form-control">SALVAR ALTERAÇÕES</button>
                    </div>                          
                                          
                </div>            
                    
                </form>   
                <!-- FIM FORM LANÇAMENTOS -->           
           
        </div>
        

        </div>

    </body>

@endsection