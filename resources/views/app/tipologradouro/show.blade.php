@extends('site.layouts.basico')

    @section('titulo', 'Tipos Logradouros')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar Tipos de Logradouros</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('tipo.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                
                <tr>
                    <td class="texto-caixa-alta">ID:</td>
                    <td>{{ $tipo->id }}</td>
                </tr>
                <tr>
                    <td class="texto-caixa-alta">Nome:</td>
                    <td>{{ $tipo->nome_tipo_logradouro }}</td>
                </tr>                
                                 
            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection