@extends('site.layouts.basico')

    @section('titulo', 'Tipos de Logradouros')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header alinhar-titulo">Tipos de Logradouros - Editar</h1>                       
    </div>    

    <div class="conteudo-pagina">
        
        {{ $msg ?? '' }}
        <div class="container texto-container cor-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('tipo.update', ['tipo' => $tipo->id]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center" >            
            <input type="hidden" name="id" value="{{ $tipo->id ?? '' }}">
            @csrf
            @method('PUT')

                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">Nome</label>         
                    <input type="text" style="text-transform:capitalize" name="nome_tipo_logradouro" value="{{ $tipo->nome_tipo_logradouro ?? old('nome_tipo_logradouro') }}" id="autoSizingInput" class="form-control">
                    {{ $errors->has('nome_tipo_logradouro') ? $errors->first('nome_tipo_logradouro') : '' }}
                </div>    

                <div class="col-md-2" style="margin-top:40px;">                    
                    <button type="submit" class="form-control btn-warning" style="color:#fff;">ALTERAR</button>
                </div>                  
                      
            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection