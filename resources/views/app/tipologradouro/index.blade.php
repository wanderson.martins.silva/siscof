@extends('site.layouts.basico')

    @section('titulo', 'Tipos de Logradouros')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Tipos Logradouros /Listar</h1> 
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container-fluid" style="padding-top:35px;">
                 <table class="table table-dark table-striped table-hover">
                    <thead>
                        <tr>                           
                            <th class="texto-caixa-alta">Nome</th>                            
                            <th class="texto-caixa-alta">Últ. Alteração</th>                            
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>                        
                            <tr>
                            @foreach ($tipos as $tipo )
                            <tr>
                                <td style="text-transform:capitalize">{{ $tipo->nome_tipo_logradouro }}</td>
                                <td>{{ $tipo->updated_at->format('d/m/Y H:i:s') }}</td>

                                <td>
                                    <a href="{{ route('tipo.show', ['tipo' => $tipo->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important; width:120px !important">
                                        <i class="far fa-eye" style="color:#000;"></i> Visualizar
                                    </a>
                                </td>

                                <td>
                                    <a href="" class="btn btn-sm btn-danger" style="color: #fff !important; width:120px;>
                                        <i class="fas fa-trash-alt" style="color:#000"></i> Excluir
                                    </a>
                                </td>

                                <td>
                                    <a href="{{ route('tipo.edit', ['tipo' => $tipo->id]) }}" class="btn btn-sm btn-primary" style="color: #fff !important; width:120px;">
                                        <i class="fas fa-edit" style="color:#000;"></i> Editar
                                    </a>
                                </td>
                            </tr>    
                            @endforeach                                                                                               
 
                    </tbody>
                </table>    
      
                <br>
                O Total de Tipos de Logradouros cadastrados são <strong>{{ $tipos->total() }}</strong> Registro(s)
                
        </div>

        </div>

    </div>   

</body>   

@endsection