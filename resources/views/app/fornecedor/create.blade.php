@extends('site.layouts.basico')

    @section('titulo', 'Fornecedores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Fornecedores - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('fornecedor.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf

                <div class="col-md-2" style="">       
                    <label for="inputEstadoId" class="form-label">ESTADO</label>                             
                    
                    <select name="estado_id" class="form-select texto-caixa-alta">
                        <option selected>SELECIONE</option>
                                                                        
                            @foreach ($estados as $estado)
                                <option value="{{ $estado->id }}" {{ ($fornecedor->estado_id ?? old('estado->id')) == $estado->id ? 'selected' : ''}}>{{ $estado->nome }}</option>
                            @endforeach
                            {{ $errors->has('estado_id') ? $errors->first('estado_id') : '' }}
                    </select>
                    
                </div>                

                <div class="col-md-4">
                    <label for="inputNome" class="form-label">NOME FORNECEDOR</label> 
                    <input type="text" name="nome" value="{{ old('nome') }}" class="form-control">
                    {{ $errors->has('nome') ? $errors->first('nome') : '' }}
                </div>

                <div class="col-md-3">  
                    <label for="inputCodPessoa" class="form-label">CÓDIGO PESSOA</label>   
                    <input type="text" name="cod_pessoa_externa" value="{{ old('cod_pessoa_externa') }}" class="form-control">
                    {{ $errors->has('cod_pessoa_externa') ? $errors->first('cod_pessoa_externa') : '' }}
                </div> 

                <div class="col-md-2">  
                    <label for="inputTipoPessoa" class="form-label">TIPO PESSOA</label>   
                    <select name="tipo_pessoa_id" class="form-select texto-caixa-alta">
                        <option selected>SELECIONE</option>
                                                                        
                    @foreach ($tipos as $tipo)
                        <option value="{{ $tipo->id }}" {{ ($fornecedor->tipo_pessoa_id ?? old('tipo->id')) == $tipo->id ? 'selected' : ''}}>{{ $tipo->nome_tipo_pessoa }}</option>
                    @endforeach
                                            
                    </select>
                    {{ $errors->has('tipo_pessoa_id') ? $errors->first('tipo_pessoa_id') : '' }}
                </div>  

                <div class="col-md-2">  
                    <label for="inputCnpj" class="form-label">CNPJ</label>   
                    <input type="text" name="cnpj" value="{{ old('cnpj') }}" class="form-control">
                    {{ $errors->has('cnpj') ? $errors->first('cnpj') : '' }}
                </div>  

                <div class="col-md-2">
                    <label for="inputTelefone" class="form-label">TELEFONE FIXO</label>   
                    <input type="text" name="tel_fixo" value="{{ old('tel_fixo') }}" class="form-control">
                    {{ $errors->has('tel_fixo') ? $errors->first('tel_fixo') : '' }}
                </div>
                
                <div class="col-md-2">
                    <label for="inputCelular" class="form-label">CELULAR</label>   
                    <input type="text" name="tel_cel" value="{{ old('tel_cel') }}" class="form-control">
                    {{ $errors->has('tel_cel') ? $errors->first('tel_cel') : '' }}
                </div>     

                <div class="col-md-3">
                    <label for="inputRua" class="form-label">RUA</label>                       
                    <input type="text" name="rua" value="{{ old('rua') }}" class="form-control">
                    {{ $errors->has('rua') ? $errors->first('rua') : '' }}
                </div> 

                <div class="col-md-1">
                    <label for="inputNumero" class="form-label">NUMERO</label>                       
                    <input type="text" name="numero" value="{{ old('numero') }}" class="form-control">
                    {{ $errors->has('numero') ? $errors->first('numero') : '' }}
                </div> 

                <div class="col-md-2">
                    <label for="inputComplemento" class="form-label">COMPLEMENTO</label>                       
                    <input type="text" name="complemento" value="{{ old('complemento') }}" class="form-control">
                    {{ $errors->has('complemento') ? $errors->first('complemento') : '' }}
                </div>    

                <div class="col-md-3">
                    <label for="inputBairro" class="form-label">BAIRRO</label>                       
                    <input type="text" name="bairro" value="{{ old('bairro') }}" class="form-control">
                    {{ $errors->has('bairro') ? $errors->first('bairro') : '' }}
                </div>    

                <div class="col-md-3">
                    <label for="inputCidade" class="form-label">CIDADE</label>                       
                    <input type="text" name="cidade" value="{{ old('cidade') }}" class="form-control">
                    {{ $errors->has('cidade') ? $errors->first('cidade') : '' }}
                </div> 

                <div class="col-md-2">
                    <label for="inputPais" class="form-label">PAÍS</label>                       
                    <input type="text" name="pais" value="{{ old('pais') }}" class="form-control">
                    {{ $errors->has('pais') ? $errors->first('pais') : '' }}
                </div>    

                <div class="col-md-2">
                    <label for="inputCep" class="form-label">CEP</label>                       
                    <input type="text" name="cep" value="{{ old('cep') }}" class="form-control">
                    {{ $errors->has('cep') ? $errors->first('cep') : '' }}
                </div>    

                <div class="col-md-3">
                    <label for="inputContato" class="form-label">CONTATO</label>                       
                    <input type="text" name="contato" value="{{ old('contato') }}" class="form-control">
                    {{ $errors->has('contato') ? $errors->first('contato') : '' }}
                </div>    

                <div class="col-md-4">
                    <label for="inputEmail" class="form-label">EMAIL</label>                       
                    <input type="text" name="email" value="{{ old('email') }}" class="form-control">
                    {{ $errors->has('email') ? $errors->first('email') : '' }}
                </div>    

                <div class="col-md-4">
                    <label for="inputSite" class="form-label">SITE</label>                       
                    <input type="text" name="site" value="{{ old('site') }}" class="form-control">
                    {{ $errors->has('site') ? $errors->first('site') : '' }}
                </div>    

                <div class="row">
                    <label for="inputObservacoes" class="form-label">OBSERVAÇÕES</label>                       
                    <textarea type="text" name="observacoes" class="form-control">{{ old('observacoes') }}</textarea>
                    {{ $errors->has('observacoes') ? $errors->first('observacoes') : '' }}
                </div> 

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection