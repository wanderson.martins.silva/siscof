@extends('site.layouts.basico')

    @section('titulo', 'Fornecedores')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="texto-header fw-bold texto-preto header">Visualizar Fornecedores</h1>                       
    </div>    

    <div class="conteudo-pagina">
        <div class = 'menu' style="">
            <ul>
                <li><a href = "{{ route('fornecedor.index') }}">Voltar</a></li>
                <li><a href = "">Consulta</a></li>
            </ul>
        </div>
        
        <div class="container texto-container" style="padding-top:10px;">
            
            <table class="table table-dark table-striped table-hover">
                <tr>
                    <td class="texto-caixa-alta">FORNECEDOR:</td>
                    <td>{{ $fornecedor->nome }}</td>
                </tr>

                <tr>
                    <td class="texto-caixa-alta">CNPJ:</td>
                    <td>{{ $fornecedor->cnpj }}</td>
                </tr>                
                
                <tr>
                    <td class="texto-caixa-alta">CONTATO:</td>
                    <td>{{ $fornecedor->contato }}</td>
                </tr> 

                <tr>
                    <td class="texto-caixa-alta">UF:</td>
                    <td>{{ $fornecedor->uf->nome }}</td>
                </tr>                 
                           
                <tr>
                    <td class="texto-caixa-alta">NÚMERO PROCESSO:</td>
                    <td>{{ $fornecedor->updated_at }}</td>
                </tr>
                
                <tr>                
                    <td class="texto-caixa-alta">OBSERVAÇÃO:</td>                    
                    <td style="padding-top: 50px; text-align:left">{{ $fornecedor->observacoes }}</td>
                </tr>                    

            </table>
        
        </div>  

        </div>

    </div>   

</body>   

@endsection