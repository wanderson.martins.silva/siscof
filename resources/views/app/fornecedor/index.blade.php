@extends('site.layouts.basico')

@section('titulo', 'Fornecedores')

@section('conteudo')

    <body>

        @include('site.layouts._partials.topo')
        <div class="container-fluid py-5">
            <h1 class="texto-header fw-bold texto-preto header">Fornecedores /Listar</h1>
        </div>

        <div class="conteudo-pagina alinhar_cabecalho">

            <div class="container-fluid">
                <table class="table table-dark table-striped table-hover" id="dataTable">
                    <thead>
                        <tr>
                            <th class="texto-caixa-alta">Fornecedor</th>
                            <th class="texto-caixa-alta">CÓDIGO PESSOA</th>
                            <th class="texto-caixa-alta">CNPJ</th>
                            <th class="texto-caixa-alta">Contato</th>
                            <th class="texto-caixa-alta">UF</th>
                            <th class="texto-caixa-alta">Últ. Alteração</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($fornecedores as $fornecedor)
                            <tr>
                                {{-- Se o Nome do Fornecedor conter mais de 30 Caracteres, a formatação apresentará somente 45, caso o nome seja menor que 30 apresentará o Nome Completo --}}
                                <td>{{ strlen($fornecedor->nome) < 30 ? $fornecedor->nome : mb_strimwidth($fornecedor->nome, 0, 45, "...") }} </td>
                                <td>{{ $fornecedor->cod_pessoa_externa }}</td>
                                <td>{{ $fornecedor->cnpj }}</td>
                                <td>{{ $fornecedor->contato }}</td>                                
                                <td>{{ $fornecedor->uf->nome }}</td>
                                <td>{{ $fornecedor->updated_at == NULL ? $fornecedor->created_at : $fornecedor->updated_at->format('d/m/Y H:i:s') }}</td>

                                <td>
                                    <a
                                        href="{{ route('fornecedor.show', ['fornecedor' => $fornecedor->id]) }}" class="btn btn-sm btn-success" style="color: #fff !important;"><i class="far fa-eye" style="color:#000; padding-right:4px;"></i>VISUALIZAR
                                    </a>
                                </td>
                               
                                <td>                                        
                                    <form method="POST" action="{{ route('fornecedor.destroy', $fornecedor->id) }}">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE" id="nome" data-value="Fornecedor">
                                        {{-- <input name="nome[]" type="hidden" id="nome" data-value="{{ $fornecedor->nome }}"> --}}
                                        {{-- <button type="submit" class="btn btn-sm btn-danger show-alert-delete-box" data-toggle="tooltip" title='Delete' style="margin-top:50px; width:120px;"><i class="far fa-trash-alt" style="color:#000; padding-right:4px;"></i>DELETE</button> --}}
                                        <a href="javascript.void(0)" class="btn btn-sm btn-danger show-alert-delete-box" style="float:left; margin-top:50px !important; color:#fff !important;"><i class="far fa-trash-alt" style="color:#000; padding-right:4px;"></i>EXCLUIR</a>
                                    </form>

                                </td>
                                
                                <td>
                                    <a 
                                        href="{{ route('fornecedor.edit', ['fornecedor' => $fornecedor->id]) }}" class="btn btn-sm btn-info" style="color: #fff !important;"><i class="fas fa-edit" style="color:#000;"></i> EDITAR
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

            </div>

        </div>

        </div>
        

    </body>

@endsection
