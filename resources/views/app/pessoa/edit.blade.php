@extends('site.layouts.basico')

    @section('titulo', 'Tipos de Pessoas')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Tipos de Pessoas - Editar</h1>                       
    </div>    

    <div class="conteudo-pagina" style="margin-top:10px;">
                
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('pessoa.update', ['pessoa' => $pessoa->id]) }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            <input type="hidden" name="id" value="{{ $pessoa->id ?? '' }}">
            @csrf
            @method('PUT')

                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">NOME</label>                             
                    <input type="text" style="text-transform:capitalize" name="nome_tipo_pessoa" value="{{ $pessoa->nome_tipo_pessoa ?? old('nome_tipo_pessoa') }}" id="autoSizingInput" class="form-control">                    
                    {{ $errors->has('nome_tipo_pessoa') ? $errors->first('nome_tipo_pessoa') : '' }}
                </div>    

                <div class="col-md-2" style="margin-top:40px;">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">ALTERAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection