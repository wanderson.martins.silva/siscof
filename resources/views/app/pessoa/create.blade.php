@extends('site.layouts.basico')

    @section('titulo', 'Tipos de Pessoas')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Tipos de Pessoas - Adicionar</h1>                       
    </div>    

    <div class="conteudo-pagina-2">
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('pessoa.store') }}" style="padding-top:50px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf
                <div class="col-md-4" style="">       
                    <label for="inputNome" class="form-label">NOME</label>                             
                    <input type="text" style="text-transform:capitalize" name="nome_tipo_pessoa" value="{{ old('nome_tipo_pessoa') }}" class="form-control">                    
                    {{ $errors->has('nome_tipo_pessoa') ? $errors->first('nome_tipo_pessoa') : '' }}
                </div>    

                <div class="col-md-2" style="margin-top:40px;">                     
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection