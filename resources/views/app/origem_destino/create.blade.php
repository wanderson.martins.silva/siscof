@extends('site.layouts.basico')

    @section('titulo', 'Usuário Origem Destino')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">Usuário Origem/Destino - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('origem-destino.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf

                <div class="col-md-2">  
                    <label for="inputCodigo" class="form-label">BM DO USUÁRIO</label>   
                    <input type="text" name="bm" value="{{ old('bm') }}" class="form-control texto-caixa-alta">
                    {{ $errors->has('bm') ? $errors->first('bm') : '' }}
                </div> 

                <div class="col-md-5">  
                    <label for="inputNome" class="form-label">NOME DO USUÁRIO</label>   
                    <input type="text" name="nome_usuario" value="{{ old('nome_usuario') }}" class="form-control texto-caixa-alta">
                    {{ $errors->has('nome_usuario') ? $errors->first('nome_usuario') : '' }}
                </div> 

                <div class="col-md-4">  
                    <label for="inputIdSetor" class="form-label">NOME DO SETOR</label>   
                    
                    <select name="id_setor" id="setor" class="form-control texto-caixa-alta">                        
                        @foreach ($setores as $setor)
                        <option value="{{ $setor->id }}">{{ $setor->sigla_setor. '- ' . $setor->nome_setor }}</option>
                        @endforeach
                    </select>
                    {{ $errors->has('id_setor') ? $errors->first('id_setor') : '' }}
                </div> 

                <div class="quebralinha">
                    <br />
                </div>

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>

        </div>

        <div class="container-fluid" style="margin-top:10px;">
            <table class="table table-dark table-striped table-hover" id="dataTable">
                <thead>
                    <tr>
                        <th class="texto-caixa-alta">BM DO USUÁRIO</th>
                        <th class="texto-caixa-alta">NOME DO USUÁRIO</th>                                                                        
                        <th class="texto-caixa-alta">SETOR DO USUÁRIO</th>   
                    </tr>
                </thead>
                <tbody>
                    @foreach ($usuarios as $usuario)
                    <tr>
                        <td class="texto-caixa-alta">{{ $usuario->bm }}</td>
                        <td class="texto-caixa-alta">{{ $usuario->nome_usuario }}</td>                        
                        <td>{{ $usuario->nome_setor }}</td>                        
                    </tr>                            
                    @endforeach
                </tbody>    
            </table>        
        </div>           

    </div>   

</body>   

@endsection