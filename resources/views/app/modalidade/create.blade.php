@extends('site.layouts.basico')

    @section('titulo', 'Modalidade')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">MODALIDADES - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container-fluid texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('modalidade.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf

                <div class="col-md-5">  
                    <label for="inputNome" class="form-label">NOME DA MODALIDADE</label>   
                    <input type="text" name="nome_modalidade" value="{{ old('nome_modalidade') }}" class="form-control">
                    {{ $errors->has('nome_modalidade') ? $errors->first('nome_modalidade') : '' }}
                </div> 

                <div class="quebralinha">
                    <br />
                </div>

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>

        </div>

        <div class="container-fluid" style="margin-top:10px;">
            <table class="table table-dark table-striped table-hover" id="dataTable">
                <thead>
                    <tr>
                        <th class="texto-caixa-alta">CÓDIGO DA MODALIDADE</th>
                        <th class="texto-caixa-alta">NOME DA MODALIDADE</th>                                                                        
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($modalidades as $modalidade)
                    <tr>
                        <td>{{ $modalidade->id }}</td>
                        <td>{{ $modalidade->nome_modalidade }}</td>                        
                    </tr>                            
                    @endforeach
                </tbody>    
            </table>        
        </div>           

    </div>   

</body>   

@endsection