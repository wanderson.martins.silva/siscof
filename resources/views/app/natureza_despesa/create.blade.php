@extends('site.layouts.basico')

    @section('titulo', 'Natureza Despesas')

    @section('conteudo') 
<body>
    
    @include('site.layouts._partials.topo')
    <div class="container-fluid py-5">
        <h1 class="text-header fw-bold texto-preto header alinhar-titulo">NATUREZA DESPESAS - Adicionar</h1>                            
    </div>    

    <div class="conteudo-pagina-2">        
        
        <div class="container texto-container" style="padding-top:10px;">
        
        <form method="post" action="{{ route('natureza_despesa.store') }}" style="padding-top:10px;" class="row gy-2 gx-3 align-items-center cor-container">            
            
            @csrf

                <div class="col-md-2">
                    <label for="inputCodigoNatureza" class="form-label">CÓDIGO NATUREZA</label> 
                    <input type="text" name="codigo_natureza" value="{{ old('codigo_natureza') }}" class="form-control">
                    {{ $errors->has('codigo_natureza') ? $errors->first('codigo_natureza') : '' }}
                </div>

                <div class="col-md-5">  
                    <label for="inputNome" class="form-label">NOME DA NATUREZA</label>   
                    <input type="text" name="nome_natureza" value="{{ old('nome_natureza') }}" class="form-control">
                    {{ $errors->has('nome_natureza') ? $errors->first('nome_natureza') : '' }}
                </div> 
                
                <div class="col-md-3">  
                    <label for="inputEscrituracao" class="form-label">ESCRITURAÇÃO</label>                      
                    <select name="escrituracao" class="form-select texto-caixa-alta">
                        <option selected>Existe Escrituracao ?</option>                                                                                                      
	                            <option value="SIM">SIM</option>
	                            <option value="NÃO">NÃO</option>
                    </select>
                    {{ $errors->has('escrituracao') ? $errors->first('escrituracao') : '' }}
                </div>  

                <div class="row">
                    <label for="inputEmenta" class="form-label">EMENTA</label>                       
                    <textarea name="ementa" name="ementa" class="form-control">{{ old('ementa') }}</textarea>
                    {{ $errors->has('ementa') ? $errors->first('ementa') : '' }}
                </div>

                <div class="col-md-2">                    
                    <button type="submit" class="form-control btn-success" style="color:#fff;">CADASTRAR</button>
                </div>                    

            </form>
            </div>

        </div>

    </div>   

</body>   

@endsection