@extends('site.layouts.basico')
    

    @section('conteudo')
    <body>

        @include('site.layouts._partials.topo')

        <div class="conteudo-destaque texto-preto container-destaque">
        
            <div class="container-fluid py-5">
                <h1 class="display-6 fw-bold texto-preto header mt-2">Gestão completa e descomplicada</h1>
                <p class="col-md-8 fs-4 texto-preto">Aplicação Financeira, Orçamentária e Almoxarifado</p>                
            </div>
            
        </div>
    </body>    
    @endsection

