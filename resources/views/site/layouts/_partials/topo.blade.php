<div class="topo">

            <div class="logo">        
              <!-- Para mudar a versão manualmente altere na linah abaixo --> 
              <span><img src="{{ asset('/img/logo-pbh-transp-2.png') }}" style="width:180px; height:76px; margin-top:-18px; position:absolute;"></span>    
              <span class="versao" style="padding-left:180px; font-family: monospace; font-size:20px;">VERSÃO 1.7.0</span> <!-- Versionamento do Sistema -->
            </div>

            <ul class="nav nav-pills float-end">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('site.index') }}">PRINCIPAL</a>
                </li>

                <!-- Início do Sub Menu CADASTROS -->
                <ul class="navbar-nav">
                    <li class="nav-item dropdown alinhar-submenu">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        CADASTROS
                      </a>
                      <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                        {{-- <li><a class="dropdown-item" href="{{ route('fornecedor.index') }}">Consultar</a></li> --}}
                        <li><a class="dropdown-item" href="{{ route('fornecedor.create') }}">Novo Fornecedor</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('ordenador.create') }}">Novo Ordenador</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('setor.create') }}">Novo Setor</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('uf.create') }}">Nova UF</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('tipo.create') }}">Novo Tipo de Logradouro</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('logradouro.create') }}">Novo Logradouro</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('contrato.create') }}">Novo Contrato</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('pessoa.create') }}">Nova Pessoa</a></li>                       
                      </ul>
                    </li>
                  </ul>
                 <!-- Fim do Sub Menu CADASTROS -->

                <!-- Início do Sub Menu CONSULTAS -->
                <ul class="navbar-nav">
                    <li class="nav-item dropdown alinhar-submenu">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        CONSULTAS
                      </a>
                      <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                        {{-- <li><a class="dropdown-item" href="{{ route('fornecedor.index') }}">Consultar</a></li> --}}
                        <li><a class="dropdown-item" href="{{ route('fornecedor.index') }}">Consultar Fornecedores</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('ordenador.index') }}">Consultar Ordenadores</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('setor.index') }}">Consultar Setores</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('uf.index') }}">Consultar UFs</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('tipo.index') }}">Consultar Tipos de Logradouros</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('logradouro.index') }}">Consultar Logradouros</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('contrato.index') }}">Consultar Contratos</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('pessoa.index') }}">Consultar Pessoas</a></li>                       
                        <li><a class="dropdown-item" href="{{ route('natureza_despesa.index') }}">Consultar Despesas</a></li>                       
                      </ul>
                    </li>
                  </ul>
                 <!-- Fim do Sub Menu CONSULTAS -->
                 
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="{{ route('empenho.index') }}">EMPENHOS</a>
                </li>    
                
                <!-- Início do Sub Menu ESPELHAMENTO -->
                <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      ESPELHAMENTO
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                      <li><a class="dropdown-item" href="{{ route('espelhamento.index') }}">Espelhar</a></li>
                      <li><a class="dropdown-item" href="{{ route('prepara_espelhamento.index') }}">Realizados</a></li>                       
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu ESPELHAMENTO -->

                <!-- Início do Sub Menu IDO -->
                <li class="nav-item">
                  <a class="nav-link" aria-current="page" href="{{ route('mcc.index') }}">MCC</a>
                </li>  

                 <!-- Início do Sub Menu IDO -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      IDO
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                      <li><a class="dropdown-item" href="{{ route('ido.create') }}">Criar Objeto IDO</a></li>                      
                      <li><a class="dropdown-item" href="{{ route('ido.index') }}">Lançar Objeto IDO</a></li>                                                                                                     
                      <li><a class="dropdown-item" href="{{ route('lancamento_ido.index') }}">Consultar Lançamentos IDO</a></li>                                                                                                     
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu IDO -->

                 <!-- Início do Sub Menu NATUREZA DESPESAS -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      NATUREZA DESPESAS
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                      <li><a class="dropdown-item" href="{{ route('natureza_despesa.create') }}">Criar Natureza Despesa</a></li>                                            
                      <li><a class="dropdown-item" href="{{ route('natureza_despesa.index') }}">Consultar Natureza Despesa</a></li>                                            
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu NATUREZA DESPESAS -->


                 <!-- Início do Sub Menu ITENS NATUREZA DESPESAS -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      ITENS NAT. DESPESAS
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">                                                                 
                      <li><a class="dropdown-item" href="{{ route('item_despesa.index') }}">Consultar Itens Natureza Despesa</a></li>                                            
                      {{-- <li><a class="dropdown-item" href="{{ route('get-itens') }}">Query Itens</a></li>                                             --}}
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu ITENS NATUREZA DESPESAS -->

                 <!-- Início do Sub Menu RECURSO SICOM -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      RECURSO SICOM
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">                                                                 
                      <li><a class="dropdown-item" href="{{ route('recurso_sicom.create') }}">Criar Recurso</a></li>                                            
                      {{-- <li><a class="dropdown-item" href="{{ route('get-itens') }}">Query Itens</a></li>                                             --}}
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu RECURSO SICOM -->

                 <!-- Início do Sub Menu MODALIDADE -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      MODALIDADE
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">                                                                 
                      <li><a class="dropdown-item" href="{{ route('modalidade.create') }}">Criar Modalidade</a></li>                                            
                      
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu MODALIDADE -->
                
                 <!-- Início do Sub Menu ORIGEM/DESTINO -->
                 <ul class="navbar-nav">
                  <li class="nav-item dropdown alinhar-submenu">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                      ORIGEM/DESTINO
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">                                                                 
                      <li><a class="dropdown-item" href="{{ route('origem-destino.create') }}">Criar Usuáio Origem/Destino</a></li>                                            
                      
                    </ul>
                  </li>
                </ul>
                <!-- Fim do Sub Menu ORIGEM/DESTINO -->

                <li class="nav-item">
                   <a class="nav-link" href="{{ route('login') }}">LOGIN</a>  
                </li>
            </ul>
        </div>

