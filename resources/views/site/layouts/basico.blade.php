<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SISCOF/ALMOX @yield('titulo')</title>
        <meta charset="utf-8">        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
                
        <link rel="stylesheet" href="{{asset('css/estilo_basico.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/bootstrap-icons.min.css'>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/5.0.7/sweetalert2.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    </head>

        @yield('conteudo')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>            
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
        <script src="{{ asset('/js/menu.js') }}"></script> 
        <script src="{{ asset('/js/jquery.maskMoney.min.js') }}"></script> 

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/js/brands.min.js"></script>

        <script type="text/javascript" charset="utf-8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
        
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        {{-- Define as opções do Datatable para Português, controla a paginação com as quantidades 10, 25, 50 e 100 --}}
        <script>
	        $(document).ready( function(){      
                $('#dataTable').DataTable({                                        

                    language: {                                                
                        "url": "//cdn.datatables.net/plug-ins/1.11.4/i18n/pt_br.json",
                    },
                    "pageLength": 10,
                    "lengthMenu": [10, 25, 50, 100],    
                        
                    });
		        
	        }); 
        </script>
                        
        <script>            
            /*
                Ativando o preenchimento da máscara monetária no padrão Brasileiro com . para Milhares e , para Decimais
                Utilizei o Plugin Jquery MaskMoney.
            */    
            $("#valor_despesa").maskMoney({thousands:'.', decimal:',', allowZero:true}) //Para digitação Manual comente esta linha
            $(".monetario").maskMoney({selectAllOnFocus: true, thousands:'.', decimal:',', allowZero:true}) //Para digitação Manual comente esta linha
        </script>    

        {{-- Script que dispara a mensagem antes da tentativa de exclusão --}}

        <script>  
            function uppercase(txt) {
                txt.value = txt.value.toUpperCase();
            }
        </script> 

        {{-- Utilizando Sweet Alert 2 para emitr os alertas antes da Exclusão --}}
        <script> 
            $('.show-alert-delete-box').click(function(event){                
                var value = $('#nome').data('value');
                var form =  $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                    title: "Você tem certeza que deseja Excluir este " + value + " ?",
                    text: "Se você excluí-lo, não terá mais como recuperá-lo!",
                    icon: "warning",
                    type: "warning",
                    buttons: ["Cancelar","Sim!"],
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim, Excluir!',  
                    cancelButtonText: 'Cancelar'                  
                }).then((willDelete) => {
                    if (willDelete) {                        
                        form.submit();                        
                    }
                });
            });
        </script>     

        {{-- Script que vai submeter o formulário através de um <a href class="btn enviar"> --}}

        <script> 
            $('.enviar').click(function(event){                
                var value = $('#nome').data('value');
                var form =  $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();                 
                form.submit();
                                        
            });
        </script>     
        
        {{-- Script que vai ler o Campo Ficha na View show ido e auto completar os campos uo, ua, funcao, acao(projativ), programa, subfuncao, natureza_despesa, valor estimado(valor_orcado) 
         fonte, fonte_detalhe   --}}

        <script> 
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');            

            $(document).ready(function(){                 

            $( "#mcc_search" ).autocomplete({
            source: function( request, response ) {
                // Fetch data
                $.ajax({
                    url:"http://localhost:8000/app/mcc/get_mcc",
                    type: 'post',
                    dataType: "json",
                    data: {
                    _token: CSRF_TOKEN,                    
                    search: request.term
                    },
                    success: function( data ) {
                    response( data );
                    }
                });
            },
            select: function (event, ui) {
                // Set selection
                $('#mcc_search').val(ui.item.label); // display the selected text
                $('#mccuo').val(ui.item.uo); // save selected uo to input
                $('#mccua').val(ui.item.ua); // save selected ua to input
                $('#mccfuncao').val(ui.item.funcao); // save selected função to input
                $('#mccprojativ').val(ui.item.projativ); // save selected ação to input
                $('#mccprograma').val(ui.item.programa); // save selected programa to input
                $('#mccsubfuncao').val(ui.item.subfuncao); // save selected subfuncao to input
                $('#mccnaturezadespesa').val(ui.item.naturezadespesa); // save selected natureza_despesa to input                
                $('#mccfonte').val(ui.item.fonte); // save selected fonte to input
                $('#mccfontedetalhe').val(ui.item.fontedetalhe); // save selected fonte_detalhe to input
                return false;
            }
            });

            });                    
        </script>         

        {{-- Busca o Item na Caixa de Seleção de Itens do IDO e ao ser clicado preenche o Input descitemnatureza com o código e nome do Item Selecionado --}}
        <script>
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');    
            $("#carrega_itens").click(buscaItem);
            
            function buscaItem(){
                 
                var itemId = $("#mccnaturezadespesa").val();
                var dados = {codigo_natureza_despesa: itemId}
                var path = "http://localhost:8000/app/item_despesa/getItens/"+itemId;
            
            $.ajax({
                url: "http://localhost:8000/app/item_despesa/getItens/"+itemId,
                type: 'get',
                dataType: 'json',
                data: {
                    _token: CSRF_TOKEN,                    
                    dados: dados,
                },
                success: function (obj){
                    if (obj != null) {
                        var item = obj.data;
                        var selectbox = $('#item_search');
                        selectbox.find('option').remove();
                        $.each(obj, function (index, value) {
                            $('<option>').val(value.label).text(value.label + ' - ' + value.nome_item).appendTo(selectbox);                                                                                                    
                        });                        
                        
                    }    
                    
                    $("#item_search").change(preencheDesc);                    
                    
                }
            });

            }

            function preencheDesc() {
                var natureza = $("#mccnaturezadespesa").val();
                // Pega o valor da Option Selecionada
                var item_selecionado = $("#item_search option:selected").text();                                
                
                $("#descitemnatureza").val(natureza + ' - ' + item_selecionado);
            }

        </script>

        {{-- Transforma os Selects utilizando o Plugin Select2 para realizar pesquisas para agilizar a encontrar o usuário desejado --}}
        <script type="text/javascript">
            $(document).ready(function(){
                $("#recurso").select2();
                $("#modalidade").select2();
                $("#item_search").select2();
                $("#setor").select2();                                              
                $("#ordenador1").select2();                                              
                $("#ordenador2").select2();                                              
                $("#ordenador3").select2();                                              
                $("#ordenador4").select2();                                              
            });
        </script>

        {{-- Seleciona os Usuários e Setores de Origem e Destino do IDO --}}
        <script>
            
            var $bm1 = $("#nome1").select2();
            $('#nome1').on('select2:select', function (e) { 
                var selecionado1 = $("#nome1 option:selected").val();
                $("#setor1").val(selecionado1);

                // Separa a Matricula1 e o Nome1
                var matricula1 = $("#nome1 option:selected").text().split("-");   

                // Preenche a Matricula1 com o valor do array na posição 0 pegando a Matrícula do Usuário Selecionado
                for (num=0; num < matricula1.length; num++){
                   var matricula_selecionada1 = matricula1[0];
                }
                
                $("#bm1").val(matricula_selecionada1);
                // console.log(matricula_selecionada1);
            });
            
            var $bm2 = $("#nome2").select2();
            $('#nome2').on('select2:select', function (e) { 
                var selecionado2 = $("#nome2 option:selected").val();
                $("#setor2").val(selecionado2);   

                // Separa a Matricula2 e o Nome2
                var matricula2 = $("#nome2 option:selected").text().split("-");                

                // Preenche a Matricula2 com o valor do array na posição 0 pegando a Matrícula do Usuário Selecionado
                for (num=0; num < matricula2.length; num++){
                   var matricula_selecionada2 = matricula2[0];
                }
                
                $("#bm2").val(matricula_selecionada2);
                // console.log(matricula_selecionada2);
                
            });

            // Seleção do Ordenador1 
            $('#ordenador1').on('select2:select', function (e) { 
                var selecionado3 = $("#ordenador1 option:selected").val();
                $("#setor_ordenador1").val(selecionado3);   

                // Separa a Matricula e o Nome do Ordenador
                var matricula3 = $("#ordenador1 option:selected").text().split("-");                

                // Preenche a Matricula3 com o valor do array na posição 0 pegando a Matrícula do Ordenador Selecionado
                for (num=0; num < matricula3.length; num++){
                   var matricula_selecionada3 = matricula3[0];
                }
                
                $("#matricula_ordenador1").val(matricula_selecionada3);                
                
            });
            
            // Seleção do Ordenador2 
            $('#ordenador2').on('select2:select', function (e) { 
                var selecionado4 = $("#ordenador2 option:selected").val();
                $("#setor_ordenador2").val(selecionado4);   

                // Separa a Matricula e o Nome do Ordenador
                var matricula4 = $("#ordenador2 option:selected").text().split("-");                

                // Preenche a Matricula4 com o valor do array na posição 0 pegando a Matrícula do Ordenador Selecionado
                for (num=0; num < matricula4.length; num++){
                   var matricula_selecionada4 = matricula4[0];
                }
                
                $("#matricula_ordenador2").val(matricula_selecionada4);                
                
            });

            // Seleção do Ordenador3 
            $('#ordenador3').on('select2:select', function (e) { 
                var selecionado5 = $("#ordenador3 option:selected").val();
                $("#setor_ordenador3").val(selecionado5);   

                // Separa a Matricula e o Nome do Ordenador
                var matricula5 = $("#ordenador3 option:selected").text().split("-");                

                // Preenche a Matricula5 com o valor do array na posição 0 pegando a Matrícula do Ordenador Selecionado
                for (num=0; num < matricula5.length; num++){
                   var matricula_selecionada5 = matricula5[0];
                }
                
                $("#matricula_ordenador3").val(matricula_selecionada5);                
                
            });

            // Seleção do Ordenador4 
            $('#ordenador4').on('select2:select', function (e) { 
                var selecionado6 = $("#ordenador4 option:selected").val();
                $("#setor_ordenador4").val(selecionado6);   

                // Separa a Matricula e o Nome do Ordenador
                var matricula6 = $("#ordenador4 option:selected").text().split("-");                

                // Preenche a Matricula6 com o valor do array na posição 0 pegando a Matrícula do Ordenador Selecionado
                for (num=0; num < matricula6.length; num++){
                   var matricula_selecionada6 = matricula6[0];
                }
                
                $("#matricula_ordenador4").val(matricula_selecionada6);                
                
            });
        </script>        

    </body>
</html>

