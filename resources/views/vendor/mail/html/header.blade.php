<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{ asset('/img/logo-pbh-transp-2.png') }}" class="logo" alt="Laravel Logo" style="width:180px; height:76px;">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
