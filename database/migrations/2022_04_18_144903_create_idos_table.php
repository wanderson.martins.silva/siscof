<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idos', function (Blueprint $table) {
            $table->id();
            $table->string('data');            
            $table->string('usuario');  
            $table->bigInteger('ano_ido');     
            $table->string('scm_ido')->nullable();            
            $table->string('ano_scm', 15)->nullable();  
            $table->bigInteger('id_modalidade_ido');
            $table->bigInteger('numero_ido');
            $table->bigInteger('ano_pregao_ido');   
            $table->string('processo_ido', 130);        
            $table->text('objeto');            
            $table->text('demanda');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idos');
    }
}
