<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenadores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('setor_id');
            $table->string('nome_ordenador', 50);
            $table->string('matricula_ordenador', 20);            
            $table->text('observacao_ordenador')->nullable();
            $table->timestamps();

            //Alteração na Migration incluindo o campo setor_id

            $table->foreign('setor_id')->references('id')->on('setores');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {                       
        Schema::dropIfExists('ordenadores');
    }
}
