<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspelhamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espelhamentos', function (Blueprint $table) {
            $table->id();
            $table->string('fornecedor_id', 40);            
            $table->string('numero_credor')->nullable();            
            $table->string('processo_licitatorio')->nullable();            
            $table->string('processo_pagamento')->nullable();            
            $table->string('nota_fiscal')->nullable();            
            $table->float('valor_despesa', 8, 2)->nullable();            
            $table->string('ano_exercisio');            
            $table->string('empenho');            
            $table->string('ficha');    
            $table->float('valor_empenhado', 8, 2)->nullable();                               
            $table->Integer('numero_espelhamento')->nullable();                                                       
            $table->text('observacao_espelhamento')->nullable();
            $table->timestamps();

            //Chave estrangeira para relacionamento entre as Tabelas Espelhamentos e Fornecedores
            //$table->foreign('fornecedor_id')->references('id')->on('fornecedores');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espelhamentos');
    }
}

