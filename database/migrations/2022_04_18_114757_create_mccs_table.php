<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMccsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mccs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ficha')->nullable();
            $table->bigInteger('tipo_credito')->nullable();
            $table->bigInteger('uo')->nullable();
            $table->bigInteger('ua')->nullable();
            $table->bigInteger('funcao')->nullable();
            $table->bigInteger('subfuncao')->nullable();
            $table->bigInteger('programa')->nullable();
            $table->bigInteger('subprograma')->nullable();
            $table->bigInteger('projativ')->nullable();
            $table->bigInteger('natureza_despesa')->nullable();
            $table->bigInteger('fonte')->nullable();
            $table->bigInteger('fonte_detalhe')->nullable();
            $table->float('vl_orcado', 8, 2)->nullable();
            $table->float('vl_acrescimo', 8, 2)->nullable();
            $table->float('vl_cancelado', 8, 2)->nullable();
            $table->float('vl_provido_base', 8, 2)->nullable();
            $table->float('vl_provido_adicional', 8, 2)->nullable();
            $table->float('vl_reservado_minuta', 8, 2)->nullable();
            $table->float('vl_solicitado_base', 8, 2)->nullable();
            $table->float('vl_solicitado_adicional', 8, 2)->nullable();
            $table->float('vl_aprovado_base', 8, 2)->nullable();
            $table->float('vl_aprovado_adicional', 8, 2)->nullable();
            $table->string('classificacao', 40)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mccs');
    }
}
