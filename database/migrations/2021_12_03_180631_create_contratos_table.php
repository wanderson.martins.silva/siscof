<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->id();            
            $table->bigInteger('for_id');            
            $table->string('numero_contrato');
            $table->string('inst_juri');            
            $table->string('numero_processo');            
            $table->string('pregao');            
            $table->string('ano_pregao');            
            $table->string('objeto_pregao');            
            $table->text('objeto_contrato');            
            $table->text('observacao')->nullable();            
            $table->timestamps();
            
            //Constraints
            $table->foreign('for_id')->references('id')->on('fornecedores');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contratos');
    }
}
