<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Verificar com os Responsáveis pelo Cadastro quais serão os campos obrigatórios
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('estado_id')->nullable();
            $table->bigInteger('tipo_pessoa_id')->nullable();
            $table->string('cod_pessoa_externa', 40);
            $table->string('nome', 140);            
            $table->string('cnpj', 14)->nullable();
            $table->string('tel_fixo', 11)->nullable();
            $table->string('tel_cel', 11)->nullable();
            $table->string('rua', 30)->nullable();
            $table->string('numero', 5)->nullable();
            $table->string('complemento', 20)->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('cidade', 20)->nullable();            
            $table->string('cep', 8)->nullable();
            $table->string('pais', 20)->nullable();
            $table->string('contato', 20)->nullable();
            $table->string('email', 80)->nullable();
            $table->string('site', 40)->nullable();
            $table->text('observacoes')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            //Constraint
            $table->foreign('estado_id')->references('id')->on('ufs');   
            $table->foreign('tipo_pessoa_id')->references('id')->on('tipo_pessoas');           
            
                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedores');
    }
}
