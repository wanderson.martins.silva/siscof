<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogradourosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logradouros', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tipo_logradouro_id');            
            $table->string('nome_logradouro', 40);
            $table->integer('numero_logradouro');
            $table->string('complemento_logradouro', 20);
            $table->string('bairro_logradouro', 40);
            $table->string('cidade_logradouro', 30);
            $table->bigInteger('cep_logradouro');
            $table->text('observacao_logradouro')->nullable();
            $table->timestamps();

            //Constraints
            
            $table->foreign('tipo_logradouro_id')->references('id')->on('tipo_logradouros');            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::dropIfExists('logradouros');
        
    }
}
