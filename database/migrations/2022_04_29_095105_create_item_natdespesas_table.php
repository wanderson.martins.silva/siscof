<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemNatdespesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_natdespesas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('codigo_item');
            $table->string('nome_item');
            $table->bigInteger('codigo_natureza_despesa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_natdespesas');
    }
}
