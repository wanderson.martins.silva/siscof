<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLancamentoIdosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lancamento_idos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ido_id');                                                            
            $table->bigInteger('ficha_ido')->nullable();                                                                                                   
            $table->bigInteger('uo')->nullable();
            $table->bigInteger('ua')->nullable();
            $table->bigInteger('funcao')->nullable();
            $table->bigInteger('subfuncao')->nullable();
            $table->bigInteger('programa')->nullable();
            $table->bigInteger('projativ')->nullable();
            $table->bigInteger('subacao')->nullable();
            $table->bigInteger('natureza_despesa')->nullable();
            $table->bigInteger('item')->nullable();
            $table->bigInteger('fonte')->nullable();                     
            $table->bigInteger('fonte_detalhe')->nullable();                                 
            $table->bigInteger('grupo')->nullable();  
            $table->bigInteger('sicom');
            $table->string('recurso', 130)->nullable();                                           
            $table->string('resolucao', 130)->nullable();                                           
            $table->string('conta', 130)->nullable();                                           
            $table->string('portaria', 130)->nullable();                                           
            $table->string('bm_origem', 20)->nullable();                                      
            $table->string('bm_destino', 20)->nullable();
            // Á Pedido de Tiago da Geofi serão disponibilizados 4 Ordenadores, porém apenas o 1º será Obrigatório
            $table->bigInteger('ord1');
            $table->bigInteger('ord2')->nullable();            
            $table->bigInteger('ord3')->nullable();            
            $table->bigInteger('ord4')->nullable();            
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lancamento_idos');
    }
}
