## Siscof / Almox
<b>Gestão completa e descomplicada -
Aplicação Financeira e Orçamentária</b>

--------------------------------------------------------------------------------------------------------------------------------
<b>- Primeiro Passo</b></br>

<b>Configurações Necessárias no arquivo php.ini para o bom funcionamento do Sistema.</b>

Alterar as seguintes variáveis para :

max_execution_time = 1800 </br>
max_input_time = 1800</br>
post_max_size = 20M</br>
upload_max_filesize = 20M</br>
memory_limit = 512M</br>

<b>Descomentar as seguintes linhas para utilização do Postgres</b>

extension=pgsql</br>
extension=pdo_pgsql

<b>Erros emitidos pelo Sistema caso as Configurações não sejam realizadas</b>

* trying to get property name of non object
* the "" file does not exist or is not readable.
* Allowed memory size of 33554432 bytes exhausted (tried to allocate 43148176 bytes) in php
* Maximum execution time of 60 seconds exceeded

As Varíaveis max_execution_time e max_input_time caso não sejam configuradas conforme descritos neste manual o erro abaixo vai acontecer:

Maximum execution time of 60 seconds exceeded

- Isto vai depender do Processador do Servidor, caso seja uma máquina mais antiga

--------------------------------------------------------------------------------------------------------------------------------
<b>- Segundo Passo</b>
* Crie o Banco de Dados SISCOF

--------------------------------------------------------------------------------------------------------------------------------
<b>- Terceiro Passo</b>
* A aplicação já virá direcionada para o github.com e para o repositório https://github.com/wandersonsilva/siscof.git
* Realize um clone da aplicação utilizando o comando : git clone https://github.com/wandersonsilva/siscof.git
* Faça uma cópia do arquivo .env.example para .env (Configure o Banco de dados desejado)
* Configure o banco de dados no arquivo config/database.php 
* php artisan key:generate (Para gerar a chave de autenticação para começar á utilizar o Sistema)
* php artisan migrate (Para subir as tabelas no banco de dados)

--------------------------------------------------------------------------------------------------------------------------------
<b>- Quarto Passo</b>
* Comece Alimentando o Banco de Dados com as seguintes informações para evitar atrasos nos cadastros:

- UFs
- Tipo de Logradouros
- Setores
- Tipos de Pessoas
- Importar Empenhos (Planilha de Empenhos)
* A importação da Planilha Empenhos da GEOFI vai suprir as informações necessárias para trabalhar com os espelhamentos, pois ela alimentará a Tabela Empenhos com os dados importados.
--------------------------------------------------------------------------------------------------------------------------------
<b>- Importante</b>
* Caso já utilizou o comando git clone: As próximas atualizações do Sistema devem ser feitas utilizando o comando: 
git PULL evitando o retrabalho de apagar o diretório e clonar novamente, assim o git vai até o repositório e baixar as últimas
atualizações  para o seu projeto.

--------------------------------------------------------------------------------------------------------------------------------
<b>08/12/2021</b>
* Modelagem do Banco de dados 
* Neste projeto utilizaremos o PostgreSQL porque está no Catálogo de Tecnologias da Prodabel, mas pode ser implantado no SQLServer.

<b>13/12/2021</b>
* Aplicação do old nos campos para recuperar campos de uma requisição anterior
* Aplicação da Busca por id do Fornecedor
* Cadastro  e Edição de Fornecedores

<b>14/12/2021</b>
* Acrescentado Listagem buscando também pelo Estado(UF)
* Acrescetando a exibição do Total de Fornecedores na pesquisa
* Acrescentado o padrão de exclusão SoftDeletes
* Acrescentado no Menu a opção Ordenadores
* Acrescentado a Listagem de Ordenadores
* Alteração no Campo Estado para 2 dígitos - padrão MG/SP/RJ/GO
* Correção de Bugs nas Telas de Adicionar e Editar (Fornecedores)
* Aplicação de Caixa alta nos Labels dos Forms
* Validações na Tela de Cadastro dos Ordenadores
* Criação da Tela Editar Ordenadores
* Alterações no CSS

<b>15/12/2021</b>
* Criação da <code><b>Tabela Empenhos</b></code> para importação de Planilhas em Excel e popular o banco de dados
* Tela de Empenhos criada

<b>16/12/2021 até 31/12/2021</b>
* Pausa no Projeto para apoio as UPAS - Suporte in Loco - (Daniela Freitas e Daniele Ferreira)

<b>03/01/2020 até dia 07/01/2022</b>
* Foi levantado um Ambiente de Testes para criação da Importação de Planilhas de Empenhos para o Banco de Dados

<b>10/01/2022</b>
* Implementação da Importação de Planilhas de Empenhos para o Banco de Dados no Ambiente de Homologação

<b>12/01/2022</b>
* Implementação das configurações do Sistema e Levantamento de erros caso não sejam realizadas as configurações necessárias contidas neste manual

<b>13/01/2022</b>
* Alterações nas <code><b>Tabelas Usuarios e Setores</b></code> (Redefinição de Foreign Key)
* Correções nas View Adicionar entre Estados(UFs) e Fornecedores
* Criação da View Setores Listar
* Adicionado Cadastro de Setores

<b>14/01/2022</b>
* Acrescentado View de Visualização e Edição de Setores
* Acrescentado View de Visualização/Edição/Criação de UFs (Estados)
* Inclusão de Paginação da View UFs (Estados)
* Correção de Bug na apresentação na Listagem onde ANO EMPENHO e DT ÚLT. IMPORTAÇÃO estavam apresentando Erros.
* Acrescentado Exclusão de UFs
* Correções no CSS para apresentação da View UF 

<b>17/01/2022</b>
* Ajuste de cores nos links das tags TD - Link color:rgb(230, 171, 10) | :hover color: rgb(241, 243, 116)
* Acrescentado no Menu TIPOS LOGRADOUROS e sua View inicial
* Acrescentado as Views Cadastro/Edição/Listagem de Tipos de Logradouros
* Alteração dos CSS nas Views Cadastro/Edição/Listagem para apresentar as UFs em Maiúsculas

<b>18/01/2022</b>
* Corrigido BUG na atualização na Matrícula, Setor e Observação de Ordenador
* Corrigido BUG ao buscar dados e gravar dados do Campo Observação na TAG TEXTAREA da tela de Edição de Ordenadores
* Corrigido BUG ao buscar dados e gravar dados do Campo Observação na TAG TEXTAREA da tela de Edição de Setores
* Corrigido BUG ao buscar dados e gravar dados do Campo Observação na TAG TEXTAREA da tela de Edição de Fornecedores
* UFs cadastradas agora serão gravadas no Banco de dados em Maiúsculas - Ex: MG/SP/RJ/GO mesmo se o usuário tentar digitá-las em minúsculas
* Adicionado as telas Cadastro/Listagem para LOGRADOUROS.

<b>19/01/2022</b>
* Corrigido BUG na <code><b>Tabela Logradouros</b></code> que apresentava erro de duas PRIMARIES KEYS existentes
* Corrigido BUG que impediam as outras tabelas de subirem ao Banco de Dados
* Acrescentado o campo Observação na Visualização de Setores

<b>20/01/2022</b>
* Implementação da Não Obrigatoriedade de preenchimento do Campo Observação na Tela de Cadastro de Setores/Contratos/Logradouros
* Acrescentado as Telas de Cadastro/Edição/Alteração/Visualização de Contratos
* Acrescentado as Telas de Cadastro/Edição/Alteração/Visualização de Tipos de Pessoas


<b>21/01/2022</b>
* Refatoração de todas as Views da <code><b>Tabela Fornecedor</b></code>


<b>24/01/2022</b>
* Refatoração de todas as Views index de todo o Sistema


<b>25/01/2022</b>
* Correção de Bugs na tela de Cadastro de Contratos quando relaciona com Fornecedores
*

<b>26/01/2022</b>
* Correção da View Logradouros, agora também mostra o Tipo de Logradouro
* Implementação da View index Fornecedores, configurada para mostrar o Nome da UF (SP/MG/RJ) e etc.
* Implementação da View show Fornecedores, configurada para mostrar o Nome da UF (SP/MG/RJ) na visualização dos dados do Fornecedor.
* Alteração na Migration Ordenadores criando a Chave Estrangeira setor_id para relacionamento com a <code><b>tabela Setores</b></code>.


<b>27/01/2022</b>
* Correção da View index, create, show, edit para apresentar os Ordenadores relacionados com Setores
* Ajustes realizados no CSS para alinhar todos os dados apresentados na Esquerda de todas as views index e show 
* Compo alterado conforme Alinhamento realizado com Simoni, pediu-se para que o Campo Ojeto Contrato fosse igual a um Textarea para gravação de muitas informações
* Inclusão do item Espelhamento no Menu

<b>28/01/2022</b>
* Foi acrestando na view Edit dos Logradouros o Campo bairro
* Foi ajustado os tamanhos dos campos cidade, bairro e complemento na view edit da View Logradouro
* Ajustes no tamanho da fonte no CSS para tbody, td, tfoot, th, thead, tr
* Criação da Tela Espelhamentos (View Create)

<b>31/01/2022</b>
* View Create Fornecedores ajustada para não obrigar o cadastro de alguns registros como CNPJ (A verificação dos Campos obrigatórios serão configuradas depois levantar com o Setor Responsável pelo Cadastro)
* A Obrigatoriedade da View Create está somente para Nome por enquanto
* Criado o Campo cod_pessoa_externa na <code><b>Tabela Fornecedores</b></code>


<b>01/02/2022</b>
* Views Create/Edit de Fornecedores ajustada com o novo Campo tipo pessoa
* Tipos de Pessoas Físicas serão gravadas no Banco de Dados em minúsculas (Física/Jurídica)
* Alteração layout Views Create/Edit Fornecedores ajustes nos tamanhos dos campos
* Acrescentado na View Edit Ordenadores a seleção por Setor baseado nos cadastros de Setores


<b>09/02/2022</b>
* Views Index de Espelhamentos alterado layout para adequar ao carregamento de dados via Jquery Table
* Alterado Controller Espelhamento para buscar dados dos Empenhos importados via Planilha
* Traduzida mensagem "No matching records found" para Nenhum registro encontrado na Jquery Table
* Ajuste na View index para exibir a formatação de Decimais e Milhares padrão Brasileiro
* Ajuste na View index de Espelhamento para criar o Espelhamento

<b>10/02/2022</b>
* Redimensionamento de partições do Sistema Windows no Servidor de Produção
* Levantado o Servidor Local para servir o Sistema como Ambiente Produção Local
* Views Index de Espelhamentos alterado texto do Botão para Espelhar
* Criação do Controller PreparaEspelhamentoController que recebe dados do Fornecedor que será espelhado
* Criação da View show prepara_espelhamento

<b>11/02/2022</b>
* Redefinição de parâmetros no Jquery Table #dataTable que apresentam informações de espelhamento e outras telas que o utilizarem para "pageLength": 25, "lengthMenu": [10, 25, 50, 100], 
* Alteração do Layout na tela prepara_espelhamento conforme documento word novo Siscof
* Inclusão do campo Processo no Grid Jquery Table de Espelhamento
* Refatoração da <code><b>Tabela Setores</b></code> Campo nome_setor para 80 caracteres
* Criada a Query para inserir fornecedores sem Duplicidade através de uma SELECT para agilizar os Cadastros de Fornecedores

<b>
	
	INSERT INTO fornecedores (cod_pessoa_externa, nome, estado_id, created_at, updated_at)	
	SELECT distinct(e.codigo_pessoa), e.nome_pessoa, 13, now(), now() 	
	FROM empenhos e	
	WHERE NOT EXISTS (SELECT cod_pessoa_externa
					  FROM fornecedores f
					  WHERE f.cod_pessoa_externa = e.codigo_pessoa
	);
	
</b> 	
	
* Alteração View exibição Fornecedores mostrando informações com Jquery Data Table	
* Alteração View exibição Ufs mostrando informações com Jquery Data Table	
* Alteração View exibição Setores mostrando informações com Jquery Data Table	
* Criado a validação para o Campo Responsavel no Cadastro de Setor
* Alterado as Views Fornecedores, Ordenadores, Setores, UFs, Tipos Logradouros, Logradouros, Contratos, Pessoas padronizando os tamanhos dos Containeres para container-fluid
* Criado os Inserts para popular a <code><b>Tabela UFs</b></code><br/>
<b>

	insert into ufs(nome, created_at, updated_at) values('AC', now(), now());
	insert into ufs(nome, created_at, updated_at) values('AL', now(), now());
	insert into ufs(nome, created_at, updated_at) values('AP', now(), now());
	insert into ufs(nome, created_at, updated_at) values('AM', now(), now());
	insert into ufs(nome, created_at, updated_at) values('BA', now(), now());
	insert into ufs(nome, created_at, updated_at) values('CE', now(), now());
	insert into ufs(nome, created_at, updated_at) values('DF', now(), now());
	insert into ufs(nome, created_at, updated_at) values('ES', now(), now());
	insert into ufs(nome, created_at, updated_at) values('GO', now(), now());
	insert into ufs(nome, created_at, updated_at) values('MA', now(), now());
	insert into ufs(nome, created_at, updated_at) values('MT', now(), now());
	insert into ufs(nome, created_at, updated_at) values('MS', now(), now());
	insert into ufs(nome, created_at, updated_at) values('MG', now(), now());
	insert into ufs(nome, created_at, updated_at) values('PA', now(), now());
	insert into ufs(nome, created_at, updated_at) values('PB', now(), now());
	insert into ufs(nome, created_at, updated_at) values('PR', now(), now());
	insert into ufs(nome, created_at, updated_at) values('PE', now(), now());
	insert into ufs(nome, created_at, updated_at) values('PI', now(), now());
	insert into ufs(nome, created_at, updated_at) values('RJ', now(), now());
	insert into ufs(nome, created_at, updated_at) values('RN', now(), now());
	insert into ufs(nome, created_at, updated_at) values('RS', now(), now());
	insert into ufs(nome, created_at, updated_at) values('RO', now(), now());
	insert into ufs(nome, created_at, updated_at) values('RR', now(), now());
	insert into ufs(nome, created_at, updated_at) values('SC', now(), now());
	insert into ufs(nome, created_at, updated_at) values('SP', now(), now());
	insert into ufs(nome, created_at, updated_at) values('SE', now(), now());
	insert into ufs(nome, created_at, updated_at) values('TO', now(), now());
</b>
<br/>
* Criado os Inserts para popular a <code><b>Tabela Setores</b></code> com alguns Setores da SMSA<br/>
<strong>

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Assessoria Jurídica', 'AJU-SA', 'Hércules Guerra', '3132777787', 'assejursmsa@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Assessoria de Comunicação Social', 'ASCOM-SA', 'Stephanie Mendes', '3132777756', 'gcso@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Assessoria de Educação em Saúde', 'ASEDS-SA', 'Cláudia Fidélis Barcaro', '3132779281', 'gedsa@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Assessoria de Planejamento e Ações Intersetoriais', 'ASPLAN-SA', 'Guilherme Augusto Orair', '3132779319', 'asplan@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Assessoria de Tecnologia da Informação em Saúde', 'ASTIS-SA', 'Eliete Guizilini Moreira', '3132775208', 'astis@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Conselho Municipal de Políticas sobre Drogas de Belo Horizonte', 'CMPD-BH', 'Marcelo Antonio Derussi', '3132774153', 'cmpd@pbh.gov.br', now(), now());

	insert into setores(nome_setor, sigla_setor, responsavel, telefone_setor, email_setor, created_at, updated_at)
	values('Conselho Municipal de Saúde', 'CMS', 'Carla Anunciatta de Carvalho', '3132777733', 'cmsbh@pbh.gov.br', now(), now());
</strong>

<b>14/02/2022</b>
* Alteração tamanho campo nome_setor na view Create
* Alteração tamanho campo nome_setor na view Edit
* Campos do Formulário Prepara para Espelhar estão agora como somente leitura (input readonly="true") para não sofrerem alterações pelos usuários conforme reunião Meet (14/02/2022) de hoje junto á <strong>Guéria, Simoni e Márcia GEOFISA e Marcony ASTIS</strong>
* Inclusão do Campo ficha na View <code>show prepara_espelhamento</code>

<b>15/02/2022</b>
* Alteração do Layout view prepara_espelhamento adcionando os Campos : saldo_atual, processo_pagamento, nota_fiscal, valor_despesa, observacao e Label Lançamentos Realizados
* Refatoração no Layout da view <code>show prepara_espelhamento</code> para Adicionar e Remover Lançamentos dinamicamente de NF, Nota Empenho, Valor Despesa, Observacao, Valor Despesa.
* Refatoração na migration <code>2021_12_06_120649_create_espelhamentos_table</code>, foi retirado o campo competencia_espelhamento.
* Refatatoração na migration <code>2021_12_06_120649_create_espelhamentos_table</code>, Campo com o nome cod_empenho foi mudado para empenho
* Criado os inserts para popular a <code><b>Tabela tipo_logradouros</b></code><br/>

<b>
	
	insert into tipo_logradouros(nome_tipo_logradouro, created_at, updated_at) values('Avenida', now(), now());	
	insert into tipo_logradouros(nome_tipo_logradouro, created_at, updated_at) values('Rua', now(), now());	
	insert into tipo_logradouros(nome_tipo_logradouro, created_at, updated_at) values('Alameda', now(), now());
	
</b>

* Criado os inserts para popular a <code>Tabela tipo_pessoas</code><br><br>

<b>
	
	insert into tipo_pessoas(nome_tipo_pessoa, created_at, updated_at) values('fisica', now(), now());	
	insert into tipo_pessoas(nome_tipo_pessoa, created_at, updated_at) values('juridica', now(), now());
	
</b>

* Ajuste de campos na View show prepara_espelhamento
* Criado a View index prepara_espelhamento para Mostrar os espelhamentos que foram criados  

<b>16/02/2022</b>
* Refatoração da migrations <code>2021_12_06_120649_create_espelhamentos_table</code> e <code>2021_12_03_180544_create_fornecedores_table</code>
* Alteração view <code>show prepara_espelhamento</code>

<b>17/02/2022</b>
* Refatatoração da View topo alterando o item Espelhamento como Submenu dropdown com as opções : Espelhar e Realizados.
* Inclusão das rotas nos itens Espelhar e Realizados.
* Alteração do tamanho Campo nome de 60 para 140 caracteres na Migration <code>2021_12_03_180544_create_fornecedores_table</code>
* Executar o php artisan migrate:refresh para reestruturação do Banco de Dados na Tabela Fornecedor.
* Foi acrescentada a biblioteca do Jquery popper.min.js para perfeito funcionamento dos Menus Drop Drowns.
* Alterado o Header da View index do espelhamento para: Fornecedores á Espelhar.
* Refatoração View show prepara_espelhamento, layout e form para salvar lançamentos dos empenhos que foram espelhados.
* Refatoração View show prepara_espelhamento, retirado do layout o Campo nota_empenho, pois o mesmo será substituído pelo empenho.
* Criação das novas Classes CSS cabecalho, invisivel, alinhar_cabecalho, alinhar_container
* Alteração na Classe hr
* Alterado Submenu Fornecedores para dropdown com as opções : Consultar e Novo
* Alteração no Layout da View index fornecedores com alinhamento mais próximo ao cabeçalho
* Alteração no Layout da View create fornecedores - div com a hrefs de CONSULTA E NOVO foram retiradas.
* Alterado Submenu Pessoas para dropdown com as opções : Consultar e Novo

<b>18/02/2022</b>
* Criado a Classe fonte-cabecalho para estilização dos Cabeçalhos
* Alteração do posicionamento do botão REMOVER LANÇAMENTO.
 
<b>21/02/2022</b>
* Alteração Layout View Show prepara_espelhamento
* Alteração na migration inclusão dos Campos nota_fiscal, valor_despesa
* Teste de gravação na tabela espelhamento

<b>22/02/2022</b>
* Alteração no Menu Principal Submenu ESPELHAMENTO, agora ficará do lado do Submenu EMPENHOS já que estão ligados e desta forma agilizará ainda mais nos processos dos Usuários.
* Inserido na Grid Lançamentos Realizados os Campos Dt. Lançamento e Proc. Pagamento
* Refatatoração do Controller PreparaEspelhamentoController para receber Array de Campos Adicionados aos Lançamentos

<b>23/02/2022</b>
* Ajustes na View prepara_espelhamento

<b>24/02/2022</b>
* A partir de hoje estou versionando o SISCOF com versões númericas vou me basear nas alterações conforme as datas descritas nesta Documentação; então hoje a Versão é 1.34
* Alterado a view Principal para apresentar o número da versão do Sistema <span class="versao">1.34</span> <!-- Versionamento do Sistema -->
* Alterado a view prepara_espelhamento para apresentar a Data de Lançamento formatada como dd/mm/aaaa
* Alterado CSS da Logo pa melhor Exibição do Nome do Sistema com Versão

	.logo {
		margin-bottom: 15px;   
	}
* Alteração no Controller PreparaEspelhamentoController action Show para buscar os espelhamentos de um determinado fornecedor
* Alteraçã de Versão para 1.35

<b>25/02/2022</b>
* Versão 1.36
* Alteração do Controller PreparaEspelhamentoController para melhor performance na exibição dos Campos da View prepara_espelhamento
* Corrigido o nome da variável $empenho para $empenhos; pois estava conflitando com $empenho da URL, acrescentado a query para buscar os dados dos espelhamentos lançados; agora estão retornando dados do Espelhamento pesquisado na URL

<b>28/02/2022</b>
* Versão 1.37
* Inclusão dos valores de Totais de Despesas e Saldo Atual na View Show prepara_espelhamento
* Inclusão de um card para melhorar a apresentação dos valores Totais de Despesas e Saldo Atual na View Show prepara_espelhamento
* Formatação de Moeda nos Campos Valor Empenhado, Totais das Despesas, Saldo Atual e Valor das Despesas na View Show prepara_espelhamento
* Alteração nos Campos vl_empenhado, vl_anulado, valor_liquidado, valor_pago, vl_anul_pagamento, saldo_emp_liquidar, saldo_emp_pagar na Migration create_empenhos_table alterado de String para Decimal
* Alteração nos Campos valor_despesa, valor_empenhado na Migration create_espelhamentos_table alterados de String para Decimal 
* Alteração na action Store do Controller PreparaEspelhamentoController utilizando str_replace para retirar os valores dos . e , para o Campo valor_empenhado gravar no Banco de Dados somente números.


<b>01/03/2022</b>
* Versão 1.38
* Formatação da Data de Lançamento no Formato dd/mm/aaaa h m s na View Show prepara_espelhamento
* Inclusão dos Botões EDITAR E EXCLUIR na Show prepara_espelhamento
* Inclusão de um DataTable para pesquisar e apresentar os dados da Grid Lançamentos

<b>02/03/2022</b>
* Versão 1.39
* Campos valor_despesa, valor_empenhado refatorados na Migration 2021_12_06_120649_create_espelhamentos_table para float.
* Alteração Campo observacao_espelhamento para nullable, agora aceita observações vazias.
* Alterações para o banco de Dados tratar corretamente as representações do Campo valor_despesa quando adicionar centavos ou a casa decimal com ,00

<b>03/03/2022</b>
* Versão 1.40
* Configuração dos Inputs dos valores_despesas como preenchimento automático de . para Milhares e , para Decimais
* Melhorias no visual de apresentação dos DataTables de Pesquisa em todas as Views do projeto SISCOF/ALMOX, alteração nas Classes .dataTables_filter input e dataTables_length select
* Alterado Submenu Ordenadores para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create Ordenadores
* Alterado Submenu Setores para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create Setores
* Alterado Submenu UFS para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create UFS
* Alterado Submenu TIPOS LOGRADOUROS para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create TIPOS LOGRADOUROS
* Alterado Submenu LOGRADOUROS para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create LOGRADOUROS
* Alterado Submenu CONTRATOS para dropdown com as opções : Consultar e Novo
* Correções de espaçamentos na View Create CONTRATOS

<b>04/03/2022</b>
* Versão 1.40.1
* Alterações no CSS para melhor apresentação das Informações nas Views 
* Correções nas quebras de Layout na View Show prepara_espelhamento
* Correção da Performance ao apresentar dados da DataTable

<b>08/03/2022</b>
* Versão 1.40.2
* Criação da View edit prepara_espelhamento

<b>09/03/2022</b>
* Versão 1.40.3
* Alterações na View edit prepara_espelhamento pegando os valores que serão Editados
* Ajustes View edit prepara_espelhamento

<b>10/03/2022</b>
* Versão 1.40.4
* Alteração no Campo data para somente leitura
* Refatoração da View Show prepara_espelhamento para envio de dados para a View Edit prepara_espelhamento
* Alteração Método Update prepara_espelhamento

<b>14/03/2022</b>
* Versão 1.40.5
* Finalização da View Edit prepara_espelhamento, editando os dados sensíveis monetários e permitindo alterações nos Campos data_lancamento, processo_pagamento, nota_fiscal, valor_despesa, observacao_espelhamento

<b>16/03/2022</b>
* Versão 1.40.6
* Teste de Exclusão na View prepara_espelhamento action destroy

<b>17/03/2022</b>
* Versão 1.40.7
* Testes de Exclusões na View prepara_espelhamento action destroy

<b>18/03/2022</b>
* Versão 1.40.7
* Alteração view e Testes de Exclusões nos Lançamentos de espelhamentos

<b>21/03/2022</b>
* Versão 1.40.8
* Alteração na View index prepara_espelhamento, incluído formatação nos Botões de Visualizar, Excluir, Editar
* Exclusão de espelhamento 
* Alteração controller PreparaEspelhamentoController

<b>22/03/2022</b>
* Versão 1.40.9
* Alteração no layout da View prepara_espelhamento nos botões Excluir, Editar e Visualizar
* Criação do Controller ImprimirEmpenhoController
* Criação da View imprime_carimbo
* Alterações nos estilos CSS

<b>23/03/2022</b>
* Versão 1.41.0
* Finalização da View imprime_protocolo
* Finalização do Controller imprimirEmpenhoController 
* Alterações nos tamanhos dos Botões Cadastrar e Alterar nas Views create e edit em todas as views
* Alterações dos Botões com ícones na UI em todas as Views

<b>24/03/2022</b>
* Versão 1.41.1
* Criação Logo SISCOF
* Corrigido na View index prepara_espelhamento o TH ANO EXERCÍSIO para EXERCÍCIO

<b>25/03/2022</b>
* Versão 1.4.2 Mudei o padrão para 3 Dígitos da versão
* Alteração na lista de Usuários para impressão dos Protocolos de Lançamentos
* Alteração no Logo utilizando o Logo da Prefeitura de Belo Horizonte
* Inclusão da View imprime_lancamento - esta view possibilita ao usuário imprimir o protocolo de um lançamento único de um determinado empenho
* Módulo Empenho/Espelhamento implantado em Servidor Local na ASTIS-SA e utilizado pelos usuários da GEOFI

	'Márcia Aparecida Abouid - BM - 38.127-x', <br>
    'Amanda Caroline Amaral Campos - PRPS022309',<br>
    'Guéria Silva de Paulo - PR108650',<br>
    'Juliana Carvalho de Oliveira - PR111396',<br>
    'Maria do Carmo Assunção – PR001763',<br>
    'Simoní de Souza - PRPS017787'

<b>29/03/2022</b>
* Implementação das Classes e Views Login, Register, Confirm, Reset, Verify e Email
* Implementação de Autenticação necessária para acessar Todas as Views exceto Principal

<b>30/03/2022</b>
* Retirada do Front da Aplicação a Rota Register, á partir de hoje o Setor Deverá solicitar ao Marcony cadastro de acesso para utilizar o Sistema.
* Conforme solicitado por Amanda e Márcia o Cálculo do Card com o Saldo Atual agora = vl_empenhado - despesas - vl_anulado

<b>31/03/2022</b>
* 1.4.5
* -- Melhorias --
* Implementação do Campo instrumento_juridico na View Show prepara_espelhamento á pedido de Simoni e Márcia 8º Andar
* Campo NOME FORNECEDOR e CÓDIGO PESSOA obrigatórios no cadastro de novo Fornecedor
* Alteração de Login para Campo name em vez de e-mail no Controller LoginController á pedido de Marcony
* Correção da falha ao gravar tipo de pessoa no Cadastro de Fornecedores.

<b>01/04/2022</b>
* 1.4.6
* -- Melhorias --
* Subimos para a Produção todas as alterações realizadas no Ambiente de Homologação
* Implementação da Notificação Esqueceu Senha?
* Implementação da Notificação Reset Senha
* Implementação de padronização de Logins em Maiúsculas cadastrados no Banco de dados e também validando em Maiúsculas quando for realizar o Login

<b>04/04/2022</b>
* 1.4.7
* -- Melhorias --
* Personalização da Notificação do Esqueceu a Senha
* Largura do Logo aumentado para 180px;
* padding-left do span Versão aumentado para 180px;

<b>05/04/2022</b>
* 1.4.8
* -- Melhorias --
* Inner join Fornecedores e Espelhamentos para uma melhor visualização e pesquisa pelo nome do Fornecedor agilizando os processos de visualização, edição e exclusão de Lançamentos
*Formatação de Data padrão e Valor de Despesa no formato Brasileiro
* Implementação da Tabela log_acessos

<b>06/04/2022</b>
* 1.4.9
* -- Melhorias --
* Implementação Middleware LogAcesso
* Gravação de Logs de Acessos IP + Rota acessada.
* Gravação de Logs Usuário + Acessos IP + Rota acessada.

<b>06/04/2022</b>
* 1.5.0
* -- Melhorias --
* Implementação do Campo ua na View Show prepara_espelhamento
* Implementação dos Campo UA na View - Alterações pedidas por Amanda e Márcia 8 Andar GEOFI

<b>07/04/2022</b>
* 1.5.1
* -- Melhorias --
* Corrigido a falha de Gravação de Logs quando o Usuário não estiver Logado.
* O Usuário não logado é forçado a Logar para Navegar nos Menus de Cadastros, Editar, Excluir
* Implementação da Middleware UserActivity
* Implementação do Controller UserController
* Implementação da Rota online-user
* Implementação da View user.index para visualização de Usuários Online no Sistema visualizando Nome, Login, Email, Última Visualização, Status

<b>08/04/2022</b>
* 1.5.2
* -- Melhorias --
* Implementação da Informação Saldo Atual na View index espelhamento 
* Alteração do Campo Login na View online-user para Maiúsculas

<b>11/04/2022</b>
* 1.5.3
* Correção do Cálculo do Saldo Atual
* -- Melhorias e Testes --
* Backups Gerais do Banco de Dados Agendados todos os dias 12hs (Meio Dia) e 17hs utilizando Script backup no modelo Backup_Siscof_dd_mm_aa_hh_mm
* Testes do Botão Delete via Ajax na View index de Fornecedores
* Testes do Botão Delete via Ajax na View index de Espelhamentosp

<b>12/04/2022</b>
* 1.5.4
* -- Melhorias --
* Implementação do Botão Delete via Ajax na View index de Fornecedores
* Implementação do Botão Delete via Ajax na View index de Espelhamentosp

<b>13/04/2022</b>
* 1.5.5
* -- Melhorias --
* Implementação do Botão Delete via Ajax na View show prepara_espelhamento (Exclusão de Lançamentos)
* Troca de Buttons por a href substituindo os botões editar, excluir e imprimir protocolo na View show prepara_espelhamento (Lançamentos)
* Implementação da Impressão de protocolos (Lançamentos) utilizando o Nome + Matrícula do Usuário Logado no Sistema

<b>18/04/2022</b>
* 1.5.6
* -- Melhorias --
* Alterações na View index prepara_espelhamento, botão Visualizar; agora visualiza os Lançamentos realizados, o botão Editar; agora edita o lançamento pelo seu número.
* Alteração na View show prepara_espelhamento, agora a tela apresenta o campo Data Alteração (do Lançamento).
* Alteração dos Menus do Sistema, foi Criado o Menu Cadastros e Consultas para diminuir a quantidade de informações na tela e preparando a Aplicação para a nova etapa de Desenvolvimento IDO.
* Criado o Menu IDO.

<b>19/04/2022</b>
* 1.5.7
* -- Melhorias --
* Criação da View create ido para criar os IDOS
* Criação da View index ido para visualizar, criar, editar e excluir os IDOS

<b>20/04/2022</b>
* 1.5.8
* -- Melhorias --
* Criação das Migrations : 2022_04_18_114757_create_mccs_table, 2022_04_18_144903_create_idos_table, 2022_04_20_091758_create_lancamento_idos_table
* Criação da View show para criar os Lançamentos dos IDOs

<b>27/04/2022</b>
* 1.5.9
* -- Melhorias --
* Implementação do AJAX pesquisa para o Campo Ficha que ao ser digitado e selecionado preenche os campos uo, ua, função, subfunção, programa, ação, nat. despesa, fonte, f.detalhe e valor estimado na View show ido

<b>29/04/2022</b>
* 1.6.0
* -- Melhorias --
* Criação das migrations 2022_04_29_094619_create_natureza_despesas_table e 2022_04_29_095105_create_item_natdespesas_table
* Criação da View create natureza_despesa
* Criação da View index natureza_despesa

<b>02/05/2022</b>
* 1.6.1
* -- Melhorias --
* Criação da View create item_natdespesa
* Alteração na View create item_natdespesa para apresentar os itens criados depois de cadastrados em Grid.
* Criação da View index item_natdespesa para apresentar os itens de natureza despesas criados

<b>03/05/2022</b>
* 1.6.2
* -- Melhorias --
* Alteração na View show ido para buscar itens relacionados á Natureza Despesa

<b>04/05/2022</b>
* 1.6.3
* -- Melhorias --
* Criação do Botão Carrega itens que busca itens de Naturezas Despesas na view show ido

<b>05/05/2022</b>
* 1.6.4
* -- Melhorias --
* Input Descrição item da Natureza Despesa traz os valores do input Natureza Despesa e concatena com os valores do Option selecionado em itens

<b>06/05/2022</b>
* 1.6.5
* -- Melhorias --
* Criação da Migration 2022_05_06_131050_create_recurso_sicoms_table, controller RecursoSicomController
* implementação do Plugin Jquery Select2 melhorando a pesquisa dos recursos dentro do Select Option na View show ido

<b>09/05/2022</b>
* 1.6.6
* -- Melhorias --
* Criação dos Inserts de itens para os itens de natureza despesas para popular o banco dados, testando a aplicabilidade entre itens e Natureza de Despesas
* Criação dos Inserts de recurso sicoms para popular o banco de dados, testando a aplicabilidade entre recursos e Idos 
* Criação da migration 2022_05_09_105510_create_modalidades_table
* Criação do Controller ModalidadeController
* Criação da View create modalidade para Criação de Modalidades

<b>11/05/2022</b>
* 1.6.7
* -- Melhorias --
* Implementação nos Selects ids nome1, nome2, ordenador utilizando o plugin Jquery Select2 deixando realizar pesquisas no elemento Select
* Implementação dos Scripts na view basico para que ao ser selecionado uma opção no elemento Select preencha também os inputs setor1, setor2
* Implementação de um Input hidden para capturar as Matrículas dos usuários
* Campo observação no Controller OrdenadorController agora não exige digitação e pode ser gravado como null

<b>12/05/2022</b>
* 1.6.8
* -- Melhorias --
* Implementação no Select id ordenador utilizando o Jquery Select2 deixando realizar pesquisas no elemento Select na view show ido
* Implementação de um Input hidden para capturar as Matrículas dos Ordenadores
* Refatoração das tabelas setores, lancamento_idos

<b>13/05/2022</b>
* 1.6.9
* -- Melhorias --
* Alteração no Texto do Menu IDO -> Submenu Criar IDO para Criar Objeto IDO
* Alteração na section Titulo para Objeto IDO na view create ido
* Alteração na H1 para Objeto IDO - Adicionar na view create ido
* Alteração na section titulo para Objeto IDO na view index ido
* Alteração da H1 para Objeto IDO na view index ido
* Implementação da DataTable para pesquisas e filtros das informações de IDOS criados
* Alteração na TH da DataTable para OJETO IDO
* Alteração no Texto do Menu IDO -> Submenu Consultar IDO para Lançar Objeto IDO
* Alteração do Button Lançar IDO e TA para Lançar Objeto IDO
* Refatatoração da Tabela idos e lancamento_idos, foram retirados os campos ano_ido, scm_ido, ano_scm, id_modalidade_ido, numero_ido, ano_pregao_ido, processo_ido da tabela lancamento_idos e incluídos na tabela idos.

<b>14/05/2022</b>
* 1.7.0
* -- Melhorias --
* Criado os Campos resolucao, conta, portaria na tabela lancamento_idos
* Criar um inner join entre idos e lancamentos para trazer processo_ido.
* Pesquisa de Lançamentos Objeto IDO agora deixa filtrar por resolucao, conta, portaria, processo_ido, ano_ido, ficha