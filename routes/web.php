<?php

use Illuminate\Support\Facades\Route;
use App\Mail\MensagemReset;
use App\Http\Middleware\LogAcessoMiddleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(LogAcessoMiddleware::class)->get('/', 'PrincipalController@principal')->name('site.index');

Route::middleware(['auth', LogAcessoMiddleware::class])->prefix('/app')->group(function(){
    
    Route::resource('fornecedor', 'FornecedorController');
    Route::resource('ordenador', 'OrdenadorController');
    Route::resource('empenho', 'EmpenhoController');    
    Route::post('mcc/mostraMccs/', 'MccController@mostraMccs')->name('mostraMccs');
    Route::post('mcc/get_mcc/', 'MccController@getMcc')->name('get-mcc');        
    Route::resource('mcc', 'MccController');    
    Route::post('import-empenho', 'ImportEmpenhoController@import')->name('importEmpenho');
    Route::post('import-mcc', 'ImportMccController@import')->name('importMcc');
    Route::resource('setor', 'SetorController');
    Route::resource('uf', 'UFController');
    Route::resource('tipo', 'TipoLogradouroController');
    Route::resource('logradouro', 'LogradouroController');
    Route::resource('contrato', 'ContratoController');
    Route::resource('pessoa', 'TipoPessoaController');
    Route::resource('espelhamento', 'EspelhamentoController');   
    Route::resource('prepara_espelhamento', 'PreparaEspelhamentoController');    
    Route::get('ido/teste/', 'IdoController@teste')->name('teste');
    Route::resource('ido', 'IdoController');                                  
    Route::resource('lancamento_ido', 'LancamentoIdoController');                   
    Route::resource('natureza_despesa', 'NaturezaDespesasController');                     
    Route::resource('item_despesa', 'ItemNatdespesasController');                   
    Route::post('item_despesa/getItens/{item}', 'ItemNatdespesasController@getItens')->name('get-itens');     
    Route::get('item_despesa/getItens/{item}', 'ItemNatdespesasController@getItens')->name('get-itens');     
    Route::post('imprimir_protocolo', 'ImprimirEmpenhoController@imprimir_protocolo')->name('imprimir_protocolo');
    Route::post('lancamento_unico', 'ImprimirEmpenhoController@lancamento_unico')->name('lancamento_unico');
    Route::resource('recurso_sicom', 'RecursoSicomController');
    Route::resource('modalidade', 'ModalidadeController');
    Route::get('origem-destino/getUsuario/{usuario}', 'OrigemDestinoController@getUsuario');    
    Route::resource('origem-destino', 'OrigemDestinoController');
    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mensagem-reset', function() {
	return new MensagemReset();

});

Route::get('/online-user', 'UserController@index')->name('online_user');

